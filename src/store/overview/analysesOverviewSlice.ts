import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { createSelector } from 'reselect'

import API from 'app/API'

import { processAndStoreAnalysis } from 'store/interpretation/analysisSlice'
import type { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import * as ApiTypes from 'types/api/Pydantic'
import { OverviewAnalysis, mapFromApi } from 'types/store/AnalysisOverview'

type WorkflowStage =
  | 'notReady'
  | 'notStarted'
  | 'markedReview'
  | 'markedMedicalReview'
  | 'ongoing'
  | 'finalized'

type SliceState = Record<WorkflowStage, OverviewAnalysis[]>

const initialState: SliceState = {
  notReady: [],
  notStarted: [],
  markedReview: [],
  markedMedicalReview: [],
  ongoing: [],
  finalized: [],
}

export const fetchAnalyses = createAsyncThunk<SliceState>(
  'analysisOverview/fetchAnalyses',
  async (_, { dispatch }) => {
    const apiAnalysesByWorkflowStage = await API.NonFinalizedAnalyses.get().then(
      (response) => response.data,
    )

    // Process and store analysis on separate slice
    const promises: Promise<any>[] = []
    const mapFunction = (apiAnalysis: ApiTypes.OverviewAnalysis): OverviewAnalysis => {
      promises.push(dispatch(processAndStoreAnalysis(apiAnalysis)))
      return mapFromApi(apiAnalysis)
    }

    const finalized = await API.FinalizedAnalyses.get().then((response) => response.data)

    const analysesByWorkflowStage = {
      notReady: apiAnalysesByWorkflowStage.not_ready.map(mapFunction),
      notStarted: apiAnalysesByWorkflowStage.not_started.map(mapFunction),
      ongoing: apiAnalysesByWorkflowStage.ongoing.map(mapFunction),
      markedReview: apiAnalysesByWorkflowStage.marked_review.map(mapFunction),
      markedMedicalReview:
        apiAnalysesByWorkflowStage.marked_medicalreview.map(mapFunction),
      finalized: finalized.map(mapFunction),
    }

    await Promise.all(promises)

    return analysesByWorkflowStage
  },
)

const analysesSlice = createSlice({
  name: 'analyses',
  initialState,
  reducers: {
    setAnalyses: (state, action: PayloadAction<SliceState>) => {
      Object.assign(state, action.payload)
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAnalyses.fulfilled, (state, action) => action.payload)
  },
})

export const selectNotStartedAnalyses = (state: RootState) =>
  state.analysesOverview.notStarted
export const selectNotReadyAnalyses = (state: RootState) =>
  state.analysesOverview.notReady
export const selectPendingReviewAnalyses = (state: RootState) =>
  state.analysesOverview.markedReview
export const selectPendingMedicalReviewAnalyses = (state: RootState) =>
  state.analysesOverview.markedMedicalReview
export const selectFinalizedAnalyses = (state: RootState) =>
  state.analysesOverview.finalized

export const selectOwnOngoingAnalyses = createSelector(
  (state: RootState) => state.analysesOverview.ongoing,
  (state: RootState) => selectCurrentUser(state)?.id,
  (ongoing, userId) =>
    ongoing.filter(
      (analysis) => analysis.overviewInterpretations.at(-1)?.userId === userId,
    ),
)
export const selectOthersOngoingAnalyses = createSelector(
  (state: RootState) => state.analysesOverview.ongoing,
  (state: RootState) => selectCurrentUser(state)?.id,
  (ongoing, userId) =>
    ongoing.filter(
      (analysis) => analysis.overviewInterpretations.at(-1)?.userId !== userId,
    ),
)

export default analysesSlice.reducer
