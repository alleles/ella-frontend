import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { AnalysisInterpretation, mapFromApi } from 'types/store/AnalysisInterpretation'

const initialState: Record<number, AnalysisInterpretation> = {}

const fetchAnalysisInterpretationsByAnalysisId = createAsyncThunk<
  Record<number, AnalysisInterpretation>,
  number
>(
  'analysisInterpretation/fetchAnalysisInterpretationsByAnalysisId',
  async (analysisId) => {
    const apiInterpretations = await API.AnalysisInterpretations.get(analysisId).then(
      (result) => result.data,
    )
    const interpretations = apiInterpretations.reduce(
      (previous, current) => ({
        ...previous,
        [current.id]: mapFromApi(analysisId, current),
      }),
      {} as Record<number, AnalysisInterpretation>,
    )
    return interpretations
  },
)

const analysisInterpretationSlice = createSlice({
  name: 'analysisInterpretations',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      fetchAnalysisInterpretationsByAnalysisId.fulfilled,
      (state, action) => {
        Object.assign(state, action.payload)
      },
    )
  },
})

const selectAnalysisInterpretationById = (
  state: RootState,
  { analysisInterpretationId }: { analysisInterpretationId: number },
): AnalysisInterpretation | undefined =>
  state.analysisInterpretations[analysisInterpretationId]

const selectDefaultAnalysisInterpretation = (
  state: RootState,
  { analysisId }: { analysisId: number },
) => {
  const possibleInterpretations = Object.values(state.analysisInterpretations).filter(
    (analysisInterpretation) => analysisInterpretation.analysisId === analysisId,
  )
  const defaultInterpretation = possibleInterpretations.sort((a, b) => b.id - a.id)[0]
  return defaultInterpretation
}

export {
  fetchAnalysisInterpretationsByAnalysisId,
  selectAnalysisInterpretationById,
  selectDefaultAnalysisInterpretation,
}

export default analysisInterpretationSlice.reducer
