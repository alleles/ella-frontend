Feature: Allele table
  Background:
    Given state "test-bdd"
    And I am logged in as "testuser4"

  Scenario: Selecting alleles
    When I view the analysis with id 2
    And I select the allele row 0 from the allele table
    Then I should see "- REF (C):"
    When I select the allele row 1 from the allele table
    Then I should see "- REF (A):"
    When I select the allele row 2 from the allele table
    Then I should see "- REF (G):"

  Scenario: Sorting and changing views in allele table
    When I view the analysis with id 1

    # Default sort: POSITION
    Then the value of the cell in row 0, column 0 in the table with id "allele-table" should be "1:161518333"

    # Sort by INH
    When I press the element with xpath "//th[contains(text(), 'Inh')]"
    Then the value of the cell in row 0, column 1 in the table with id "allele-table" should be "AD"

    # Sort by GENES
    When I press the element with xpath "//th[contains(text(), 'Genes')]"
    Then the value of the cell in row 0, column 2 in the table with id "allele-table" should be "AMER1 (2/2)"

    # Sort by CSQ
    When I press the element with xpath "//th[contains(text(), 'CSQ')]"
    Then the value of the cell in row 0, column 5 in the table with id "allele-table" should be "splice_acceptor"
    And the value of the cell in row 13, column 5 in the table with id "allele-table" should be "splice_region, intron"

    # Sort by QUAL
    When I press the element with xpath "//th[contains(text(), 'Qual')]"
    Then the value of the cell in row 0, column 7 in the table with id "allele-table" should be "15"

    # Sort by DP
    When I press the element with xpath "//th[contains(text(), 'Depth')]"
    Then the value of the cell in row 0, column 8 in the table with id "allele-table" should be "P: 3 | M: 16 | F: 8"

    # Sort by RATIO
    When I press the element with xpath "//th[contains(text(), 'Ratio')]"
    Then the value of the cell in row 0, column 9 in the table with id "allele-table" should be "P: 0.26 | M: ? | F: 0.03"

    # Sort by HI.FREQ
    When I press the element with xpath "//th[contains(text(), 'Hi. freq.')]"
    Then the value of the cell in row 4, column 10 in the table with id "allele-table" should be "2.318e-5"

    # Sort by HI.COUNT
    When I press the element with xpath "//th[contains(text(), 'Hi. count')]"
    Then the value of the cell in row 4, column 11 in the table with id "allele-table" should be "1"

    # Sort by EXTERNAL
    When I press the element with xpath "//th[contains(text(), 'External')]"
    Then the value of the cell in row 10, column 12 in the table with id "allele-table" should be "CLINVAR"

    # Sort by CLASS; also tests the Classified tab
    ## Return to default sort
    When I start the interpretation
    And I press the element with xpath "//th[contains(text(), 'Position')]"

    ## Classify two variants such that default sort (position) is different from Class sort
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 4"
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 3"

    ## Check sorting
    And I select the "Classified" tab
    And I press the element with xpath "//th[contains(text(), 'Class')]"
    Then the value of the cell in row 0, column 13 in the table with id "allele-table" should be "4"
    # TODO: pending #189 (re-evalute still valid classification); Check old --> new classification
    # When I select the allele row 0 from the allele table
    # And I submit the class as "Class 2"
    # Then the value of the cell in row 0, column 13 in the table with id "allele-table" should be "3 -> 2"

    # Check list view
    When I expand the bottom panel
    Then I should see "Unclassified Variants (18)"
    And I should see "Classified Variants (2)"

# TODO: Test Not Relevant and Technical tabs; pending #152
