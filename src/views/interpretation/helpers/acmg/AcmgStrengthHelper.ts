import { firstBy } from 'thenby'

import { AcmgCode, AcmgStrength, AcmgType, acmgTypes } from 'types/store/Acmg'
import { Config } from 'types/store/Config'

export class AcmgStrengthHelper {
  static sortCodesByTypeStrength(
    codes: AcmgCode[],
    config: Config,
  ): {
    other: AcmgCode[]
    pathogenic: AcmgCode[]
    benign: AcmgCode[]
  } {
    const codeStrs = codes.map((c) => c.code)
    const sortedStrs = this.sortCodeStrByTypeStrength(codeStrs, config)
    const sortedCodes: { [key in AcmgType]: AcmgCode[] } = {
      other: [],
      pathogenic: [],
      benign: [],
    }
    Object.keys(sortedCodes).forEach((category) => {
      codes.forEach((c) => {
        if (sortedStrs[category].includes(c.code)) {
          sortedCodes[category].push(c)
        }
      })
    })

    acmgTypes.forEach((category) => {
      sortedCodes[category].sort(
        firstBy<AcmgCode>((x) => sortedStrs[category].indexOf(x.code)),
      )
    })

    return sortedCodes
  }

  static sortCodeStrByTypeStrength(
    codes: string[],
    config: Config,
  ): { [key in AcmgType]: string[] } {
    const result: { [key in AcmgType]: string[] } = {
      other: [],
      pathogenic: [],
      benign: [],
    }
    Object.keys(result).forEach((t) => {
      // Map codes to group (benign/pathogenic)
      codes.forEach((c) => {
        if (config.acmg.codes[t].some((e) => c.startsWith(e))) {
          result[t].push(c)
        }
      })

      // Sort the codes
      result[t].sort((a, b) => {
        // Find the difference in index between the codes
        const aIdx = config.acmg.codes[t].findIndex((elem) => a.startsWith(elem))
        const bIdx = config.acmg.codes[t].findIndex((elem) => b.startsWith(elem))

        // If same prefix, sort on text itself
        if (aIdx === bIdx) {
          return a.localeCompare(b)
        }
        if (t === 'benign') {
          return bIdx - aIdx
        }
        return aIdx - bIdx
      })
    })
    return result
  }

  static isModifiedStrength(codeStr: string): boolean {
    return !codeStr.startsWith('REQ') && codeStr.includes('x')
  }

  static getCodeStrength(codeStr: string, config: Config): AcmgStrength {
    let codeStrength: string | null = null
    Object.values(config.acmg.codes).forEach((strengths) => {
      strengths.forEach((s) => {
        if (codeStr.startsWith(s)) {
          codeStrength = s
        }
      })
    })
    if (!codeStrength) {
      throw new Error('No code strength found')
    }

    return codeStrength
  }
}
