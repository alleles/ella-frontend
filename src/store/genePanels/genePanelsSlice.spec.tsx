import { render } from '@testing-library/react'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import React from 'react'
import { Provider } from 'react-redux'

import API from 'app/API'
import { AuthProvider } from 'app/auth'

import genePanelsReducer, {
  fetchGenePanels,
  selectGenePanels,
} from 'store/genePanels/genePanelsSlice'
import { store } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { GenePanel } from 'types/store/GenePanel'

import { flushPromises } from 'utils/testing'

import Login from 'views/login/Login'

function buildGenePanelResponse(
  override: Partial<ApiTypes.GenePanelResponse> = {},
): ApiTypes.GenePanelResponse {
  return { name: '', version: '', genes: [], ...override }
}

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => jest.fn(),
  useLocation: () => jest.fn(),
}))

jest.mock('react', () => ({
  ...(jest.requireActual('react') as any),
  useState: () => ['', () => null],
}))

const mock = new MockAdapter(axios)

const mockGenePanels = () => {
  mock
    .onGet(API.GenePanels.path)
    .reply(200, [
      buildGenePanelResponse({ name: '1', version: '1' }),
      buildGenePanelResponse({ name: '2', version: '2' }),
    ])

  mock.onGet(API.GenePanel.path('1', '1')).reply(200, {
    ...buildGenePanelResponse({ name: '1', version: '1' }),
    detailed: true,
  })

  mock.onGet(API.GenePanel.path('2', '2')).reply(200, {
    ...buildGenePanelResponse({ name: '2', version: '2' }),
    detailed: true,
  })
}

describe('gene panels reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(genePanelsReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle loading gene panels', async () => {
    mockGenePanels()
    const { dispatch } = store
    await dispatch(fetchGenePanels())

    expect(selectGenePanels(store.getState())).toEqual<Record<string, GenePanel>>({
      '1_1': {
        name: '1',
        version: '1',
        genes: [],
        official: null,
      },
      '2_2': {
        name: '2',
        version: '2',
        genes: [],
        official: null,
      },
    })
  })

  it('should load gene panels on login', async () => {
    mock.onGet(API.CurrentUser.path).reply(200, {})

    mockGenePanels()

    render(
      <Provider store={store}>
        <AuthProvider>
          <Login />
        </AuthProvider>
      </Provider>,
    )

    await flushPromises()

    expect(selectGenePanels(store.getState())).toEqual<Record<string, GenePanel>>({
      '1_1': { name: '1', version: '1', genes: [], official: null },
      '2_2': { name: '2', version: '2', genes: [], official: null },
    })
  })
})
