import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import { store } from 'store/store'
import usersReducer, {
  fetchCurrentUserId,
  fetchUsers,
  selectUsersInOwnGroup,
} from 'store/users/usersSlice'

import { User } from 'types/store/User'

const mock = new MockAdapter(axios)

describe('users reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(usersReducer(undefined, { type: 'unknown' })).toEqual({
      currentUserId: null,
      users: {},
    })
  })

  it('should handle fetching the users from the backend', async () => {
    mock
      .onGet(API.Users.path)
      .reply(200, [{ id: 1, first_name: 'foo', last_name: 'bar' }])

    const dispatch = jest.fn()
    await fetchUsers()(dispatch, () => undefined, {})
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          1: expect.objectContaining<Partial<User>>({
            firstName: 'foo',
            lastName: 'bar',
          }),
        },
      }),
    )
  })

  it('should handle fetching the current user from the backend', async () => {
    mock
      .onGet(API.CurrentUser.path)
      .reply(200, { id: 1, first_name: 'foo', last_name: 'bar' })

    const dispatch = jest.fn()
    await fetchCurrentUserId()(dispatch, () => undefined, {})
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: 1,
      }),
    )
  })

  it('should handle getting other users in the same group as the user', async () => {
    mock
      .onGet(API.CurrentUser.path)
      .reply(200, { id: 1, group: { id: 1, name: 'group1' } })

    mock.onGet(API.Users.path).reply(200, [
      { id: 1, first_name: 'dabla', group: { id: 1, name: 'group1' } },
      { id: 2, first_name: 'foo', group: { id: 1, name: 'group1' } },
      { id: 3, first_name: 'bar', group: { id: 2, name: 'group2' } },
      { id: 4, first_name: 'baz', group: { id: 1, name: 'group1' } },
    ])

    await store.dispatch(fetchCurrentUserId())
    await store.dispatch(fetchUsers())

    expect(selectUsersInOwnGroup(store.getState())).toMatchObject([
      expect.objectContaining({ firstName: 'foo', group: { id: 1, name: 'group1' } }),
      expect.objectContaining({ firstName: 'baz', group: { id: 1, name: 'group1' } }),
    ])
  })
})
