import { buildAnnotation } from 'store/interpretation/annotationSlice.spec'
import { selectAnnotationFilteredTranscripts } from 'store/selectors/genePanelAnnotation'
import { store } from 'store/store'

import { GeneAlternative, GenePanel } from 'types/store/GenePanel'

function buildGene(override: Partial<GeneAlternative>): GeneAlternative {
  return {
    hgncId: 0,
    hgncSymbol: '',
    phenotypes: [],
    transcripts: [],
    ...override,
  }
}

function buildGenePanel(override: Partial<GenePanel>): GenePanel {
  return {
    name: '',
    version: '',
    official: null,
    genes: [],
    ...override,
  }
}

describe('select filtered transcripts', () => {
  const stateFactory = (annotationTranscripts) => ({
    ...store.getState(),
    genePanels: {
      Dabla_v01: buildGenePanel({
        name: 'Dabla',
        version: 'v01',
        genes: [
          buildGene({
            transcripts: [
              { id: 1, transcriptName: 'NM_001.1' },
              { id: 2, transcriptName: 'NM_001.3' },
              { id: 3, transcriptName: 'NM_002.1' },
              { id: 4, transcriptName: 'NM_003.3' },
              { id: 5, transcriptName: 'NM_004.1' },
            ],
          }),
        ],
      }),
    },
    annotations: {
      1: buildAnnotation({
        transcripts: annotationTranscripts.map((txName) => ({
          transcript: txName,
        })),
      }),
    },
  })

  it('should return transcript matched on base', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_001.2']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_001.2'])
  })

  it('should NOT return transcript matched on base if identical match for base exists', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_001.1', 'NM_001.2']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_001.1'])
  })

  it('should return transcript matched on base, even if other transcript match identically', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_001.3', 'NM_002.2']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_001.3', 'NM_002.2'])
  })

  it('should discard transcripts not found in gene panel', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_001.1', 'NM_999.1']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_001.1'])
  })

  it('should not discard transcripts not found in gene panel if no other match is found', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_999.1']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_999.1'])
  })

  it('should return exact matches when base matches gene panel', async () => {
    expect(
      selectAnnotationFilteredTranscripts(
        stateFactory(['NM_001.1', 'NM_001.2', 'NM_001.3', 'NM_001.5']),
        {
          annotationId: 1,
          genePanelName: 'Dabla',
          genePanelVersion: 'v01',
        },
      ),
    ).toEqual(['NM_001.1', 'NM_001.3'])
  })

  it('should return highest version when no transcripts match gene panel', async () => {
    expect(
      selectAnnotationFilteredTranscripts(stateFactory(['NM_001.8', 'NM_001.9']), {
        annotationId: 1,
        genePanelName: 'Dabla',
        genePanelVersion: 'v01',
      }),
    ).toEqual(['NM_001.9'])
  })

  it('caches results from selectAnnotationFilteredTranscripts', () => {
    const state = stateFactory(['NM_001.8', 'NM_001.9'])
    const computations = selectAnnotationFilteredTranscripts.recomputations()
    selectAnnotationFilteredTranscripts.clearCache()

    selectAnnotationFilteredTranscripts(state, {
      annotationId: 1,
      genePanelName: 'Dabla',
      genePanelVersion: 'v01',
    })
    expect(selectAnnotationFilteredTranscripts.recomputations()).toEqual(computations + 1)

    selectAnnotationFilteredTranscripts(state, {
      annotationId: 1,
      genePanelName: 'Dabla',
      genePanelVersion: 'v01',
    })
    expect(selectAnnotationFilteredTranscripts.recomputations()).toEqual(computations + 1)

    selectAnnotationFilteredTranscripts(state, {
      annotationId: 2,
      genePanelName: 'Dabla',
      genePanelVersion: 'v01',
    })
    expect(selectAnnotationFilteredTranscripts.recomputations()).toEqual(computations + 2)

    selectAnnotationFilteredTranscripts(state, {
      annotationId: 1,
      genePanelName: 'Dabla',
      genePanelVersion: 'v01',
    })
    expect(selectAnnotationFilteredTranscripts.recomputations()).toEqual(computations + 2)
  })
})
