import React from 'react'

export default function Separator({ label }: { label: string }) {
  return (
    <div className="mb-6 bg-ellablue-lightest px-2 py-1 text-sm tracking-wider text-ellagray-900">
      {label}
    </div>
  )
}
