import React, { HTMLProps } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { createAlert } from 'store/alerts/alertsSlice'
import { selectAppConfig } from 'store/config/configSlice'

export default function Anchor(props: HTMLProps<HTMLAnchorElement>) {
  const { href, children, ...rest } = props
  const dispatch = useDispatch()
  const appConfig = useSelector(selectAppConfig)
  const useClipboard = appConfig.links_to_clipboard

  const onClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    if (useClipboard) {
      e.preventDefault()
      e.stopPropagation()
      navigator.clipboard.writeText(href || '')
      dispatch(
        createAlert({
          body: 'Copied link to clipboard',
          title: 'Link copied',
          type: 'info',
        }),
      )
    }
  }

  return (
    <a
      className="text-ellablue-darker hover:text-ellablue-darkest"
      href={href}
      target="_blank"
      rel="noopener noreferrer"
      {...rest}
      onClick={onClick}
    >
      {children}
    </a>
  )
}
