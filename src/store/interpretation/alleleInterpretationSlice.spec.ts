import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import alleleInterpretationReducer, {
  processAndStoreAlleleInterpretation,
  selectAlleleInterpretationById,
} from 'store/interpretation/alleleInterpretationSlice'
import { store } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleInterpretation } from 'types/store/AlleleInterpretation'

const mock = new MockAdapter(axios)

function buildAlleleInterpretation(
  override: Partial<AlleleInterpretation>,
): AlleleInterpretation {
  return {
    id: 0,
    alleleId: 1,
    dateCreated: '',
    dateLastUpdate: '',
    status: 'Not started',
    workflowStatus: 'Interpretation',
    genePanelName: '',
    genePanelVersion: '',
    state: {} as AlleleInterpretation['state'],
    userState: {} as AlleleInterpretation['userState'],
    finalized: false,
    userId: 0,
    ...override,
  }
}

describe('allele interpretation reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(alleleInterpretationReducer(undefined, { type: 'unknown' })).toEqual({})
  })
  it('should handle processing allele interpretations', async () => {
    mock.onGet(API.AlleleInterpretationAllele.path(10, 1)).reply(200, [
      {
        annotation: {
          annotation_id: 1,
          custom_annotation_id: 2,
        },
      },
    ])

    const inputInterpretation: ApiTypes.AlleleInterpretation = {
      id: 1, // Removed
      genepanel_name: 'foo',
      genepanel_version: 'v01',
      status: 'Not started',
      workflow_status: 'Interpretation',
      date_last_update: '2022-01-10',
      date_created: '1970-01-01',
      state: {
        allele: {
          10: {
            allele_id: 10, // REMOVED
            alleleassessment: {
              reuseCheckedId: 2, // Renamed to alleleAssessmentId
            },
            allelereport: {
              copiedFromId: 3, // Renamed to alleleReportId
              evaluation: { comment: '' },
            },
            // Renamed to referenceAssessmentIds
            referenceassessments: [
              {
                id: 1,
                allele_id: 1,
                reference_id: 1,
                evaluation: {},
              },
              {
                id: 2,
                allele_id: 1,
                reference_id: 2,
                evaluation: {},
              },
            ],
          },
        },
      },
      user_state: {
        allele: {
          10: {
            allele_id: 'REMOVED',
            foo: 'bar',
          },
        },
      },
    }

    const outputInterpretation: Record<number, AlleleInterpretation> = {
      1: {
        id: 1,
        alleleId: 2,
        dateCreated: '1970-01-01',
        dateLastUpdate: '2022-01-10',
        finalized: false,
        genePanelName: 'foo',
        genePanelVersion: 'v01',
        state: {
          allele: {
            '10': {
              alleleAssessment: {
                attachmentIds: [],
                classification: null,
                evaluation: {
                  acmg: {
                    included: [],
                    suggested: [],
                    suggestedClassification: undefined,
                  },
                  classification: {
                    comment: '',
                  },
                  external: {
                    comment: '',
                  },
                  frequency: {
                    comment: '',
                  },
                  prediction: {
                    comment: '',
                  },
                  reference: {
                    comment: '',
                  },
                  similar: {
                    comment: '',
                  },
                },
              },
              alleleAssessmentId: 2,
              alleleReport: {
                comment: '',
              },
              alleleReportId: 3,
              referenceAssessmentIds: [1, 2],
              referenceAssessments: [],
              reuseAlleleAssessment: false,
              reuseAlleleReport: false,
            },
          },
        },
        status: 'Not started',
        userId: null,
        userState: {},
        workflowStatus: 'Interpretation',
      },
    }

    const dispatch = jest.fn()
    await processAndStoreAlleleInterpretation({
      interpretationFromApi: inputInterpretation,
      alleleId: 2,
    })(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: outputInterpretation,
      }),
    )
  })

  it('should handle getting allele interpretations by id', async () => {
    const state = {
      ...store.getState(),
      alleleInterpretations: {
        1: buildAlleleInterpretation({ genePanelName: 'foo' }),
        2: buildAlleleInterpretation({ genePanelName: 'bar' }),
      },
    }

    expect(
      selectAlleleInterpretationById(state, {
        alleleInterpretationId: 1,
      }),
    ).toEqual(buildAlleleInterpretation({ genePanelName: 'foo' }))

    expect(
      selectAlleleInterpretationById(state, {
        alleleInterpretationId: 2,
      }),
    ).toEqual(buildAlleleInterpretation({ genePanelName: 'bar' }))
    expect(
      selectAlleleInterpretationById(state, {
        alleleInterpretationId: 3,
      }),
    ).toEqual(undefined)
  })
})
