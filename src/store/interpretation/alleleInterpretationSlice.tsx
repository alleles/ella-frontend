import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import {
  processAndStoreAnnotation,
  selectAnnotationByAnnotationId,
} from 'store/interpretation/annotationSlice'
import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleInterpretation, mapFromApi } from 'types/store/AlleleInterpretation'
import { Reference } from 'types/store/Reference'

const initialState: Record<number, AlleleInterpretation> = {}

const fetchAlleleInterpretationsForAlleleId = createAsyncThunk<
  Record<number, AlleleInterpretation>,
  number
>(
  'alleleInterpretation/fetchAlleleInterpretationsForAlleleId',
  async (alleleId, { dispatch }) => {
    const apiAlleleInterpretations = await API.AlleleInterpretationList.get(
      alleleId,
    ).then((result) => result.data)
    const alleleInterpretations = apiAlleleInterpretations.reduce(
      (prev, current) => ({
        ...prev,
        [current.id]: mapFromApi(current, alleleId),
      }),
      {} as Record<number, AlleleInterpretation>,
    )

    return alleleInterpretations
  },
)

const processAndStoreAlleleInterpretation = createAsyncThunk<
  Record<number, AlleleInterpretation>,
  { interpretationFromApi: ApiTypes.AlleleInterpretation; alleleId: number }
>(
  'alleleInterpretation/processAndStoreAlleleInterpretation',
  async ({ interpretationFromApi, alleleId }, { dispatch }) => {
    const interpretation = mapFromApi(interpretationFromApi, alleleId)

    const { id } = interpretationFromApi

    for (const alleleId of Object.keys(interpretation.state.allele)) {
      // This is properly messed up, but we need it to fetch annotationId
      // and customAnnotationId. Since we do not have a separate endpoint for
      // fetching annotations, this must be dispatched from here. Custom annotation
      // is fetched through a separate dispatch in alleleInterpretationProxy
      const interpretationAllele = await API.AlleleInterpretationAllele.get(
        parseInt(alleleId, 10),
        id,
      ).then((result) => result.data[0])

      if (interpretationAllele.annotation) {
        dispatch(
          processAndStoreAnnotation({
            annotationsFromApi: interpretationAllele.annotation,
            alleleId: parseInt(alleleId, 10),
          }),
        )
      }
    }

    return { [id]: interpretation }
  },
)

const alleleInterpretationSlice = createSlice({
  name: 'alleleInterpretations',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(processAndStoreAlleleInterpretation.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
    builder.addCase(fetchAlleleInterpretationsForAlleleId.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectAlleleInterpretationById = (
  state: RootState,
  { alleleInterpretationId }: { alleleInterpretationId: number },
): AlleleInterpretation | undefined => state.alleleInterpretations[alleleInterpretationId]

const selectDefaultAlleleInterpretationByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
): AlleleInterpretation | undefined =>
  Object.values(state.alleleInterpretations)
    .filter((interpretation) => interpretation.alleleId === alleleId)
    .sort((interpretationA, interpretationB) => {
      return interpretationA.id - interpretationB.id
    })
    .at(-1)

export {
  fetchAlleleInterpretationsForAlleleId,
  processAndStoreAlleleInterpretation,
  selectAlleleInterpretationById,
  selectDefaultAlleleInterpretationByAlleleId,
}

export default alleleInterpretationSlice.reducer
