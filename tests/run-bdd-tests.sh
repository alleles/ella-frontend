#!/bin/bash -e

if [[ -z $CI ]] || [[ $- = *i* ]]; then
    TTY_MODE=
else
    TTY_MODE=-T
fi

supervisor() {
    # needs -T or it will fail even if the command itself succedes
    docker-compose exec -T backend supervisorctl -c ops/dev/supervisor.cfg "$@"
}

check_db_reset() {
    supervisor status dbreset | grep -q EXITED
}

check_ui() {
    # craco build/start can take a while in CI, so check for it to return 200
    docker-compose exec ${TTY_MODE} nginx curl --max-time 5 -sfI http://frontend:3000 >/dev/null
}

ts_echo() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $*"
}

fetch_logs() {
    ts_echo "Tests failed, dumping container logs"
    mkdir -p logs/
    docker-compose logs -t --no-color >logs/containers.log
    docker-compose ps -q | xargs docker inspect >logs/configs.json
    docker-compose ps -a >logs/compose_state.log
    (docker info || true) >logs/docker_info.log
    # old version of docker-compose doesn't support cp
    docker cp ella.backend:/logs logs/backend
}

trap fetch_logs ERR

DB_OK=
UI_OK=
# supervisor start dbreset
for i in {1..30}; do
    ts_echo "Checking if backend is ready... Attempt ${i}"
    if [[ -z $DB_OK ]]; then
        check_db_reset && DB_OK=1
        [[ -n $DB_OK ]] && ts_echo "dbreset is finished" || ts_echo "dbreset still running"
    fi
    if [[ -z $UI_OK ]]; then
        check_ui && UI_OK=1
        [[ -n $UI_OK ]] && ts_echo "UI is ready" || ts_echo "UI still starting"
    fi
    if [[ -n $DB_OK && -n $UI_OK ]]; then
        ts_echo "Backend is ready"
        break
    fi
    if [[ $i -eq 30 ]]; then
        ts_echo "Did not identify a successful db reset on ella.backend"
        exit 1
    fi
    sleep 10
done

declare -a BEHAVE_ARGS
if [[ -n "$*" ]]; then
    BEHAVE_ARGS+=("$@")
fi
BEHAVE_ARGS+=(tests/)

docker-compose exec ${TTY_MODE} -e CI="${CI}" -e SITE_URL="https://app.local.allel.es:8080" frontend behave "${BEHAVE_ARGS[@]}"

# always save logs
fetch_logs >/dev/null
