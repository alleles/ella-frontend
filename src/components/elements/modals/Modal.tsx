import React, { useEffect } from 'react'

interface Props {
  children
  onClose: () => void
}

type KeyCode = 'Enter' | 'Escape'

function useKeyPress(callback: () => void, keyCodes: KeyCode[]): void {
  const handler = (event: KeyboardEvent) => {
    if (keyCodes.includes(event.key as KeyCode)) {
      callback()
    }
  }

  useEffect(() => {
    window.addEventListener('keyup', handler)
    return () => {
      window.removeEventListener('keyup', handler)
    }
  }, [callback])
}

export default function Modal({ children, onClose }: Props) {
  useKeyPress(onClose, ['Escape'])

  return (
    <div
      className="modal-backdrop fixed inset-0 z-20 h-full w-full cursor-default overflow-y-auto bg-ellagray-700/50"
      onClick={(event) => {
        if ((event.target as Element).classList.contains('modal-backdrop')) {
          onClose()
        }
      }}
    >
      <div className="relative top-20 mx-auto w-fit rounded-md border border-black/5 bg-white p-5 shadow-lg">
        <div className="mt-3">{children}</div>
      </div>
    </div>
  )
}
