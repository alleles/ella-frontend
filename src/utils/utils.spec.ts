import { deepMerge } from 'utils/general'

describe('deep merge', () => {
  it('should handle merging objects', () => {
    const obj = {
      foo: 'foo',
      bar: {
        baz: 'baz',
      },
    }
    deepMerge(obj, { bar: { baz: 'baz2' } })
    expect(obj.bar.baz).toEqual('baz2')
  })
  it('should handle merging arrays in objects', () => {
    const obj = {
      foo: 'foo',
      bar: {
        baz: [1, 2, 3],
      },
    }
    deepMerge(obj, { bar: { baz: [1, 2] } })
    expect(obj.bar.baz).toEqual([1, 2])
  })
})
