import React from 'react'
import { useSelector } from 'react-redux'

import Menu from 'components/elements/DropDownMenu'

import { selectConfigClassificationOptions } from 'store/config/configSlice'

import { AlleleAssessmentClassification } from 'types/api/Pydantic'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

interface ClassificationDropdownProps {
  disabled: boolean
}

export default function ClassificationDropdown({
  disabled,
}: ClassificationDropdownProps) {
  const { alleleState, setAlleleState } = useInterpretationStateContext()

  // get available classifications
  const configClassificationOptions = useSelector(selectConfigClassificationOptions)
  // add a null/empty classification (doesn't seem to be part of AlleleAssessmentClassification)
  const classificationOptions = [
    {
      name: 'Select class',
      value: null,
    },
    ...configClassificationOptions,
  ]

  // get active classification
  const classificationState = alleleState?.alleleAssessment?.classification
  const selectedClassification = classificationOptions.find(
    (classOption) => classOption.value === classificationState,
  )

  return (
    <Menu
      id="classification-dropdown"
      title={selectedClassification?.name || 'internal-error'}
    >
      {!disabled &&
        classificationOptions.map((classOption) => (
          <Menu.MenuItem
            title={classOption.name}
            key={classOption.value}
            onClick={() => {
              setAlleleState({
                alleleAssessment: {
                  classification: classOption.value as AlleleAssessmentClassification,
                },
              })
            }}
          />
        ))}
    </Menu>
  )
}
