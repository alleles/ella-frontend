import React, { useState } from 'react'

import { AsyncSelect } from 'components/elements/Select'
import { fetchUserOptions } from 'components/navbar/search/searchLogic'

import { User } from 'types/store/User'

interface Props {
  onChange: (newUser: User) => void
}

export default function UserSelect({ onChange }: Props) {
  const [user, setUser] = useState<User | null>(null)

  return (
    <AsyncSelect
      onChange={(v) => {
        setUser(v)
        onChange(v)
      }}
      loadOptions={fetchUserOptions}
      placeholder="Search user"
      selected={user}
    />
  )
}
