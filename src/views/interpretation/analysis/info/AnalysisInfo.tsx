import { format } from 'date-fns'
import React from 'react'
import { useSelector } from 'react-redux'

import SectionBox from 'components/elements/SectionBox'

import { selectAnalysisById } from 'store/interpretation/analysisSlice'
import { RootState } from 'store/store'

import { Analysis } from 'types/store/Analysis'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import Report from 'views/interpretation/analysis/info/Report'
import Samples from 'views/interpretation/analysis/info/Samples'

export default function AnalysisInfo() {
  const { id } = useInterpretationStateContext()
  const analysis = useSelector((state: RootState) =>
    selectAnalysisById(state, { analysisId: id }),
  ) as Analysis

  const dateRequested = new Date(analysis.dateRequested as string)
  const dateImported = new Date(analysis.dateDeposited as string)

  return (
    <div className="grow">
      {analysis.warnings ? (
        <SectionBox title="Pipeline Warnings" warn>
          <div className="pl-2 pt-2">
            <Report report={analysis.warnings} />{' '}
          </div>
        </SectionBox>
      ) : null}
      <SectionBox title="Analysis Info" color="ellablue-lighter">
        <div className="pl-2">
          <div className="grid w-max grid-cols-2 pt-2 text-sm">
            <div className="font-semibold">Requested:</div>
            <div>{format(dateRequested, 'yyyy-MM-dd')}</div>
            <div className="font-semibold">Imported:</div>
            <div>{format(dateImported, 'yyyy-MM-dd HH:mm')}</div>
          </div>
          <Samples />
        </div>
      </SectionBox>
      <SectionBox title="Pipeline Report" color="ellablue-lighter">
        <div className="pl-2">
          <Report report={analysis.report || ''} />
        </div>
      </SectionBox>
    </div>
  )
}
