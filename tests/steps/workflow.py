from typing import Optional

from behave import when
from behaving.personas.persona import persona_vars


# Start untouched interpretation
@when("I start the interpretation")
def start_interpretation(context):
    context.execute_steps(
        """
        When I press "Start interpretation"
        Then I should see "Finish"
        And I should see "Save"
        """
    )


# Start any workflow and optionally change step
@when('I start "{workflow}"')
@when('I start "{workflow}" and send to "{to_workflow}"')
def start_workflow(context, workflow: str, to_workflow: Optional[str] = None):
    context.execute_steps(
        f"""
        When I press "Start {workflow.lower()}"
        Then I should see "Finish"
        And I should see "Save"
        """
    )
    if to_workflow:
        context.execute_steps(
            f"""
        When I press "Finish"
        And I press "{to_workflow}"
        And I press "OK"
        """
        )


# Finish already started workflow and change step
@when('I finish "{workflow}" and send to "{to_workflow}"')
def change_workflows(context, workflow: str, to_workflow: str):
    context.execute_steps(
        f"""
        When I press "Finish"
        And I press "{to_workflow}"
        And I press "OK"
        """
    )


# Save
@when("I save the interpretation")
def save_interpretation(context):
    context.execute_steps(
        """
        When I press "Save"
        Then I should see "Your interpretation was saved"
        """
    )


# Reassign
@persona_vars
@when("I reassign to me")
def reassign(context):
    context.execute_steps(
        """
        When I press "Reassign to me"
        Then I should see "This interpretation is currently assigned to another user. Are you sure you want to assign it to yourself?"
        When I press "Yes"
        Then I should see "Interpretation reassigned to you"
        And I should see "Finish"
        """
    )


# Add/change ACMG
@when('I add the ACMG criterion "{criterion}"')
def add_acmg_criterion(context, criterion: str):
    context.execute_steps(
        f"""
        When I press "Add ACMG"
        And I press "{criterion}"
        And I press "addACMG-{criterion}"
        And I press "Add ACMG"
        Then I should see "{criterion}"
        """
    )


@when('I strengthen the ACMG criterion "{criterion}"')
def strengthen_acmg_criterion(context, criterion: str):
    context.execute_steps(
        f"""
        When I press "strengthenACMG-{criterion}"
        """
    )


@when('I weaken the ACMG criterion "{criterion}"')
def weaken_acmg_criterion(context, criterion: str):
    context.execute_steps(
        f"""
        When I press "weakenACMG-{criterion}"
        """
    )


@when('I remove the ACMG criterion "{criterion}"')
def remove_acmg_criterion(context, criterion: str):
    context.execute_steps(
        f"""
        When I press "removeACMG-{criterion}"
        """
    )


# Add/submit classification
# Select class, Class 1, Class 2, Class 3, Class 4, Class 5, Risk factor, Drug response, Not provided
@when('I set the class to "{classification}"')
def set_class(context, classification: str):
    context.execute_steps(
        f"""
        When I press the element with id "classification-dropdown"
        And I press "{classification}"
        Then I should see "{classification}"
        But I should not see "Select class"
        And I should see "Submit"
        """
    )


@when('I submit the class as "{classification}"')
def submit_class(context, classification: str):
    context.execute_steps(
        f"""
        When I press the element with id "classification-dropdown"
        And I press "{classification}"
        And I press "Submit"
        Then I should see "{classification}"
        But I should not see "Submit"
        And I should not see "Add ACMG"
        """
    )


# TODO: Steps for testing comment field; setting nested variable needs changes in steps/editor.py to work


# @when('I verify that the "{criterion}" section is read-only')
# def check_readonly_section(context, criterion: str):
#     context.execute_steps(
#         f"""
#         Then I should see an element with xpath "//div[@id='{criterion}-comment']//div[@contenteditable='false']"
#         """
#     )


# @when('I add a comment to the "{criterion}" section')
# def add_section_comment(context, criterion: str):
#     context.execute_steps(
#         f"""
#         When I type "Test {criterion} comment" into the editor with id "{criterion}-comment"
#         Then I should see "Test {criterion} comment"
#         """
#     )
