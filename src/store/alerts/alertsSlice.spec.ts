import { configureStore } from '@reduxjs/toolkit'

import alertsReducer, {
  DEFAULT_TIMEOUT,
  createAlert,
  removeAlert,
} from 'store/alerts/alertsSlice'

import { sleep } from 'utils/testing'

describe('alert reducer', () => {
  let store

  beforeEach(() => {
    store = configureStore({
      reducer: alertsReducer,
    })
  })

  it('should handle initial state', () => {
    expect(store.getState()).toEqual([])
  })

  it('should handle an alert being added to state with default values', async () => {
    const id = await store.dispatch(createAlert({ body: 'foo' })).then((r) => r.payload)

    expect(store.getState()).toEqual([
      {
        id,
        body: 'foo',
        type: 'info',
        timeout: DEFAULT_TIMEOUT,
      },
    ])
  })

  it('should handle an alert being added to state with non-default values', async () => {
    const id = await store
      .dispatch(createAlert({ body: 'foo', title: 'bar', type: 'warning', timeout: 0 }))
      .then((r) => r.payload)

    expect(store.getState()).toEqual([
      {
        id,
        body: 'foo',
        title: 'bar',
        type: 'warning',
        timeout: 0,
      },
    ])
  })

  it('should handle removing an alert', async () => {
    const id = await store.dispatch(createAlert({ body: 'foo' })).then((r) => r.payload)
    await store.dispatch(removeAlert(id))
    expect(store.getState()).toEqual([])
  })

  it('is should automaticlly remove expired alerts after a timeout', async () => {
    const id = await store
      .dispatch(createAlert({ body: 'foo', timeout: 300 }))
      .then((r) => r.payload)
    expect(store.getState()).toEqual([
      {
        id,
        body: 'foo',
        type: 'info',
        timeout: 300,
      },
    ])
    await sleep(400)
    expect(store.getState()).toEqual([])
  })
})
