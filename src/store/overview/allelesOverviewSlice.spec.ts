import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import {
  fetchAlleles,
  selectFinalizedAlleles,
  selectMarkedReviewAlleles,
  selectNotStartedAlleles,
  selectOngoingAlleles,
} from 'store/overview/allelesOverviewSlice'
import { store } from 'store/store'

function buildAlleleOverview(override) {
  return {
    allele: {
      annotation: {},
    },
    genepanel: {},
    interpretations: [],
    review_comment: 'ongoing',
    ...override,
  }
}

const mock = new MockAdapter(axios)

describe('alleles reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(store.getState().allelesOverview).toEqual({
      finalized: [],
      markedReview: [],
      notStarted: [],
      ongoing: [],
    })
  })

  it('Alleles from the backend should be sent to the store and then to the allele table', async () => {
    mock.onGet(API.NonFinalizedAlleles.path).reply(200, {
      ongoing: [buildAlleleOverview({ review_comment: 'ongoing' })],
      not_started: [buildAlleleOverview({ review_comment: 'not_started' })],
      marked_review: [buildAlleleOverview({ review_comment: 'marked_review' })],
    })
    mock
      .onGet(API.FinalizedAlleles.path)
      .reply(200, [buildAlleleOverview({ review_comment: 'finalized' })])

    const { dispatch } = store
    await dispatch(fetchAlleles())

    const state = store.getState()

    expect(selectOngoingAlleles(state).length).toBe(1)
    expect(selectOngoingAlleles(state)[0].reviewComment).toBe('ongoing')

    expect(selectNotStartedAlleles(state).length).toBe(1)
    expect(selectNotStartedAlleles(state)[0].reviewComment).toBe('not_started')

    expect(selectMarkedReviewAlleles(state).length).toBe(1)
    expect(selectMarkedReviewAlleles(state)[0].reviewComment).toBe('marked_review')

    expect(selectFinalizedAlleles(state).length).toBe(1)
    expect(selectFinalizedAlleles(state)[0].reviewComment).toBe('finalized')
  })
})
