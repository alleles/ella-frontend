import React from 'react'

function Palette() {
  return (
    <div className="grid grid-cols-1 gap-8 text-ellagray-900 font-mono bg-white p-6 rounded shadow text-xs">
      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLAblue</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7 gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellablue-lightest" />
              <div className="px-0.5">
                <div>ellablue-lightest</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellablue-lighter" />
              <div className="px-0.5">
                <div>ellablue-lighter</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellablue" />
              <div className="px-0.5">
                <div>ellablue</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellablue-darker" />
              <div className="px-0.5">
                <div>ellablue-darker</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellablue-darkest" />
              <div className="px-0.5">
                <div>ellablue-darkest</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLApurple</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellapurple-lightest" />
              <div className="px-0.5">
                <div>ellapurple-lightest</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellapurple-lighter" />
              <div className="px-0.5">
                <div>ellapurple-lighter</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellapurple" />
              <div className="px-0.5">
                <div>ellapurple</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellapurple-dark" />
              <div className="px-0.5">
                <div>ellapurple-dark</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLAgreen</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded" />
              <div className="px-0.5" />
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagreen-light" />
              <div className="px-0.5">
                <div>ellagreen-light</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded  bg-ellagreen" />
              <div className="px-0.5">
                <div>ellagreen</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded  bg-ellagreen-dark" />
              <div className="px-0.5">
                <div>ellagreen-dark</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLAred</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded" />
              <div className="px-0.5" />
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded  bg-ellared-light" />
              <div className="px-0.5">
                <div>ellared-light</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellared" />
              <div className="px-0.5">
                <div>ellared</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellared-dark" />
              <div className="px-0.5">
                <div>ellared-dark</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLAyellow</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7 gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded" />
              <div className="px-0.5" />
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellayellow-light" />
              <div className="px-0.5">
                <div>ellayellow-light</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellayellow" />
              <div className="px-0.5">
                <div>ellayellow</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellayellow-dark" />
              <div className="px-0.5">
                <div>ellayellow-dark</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">ELLAgray</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-50" />
              <div className="px-0.5">
                <div>ellagray-50</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-100" />
              <div className="px-0.5">
                <div>ellagray-100</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray" />
              <div className="px-0.5">
                <div>ellagray</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-400" />
              <div className="px-0.5">
                <div>ellagray-400</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-500" />
              <div className="px-0.5">
                <div>ellagray-500</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-700" />
              <div className="px-0.5">
                <div>ellagray-700</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-ellagray-900" />
              <div className="px-0.5">
                <div>ellagray-900</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pt-20">
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">Editor</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-editor-red" />
              <div className="px-0.5">
                <div>editor-red</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-editor-green" />
              <div className="px-0.5">
                <div>editor-green</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-editor-blue" />
              <div className="px-0.5">
                <div>editor-blue</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-editor-yellow" />
              <div className="px-0.5">
                <div>editor-yellow</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-editor-orange" />
              <div className="px-0.5">
                <div>editor-orange</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="flex flex-row space-y-0 space-x-4">
          <div className="w-32 shrink-0">
            <div className="h-10 flex flex-col justify-center">
              <div className="text-sm font-sans">B/W</div>
            </div>
          </div>
          <div className="min-w-0 flex-1 grid grid-cols-7  gap-x-4 gap-y-3">
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-white" />
              <div className="px-0.5">
                <div>white</div>
              </div>
            </div>
            <div className="space-y-1.5">
              <div className="h-10 w-full rounded bg-black" />
              <div className="px-0.5">
                <div>black</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Palette
