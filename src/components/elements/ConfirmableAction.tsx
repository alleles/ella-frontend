import React, { ReactNode, useCallback, useState } from 'react'

import Button from 'components/elements/Button'

interface Props {
  children: ReactNode
  confirmLabel?: string
  cancelLabel?: string
  onConfirm: () => void
  onCancel?: () => void
}

export default function ConfirmableAction({
  children,
  confirmLabel = 'Confirm',
  cancelLabel = 'Cancel',
  onConfirm = () => {},
  onCancel = () => {},
}: Props) {
  const [expanded, setExpanded] = useState(false)
  const confirm = useCallback(() => {
    setExpanded(false)
    onConfirm()
  }, [])
  const cancel = useCallback(() => {
    setExpanded(false)
    onCancel()
  }, [])

  if (expanded) {
    return (
      <div className="flex flex-row gap-2">
        <Button onClick={confirm}>{confirmLabel}</Button>
        <Button onClick={cancel} color="cancel">
          {cancelLabel}
        </Button>
      </div>
    )
  }
  return <Button onClick={() => setExpanded(true)}>{children}</Button>
}
