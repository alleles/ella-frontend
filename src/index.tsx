import App from 'App'
import 'index.css'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import * as serviceWorker from 'serviceWorker'

import { store } from 'store/store'

const container = document.getElementById('root')

if (container !== null) {
  const root = ReactDOM.createRoot(container)
  const strict = !import.meta.env.VITE_DISABLE_STRICT

  if (strict) {
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <App />
        </Provider>
      </React.StrictMode>,
    )
  } else {
    root.render(
      <Provider store={store}>
        <App />
      </Provider>,
    )
  }
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
