import React from 'react'

import API from 'app/API'

import logo from 'resources/logo_blue.svg'

import { store } from 'store/store'

type State = {
  error: Error | null
  errorInfo: React.ErrorInfo | null
}

export default class ErrorBoundary extends React.Component<
  React.PropsWithChildren,
  State
> {
  constructor(props) {
    super(props)
    this.state = { error: null, errorInfo: null }
  }

  static getDerivedStateFromError(error) {
    return { error }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ errorInfo })
    try {
      const state = store.getState()
      API.Exception.post(error, errorInfo, state)
      // eslint-disable-next-line no-empty
    } catch (e) {} // Well this is awkward...
  }

  render() {
    const { error, errorInfo } = this.state
    const { children } = this.props

    if (error) {
      return (
        <div className="flex min-h-full flex-col justify-center py-24">
          <div className="mx-auto w-full max-w-md">
            <img className="mx-auto h-12 w-auto" src={logo} alt="ELLA" />
            <h2 className="mt-6 text-center text-3xl font-semibold text-ellagray-900">
              Ooops.. Something went wrong.
            </h2>
          </div>

          <div className="mx-auto mt-8 w-full max-w-md">
            <h2 className="mt-6 text-center text-xl text-ellagray-900">Error details:</h2>
            <p className="mt-2 whitespace-pre-wrap text-center text-sm text-ellared-dark">
              {error.toString()}
            </p>
            {errorInfo && (
              <p className="mt-2 whitespace-pre-wrap text-center text-sm text-ellagray-700">
                {errorInfo.componentStack}
              </p>
            )}
          </div>
        </div>
      )
    }
    return children
  }
}
