import { ChevronDownIcon, ChevronRightIcon } from '@heroicons/react/24/solid'
import React, { Children, ReactElement, useEffect, useState } from 'react'

import { useLayout } from 'components/layout/LayoutContainer'

interface Props {
  id?
  title
  color?: string
  children?
  initCollapsed?: boolean
  warn?: boolean
  headerComponents?: ReactElement[]
}

export default function SectionBox({
  id,
  title,
  color,
  children,
  initCollapsed = false,
  warn = false,
  headerComponents,
}: Props): ReactElement {
  const [collapsed, setCollapsed] = useState(initCollapsed)
  const { sectionBoxAllCollapsed } = useLayout()

  useEffect(() => {
    if (sectionBoxAllCollapsed !== undefined) {
      setCollapsed(sectionBoxAllCollapsed)
    }
  }, [sectionBoxAllCollapsed])
  const toggleCollapsed = () => setCollapsed(!collapsed)

  // Add collapsed/setCollapsed state capabilities to editors
  const alteredChildren = Children.map(children, (child) => {
    if (child?.type.name !== 'Editor') {
      return child
    }
    return { ...child, props: { ...child.props, collapsed, setCollapsed } }
  })

  return (
    <section
      id={id}
      className={`${
        warn ? 'bg-ellared-light' : 'bg-white'
      } mb-4 rounded-sm text-ellagray-900 shadow`}
    >
      <div className="flex flex-row">
        <button
          className={`header rounded-br-sm rounded-tl-sm ${
            warn ? 'bg-ellared' : `bg-${color}`
          } flex cursor-pointer flex-row uppercase `}
          onClick={toggleCollapsed}
          type="button"
        >
          <div className="ml-2 h-6 w-6 py-1.5" aria-hidden="true">
            {collapsed ? <ChevronRightIcon /> : <ChevronDownIcon />}
          </div>
          <div className="mr-5 p-2 text-sm tracking-wider">{title}</div>
        </button>
        <div className="align-self-center space-evenly mt-1 mr-2 flex grow justify-end">
          {headerComponents}
        </div>
      </div>
      <div className={`mx-2 py-3 ${collapsed ? 'collapsed' : ''}`}>{alteredChildren}</div>
    </section>
  )
}
