import { CSSProperties } from 'react'

export function generateArrowStyles(
  placement: string,
  stylesFromPopper: CSSProperties,
): CSSProperties {
  const triangleStyles = {
    width: 0,
    height: 0,
    borderTop: '10px solid transparent',
    borderBottom: '10px solid transparent',
    borderRight: '10px solid #111827',
  }

  const arrowStyles: CSSProperties = {
    ...triangleStyles,
    ...stylesFromPopper,
  }

  if (placement === 'left') {
    arrowStyles.right = '-9px'
    arrowStyles.transform += ' rotate(180deg)'
  }

  if (placement === 'right') {
    arrowStyles.left = '-10px'
  }

  if (placement === 'top') {
    arrowStyles.bottom = '-15px'
    arrowStyles.transform += ' rotate(-90deg)'
  }

  if (placement === 'top-start') {
    arrowStyles.bottom = '-15px'
    arrowStyles.left = '8px'
    arrowStyles.transform += ' rotate(-90deg)'
  }

  if (placement === 'bottom') {
    arrowStyles.top = '-15px'
    arrowStyles.transform += ' rotate(90deg)'
  }

  if (placement === 'bottom-start') {
    arrowStyles.top = '-15px'
    arrowStyles.left = '8px'
    arrowStyles.transform += ' rotate(90deg)'
  }

  return arrowStyles
}
