import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/24/solid'
import React, { useMemo } from 'react'

interface PageProps {
  current?: boolean
  label: string
  onClick: () => void
}
interface PaginatorProps {
  current: number
  total: number
  pageSize?: number
  siblingCount?: number
  onChange: (page: number) => void
}

const range = (start, end) => {
  const length = end - start + 1
  return Array.from({ length }, (_, idx) => idx + start)
}

const ELLIPSIS = '\u2026'

function Page({ label, current, onClick }: PageProps) {
  if (label === ELLIPSIS) {
    return (
      <span className="relative inline-flex items-center border border-ellagray bg-white px-4 py-2 text-sm font-medium text-ellagray-700">
        ...
      </span>
    )
  }
  if (current)
    return (
      <button
        aria-current="page"
        className="relative z-10 inline-flex items-center border border-ellablue-darker bg-ellablue-lightest px-4 py-2 text-sm font-medium text-ellablue-darkest"
      >
        {label}
      </button>
    )
  return (
    <button
      className="relative inline-flex items-center border border-ellagray bg-white px-4 py-2 text-sm font-medium text-ellablue-darkest hover:bg-ellagray-100"
      onClick={onClick}
    >
      {label}{' '}
    </button>
  )
}

export default function Paginator({
  current,
  total,
  onChange,
  pageSize = 20,
  siblingCount = 1,
}: PaginatorProps) {
  const totalPageCount = Math.ceil(total / pageSize)

  const paginationRange = useMemo(() => {
    // Pages count is determined as siblingCount + firstPage + lastPage + current + 2*DOTS
    const totalPageNumbers = siblingCount + 5

    /*
      If the number of pages is less than the page numbers we want to show in our
      paginationComponent, we return the range [1..totalPageCount]
    */
    if (totalPageNumbers >= totalPageCount) {
      return range(1, totalPageCount)
    }

    const leftSiblingIndex = Math.max(current - siblingCount, 1)
    const rightSiblingIndex = Math.min(current + siblingCount, totalPageCount)

    /*
      We do not want to show dots if there is only one position left 
      after/before the left/right page count as that would lead to a change if our Pagination
      component size which we do not want
    */
    const shouldShowLeftDots = leftSiblingIndex > 2
    const shouldShowRightDots = rightSiblingIndex < totalPageCount - 2

    const firstPageIndex = 1
    const lastPageIndex = totalPageCount

    if (!shouldShowLeftDots && shouldShowRightDots) {
      const leftItemCount = 3 + 2 * siblingCount
      const leftRange = range(1, leftItemCount)

      return [...leftRange, ELLIPSIS, totalPageCount]
    }

    if (shouldShowLeftDots && !shouldShowRightDots) {
      const rightItemCount = 3 + 2 * siblingCount
      const rightRange = range(totalPageCount - rightItemCount + 1, totalPageCount)
      return [firstPageIndex, ELLIPSIS, ...rightRange]
    }

    const middleRange = range(leftSiblingIndex, rightSiblingIndex)
    return [firstPageIndex, ELLIPSIS, ...middleRange, ELLIPSIS, lastPageIndex]
  }, [total, pageSize, siblingCount, current])

  return (
    <div>
      <nav
        className="relative z-0 inline-flex -space-x-px rounded-md shadow-sm"
        aria-label="Pagination"
      >
        <button
          className="relative inline-flex items-center rounded-l-md border border-ellagray bg-white px-2 py-2 text-sm font-medium text-ellagray-500 hover:bg-ellagray-100"
          onClick={() => {
            if (current > 1) {
              onChange(current - 1)
            }
          }}
        >
          <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
        </button>
        {paginationRange.map((page, ind) => (
          <Page
            // eslint-disable-next-line react/no-array-index-key
            key={`${page}-${ind}`}
            label={page}
            current={page === current}
            onClick={() => onChange(page)}
          />
        ))}
        <button
          className="relative inline-flex items-center rounded-r-md border border-ellagray bg-white px-2 py-2 text-sm font-medium text-ellagray-500 hover:bg-ellagray-100"
          onClick={() => {
            if (current < totalPageCount) {
              onChange(current + 1)
            }
          }}
        >
          <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
        </button>
      </nav>
    </div>
  )
}
