import React, { MutableRefObject } from 'react'
import { useSelector } from 'react-redux'

import Tooltip from 'components/elements/tooltip/Tooltip'

import { selectConfig } from 'store/config/configSlice'

import { AcmgCode } from 'types/store/Acmg'

import { AcmgFormatter } from 'views/interpretation/helpers/acmg/AcmgFormatter'
import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'

interface Props {
  onClose: () => void
  code: AcmgCode
  tooltipAnchor: MutableRefObject<any>
}

export function AcmgTooltip({ code, onClose, tooltipAnchor }: Props) {
  const config = useSelector(selectConfig)
  const matches = AcmgHelper.getMatches(code, config)

  return (
    <Tooltip
      anchorElement={tooltipAnchor.current}
      onClose={onClose}
      placement="bottom-start"
    >
      <>
        <Tooltip.H1>
          {AcmgFormatter.formatCode(code.code.replaceAll('_', ' '), config)}
        </Tooltip.H1>
        <Tooltip.P>
          <Tooltip.H2>{AcmgHelper.getShortCriteria(code, config)}</Tooltip.H2>
        </Tooltip.P>

        {AcmgHelper.getRequiredFor(code, config).length > 0 && (
          <Tooltip.P>
            <Tooltip.H2>Required for:</Tooltip.H2>

            {AcmgHelper.getRequiredFor(code, config).map((required) => {
              const colorKey = AcmgFormatter.getColorKey(required)
              return (
                <div className={`acmg-${colorKey} mb-2`} key={required}>
                  <span className={`acmg-link-${colorKey}`}>{required}</span>
                </div>
              )
            })}
          </Tooltip.P>
        )}

        {AcmgHelper.getCriteria(code, config) && (
          <>
            <Tooltip.P>
              <Tooltip.H2>Details:</Tooltip.H2>
              <Tooltip.P>{AcmgHelper.getCriteria(code, config)}</Tooltip.P>
            </Tooltip.P>

            {AcmgHelper.getNotes(code, config).length > 0 && (
              <Tooltip.P>
                <Tooltip.H2>Notes:</Tooltip.H2>
                <ul className="acmg-notes">
                  {AcmgHelper.getNotes(code, config).map((note) => (
                    <li key={note}>{note}</li>
                  ))}
                </ul>
              </Tooltip.P>
            )}
          </>
        )}

        <Tooltip.P>
          <Tooltip.H2>Match:</Tooltip.H2>
          <div>
            <Tooltip.P>
              {matches.source} {matches.op} {matches.match}
            </Tooltip.P>
          </div>
        </Tooltip.P>
      </>
    </Tooltip>
  )
}
