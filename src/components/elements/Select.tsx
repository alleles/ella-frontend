/*
 * Exports wrapper around react-select's Select and AsyncSelect
 * Wrapped so that we can customize components with tailwind
 * See https://react-select.com/props#replacing-components for incomplete documentation on this
 *
 * Searchable and async select/listbox is not available in headlessui/tailwindui.
 * Follow thread here for developments: https://github.com/tailwindlabs/headlessui/discussions/626
 */
import React, { HTMLProps, LegacyRef, ReactElement, ReactNode, useState } from 'react'
import Select, {
  GroupBase,
  InputProps,
  MenuProps,
  MultiValue,
  SingleValue,
  components,
} from 'react-select'
import AsyncSelect from 'react-select/async'
import { SelectComponentsConfig } from 'react-select/dist/declarations/src/components'

type Option = { value: string; label: string }
type Group = GroupBase<Option>
type ComponentsConfig = SelectComponentsConfig<SingleValue<Option>, boolean, Group>

// Customized components
interface CustomOptionProps {
  innerRef: LegacyRef<HTMLDivElement>
  isFocused: boolean
  innerProps: HTMLProps<HTMLDivElement>
  children: ReactNode
}

function CustomOption({ children, innerProps, innerRef, isFocused }: CustomOptionProps) {
  return (
    <div
      ref={innerRef}
      className={`${isFocused ? 'bg-ellablue-lighter' : 'bg-ellablue-lightest'}`}
      {...innerProps}
    >
      {children}
    </div>
  )
}

function CustomMenuList({
  children,
  className,
  innerProps,
  innerRef,
}: MenuProps<SingleValue<Option>, boolean, Group>) {
  return (
    <div ref={innerRef} className={`${className} p-0`} {...innerProps}>
      {children}
    </div>
  )
}

// const CustomMenuList = (props) => <div className=

// This avoids issues with tailwind form styling.
// See https://github.com/JedWatson/react-select/issues/4686#issuecomment-927134166
function CustomInput({
  type,
  ...rest
}: InputProps<SingleValue<Option>, boolean, Group>): ReactElement {
  return <components.Input {...rest} />
}

// These are all the available elements that can be customized.
const CustomComponents: ComponentsConfig = {
  // "ClearIndicator": CustomBase,
  // "Control": CustomBase,
  // "DropdownIndicator": CustomBase,
  // "DownChevron": CustomBase,
  // "CrossIcon": CustomBase,
  // "Group": CustomBase,
  // "GroupHeading": CustomBase,
  // "IndicatorsContainer": CustomBase,
  // "IndicatorSeparator": CustomBase,
  Input: CustomInput,
  // "LoadingIndicator": CustomBase,
  Menu: CustomMenuList,
  // "MenuList": CustomMenuList,
  // "MenuPortal": CustomBase,
  // "LoadingMessage": CustomBase,
  // "NoOptionsMessage": CustomBase,
  // "MultiValue": CustomBase,
  // "MultiValueContainer": CustomBase,
  // "MultiValueLabel": CustomBase,
  // "MultiValueRemove": CustomBase,
  Option: CustomOption,
  // "Placeholder": CustomBase,
  // "SelectContainer": CustomSelectContainer,
  // "SingleValue": CustomBase,
  // "ValueContainer": CustomBase,
}

const valueIsSingle = (
  newValue: MultiValue<SingleValue<Option>> | SingleValue<SingleValue<Option>>,
): newValue is Option => {
  if (newValue && 'length' in newValue) {
    console.error(
      "Ella's custom select component does not yet support selecting multiple options",
    )
  }
  return true
}

interface BaseProps {
  placeholder?
  onChange: (value: any) => void
  selected?
}

type SyncSelectProps = BaseProps & { options: Option[] }

function WrappedSelect({
  placeholder = 'Type to search...',
  options = [],
  onChange,
  selected,
}: SyncSelectProps) {
  const selectedOption = options.find((option) => option.value === selected) ?? null

  return (
    <Select
      className="w-full"
      options={options}
      placeholder={placeholder}
      components={CustomComponents}
      value={selectedOption}
      onChange={(newValue) => {
        if (onChange && valueIsSingle(newValue)) {
          onChange(newValue?.value ?? null)
        }
      }}
    />
  )
}

type AsyncSelectProps = BaseProps & {
  loadOptions?: (inputValue, callback: (options: Option[]) => void) => void
}

function WrappedAsyncSelect({
  placeholder = 'Type to search...',
  loadOptions,
  onChange,
  selected,
}: AsyncSelectProps) {
  const [selectedValue, setSelectedValue] = useState(selected)

  return (
    <AsyncSelect
      className="w-full"
      cacheOptions
      loadOptions={loadOptions}
      placeholder={placeholder}
      defaultOptions
      components={CustomComponents}
      value={selectedValue}
      onChange={(newValue) => {
        setSelectedValue(newValue)

        if (onChange && newValue) {
          onChange(newValue.value)
        }
      }}
    />
  )
}

export default WrappedSelect
export { WrappedAsyncSelect as AsyncSelect }
