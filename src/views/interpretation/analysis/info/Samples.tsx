import React from 'react'
import { useSelector } from 'react-redux'

import { Card } from 'components/elements/Card'

import { selectSamplesByAnalysisId } from 'store/interpretation/sampleSlice'
import { RootState } from 'store/store'

import { Sample } from 'types/store/Sample'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

function SampleItem({ sample, label }: { sample: Sample; label: string }) {
  const { affected, identifier, sex, sampleType, familyId } = sample

  return (
    <Card label={label}>
      <div className="grid grid-cols-2 gap-x-2 text-sm text-ellagray-700">
        <div className="font-semibold text-ellagray-900">Sample name:</div>
        <div>{identifier}</div>
        <div className="font-semibold text-ellagray-900">Affected:</div>
        <div>{affected ? 'True' : 'False'}</div>
        <div className="font-semibold text-ellagray-900">Family:</div>
        <div>{familyId}</div>
        <div className="font-semibold text-ellagray-900">Sex:</div>
        <div>{sex}</div>
        <div className="font-semibold text-ellagray-900">Technology:</div>
        <div>{sampleType}</div>
      </div>
    </Card>
  )
}

export default function Samples() {
  const { id } = useInterpretationStateContext()
  const samples = useSelector((state: RootState) =>
    selectSamplesByAnalysisId(state, { analysisId: id }),
  )

  const proband = Object.values(samples).find((sample) => sample.proband) as Sample
  const labeledSamples = Object.entries(samples).map(([sampleId, sample]) => {
    let label
    if (sample.proband) label = 'Proband'
    else if (parseInt(sampleId, 10) === proband?.fatherId) label = 'Father'
    else if (parseInt(sampleId, 10) === proband?.motherId) label = 'Mother'
    else label = 'Sibling'
    return {
      sampleId,
      sample,
      label,
    }
  })

  return (
    <div className="mt-4 flex flex-col">
      <div className="text-lg">Samples</div>
      <div className="flex">
        {labeledSamples.map(({ sampleId, sample, label }) => (
          <SampleItem key={sampleId} sample={sample} label={label} />
        ))}
      </div>
    </div>
  )
}
