import { XCircleIcon } from '@heroicons/react/24/solid'
import * as PopperJS from '@popperjs/core'
import React, { PropsWithChildren, ReactNode, useEffect, useState } from 'react'
import { usePopper } from 'react-popper'

import { generateArrowStyles } from 'components/elements/tooltip/generateArrowStyles'

interface Props {
  children: ReactNode
  anchorElement: Element | null
  placement?: PopperJS.Placement
  onClose: () => void
}

function Tooltip({
  children,
  anchorElement,
  onClose = () => {},
  placement = 'auto',
}: Props) {
  const [popperElement, setPopperElement] = useState<HTMLElement | null>(null)
  const [arrowElement, setArrowElement] = useState<HTMLElement | null>(null)

  const { styles, attributes } = usePopper(anchorElement, popperElement, {
    placement,
    modifiers: [
      { name: 'arrow', options: { element: arrowElement } },
      { name: 'offset', options: { offset: [10, 20] } },
    ],
  })

  const handleClickOutside = (event) => {
    if (!popperElement?.contains(event.target) && popperElement !== event.target) {
      onClose()
    }
  }
  useEffect(() => {
    if (popperElement) {
      setTimeout(() => document.body.addEventListener('click', handleClickOutside))

      return () => {
        document.body.removeEventListener('click', handleClickOutside)
      }
    }

    return () => {}
  }, [popperElement])

  return (
    <div className="relative z-50 whitespace-normal">
      <div
        ref={setPopperElement}
        style={{ ...styles.popper }}
        {...attributes.popper}
        className="w-128 rounded-md bg-white py-2 px-3 text-sm shadow-lg ring-1 ring-black/5"
      >
        <button
          className="absolute top-0 right-0 inline-block cursor-pointer p-2"
          onClick={() => onClose()}
        >
          <XCircleIcon className="h-6 w-6 hover:text-ellagray-500" />
        </button>
        {children}
        <div
          ref={setArrowElement}
          style={generateArrowStyles(
            attributes.popper ? attributes.popper['data-popper-placement'] : 'right',
            styles.arrow,
          )}
          className="arrow"
        />
      </div>
    </div>
  )
}

function P({ children }: PropsWithChildren<{}>) {
  return <div className="my-3">{children}</div>
}

function H1({ children }: PropsWithChildren<{}>) {
  return <div className="text-lg">{children}</div>
}

function H2({ children }: PropsWithChildren<{}>) {
  return <div className="text-base">{children}</div>
}

Tooltip.H1 = H1
Tooltip.H2 = H2
Tooltip.P = P

export default Tooltip
