import { MinusCircleIcon, PlusCircleIcon } from '@heroicons/react/24/solid'
import React, { useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'

import Editor from 'components/editor/Editor'

import { selectConfig } from 'store/config/configSlice'

import { AcmgCode, mapFromApi } from 'types/store/Acmg'

import { AcmgTooltip } from 'views/interpretation/components/acmg/AcmgTooltip'
import { AcmgFormatter } from 'views/interpretation/helpers/acmg/AcmgFormatter'
import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'
import { AcmgStrengthChanger } from 'views/interpretation/helpers/acmg/AcmgStrengthChanger'

interface Props {
  code: AcmgCode
  onAdd?: (code: AcmgCode) => void
  onRemove?: (code: AcmgCode) => void
  onRichTextChange?: (newText: string, code: AcmgCode) => void
  onStrengthChange?: (code: AcmgCode) => void
  disabled?: boolean
  onClick?: (event) => void
}

export function AcmgChip({
  code,
  onAdd,
  onRemove,
  onRichTextChange,
  onStrengthChange,
  disabled = false,
  onClick,
}: Props) {
  const config = useSelector(selectConfig)
  const [showTooltip, setShowTooltip] = useState(false)
  const tooltipAnchor = useRef<HTMLButtonElement>(null)
  const acmgType = AcmgFormatter.getColorKey(code.code)
  const { comment } = code

  return (
    <div
      className={`flex w-128 flex-col text-sm ${onClick ? 'cursor-pointer' : ''}`}
      onClick={onClick}
    >
      <div className="flex">
        <div
          className={`flex w-full justify-between gap-x-2 border-2 border-solid p-2 acmg-${acmgType}`}
        >
          <span className="mr-4 flex">
            {!disabled &&
              onStrengthChange &&
              AcmgStrengthChanger.canDowngradeCodeObj(code, config) && (
                <button
                  name={`weakenACMG-${code.code}`}
                  onClick={() =>
                    onStrengthChange(AcmgStrengthChanger.downgradeCodeObj(code, config))
                  }
                >
                  -
                </button>
              )}
            <button
              className={`mx-1 whitespace-nowrap border-b-2 border-dotted acmg-link-${acmgType}`}
              onClick={() => setShowTooltip(true)}
              ref={tooltipAnchor}
            >
              {AcmgFormatter.makeCodeNameReadable(
                AcmgFormatter.formatCode(code.code, config),
              )}
            </button>
            <span className="w-full" />
            {!disabled &&
              onStrengthChange &&
              AcmgStrengthChanger.canUpgradeCodeObj(code, config) && (
                <button
                  name={`strengthenACMG-${code.code}`}
                  onClick={() =>
                    onStrengthChange(AcmgStrengthChanger.upgradeCodeObj(code, config))
                  }
                >
                  +
                </button>
              )}
          </span>
          <div className="w-full" />
          <span className="whitespace-nowrap">
            {AcmgHelper.getShortCriteria(code, config)}
          </span>
          {acmgType !== 'other' && onAdd && !disabled && (
            <button name={`addACMG-${code.code}`} onClick={() => onAdd(code)}>
              <PlusCircleIcon width={22} height={22} />
            </button>
          )}
          {acmgType !== 'other' && onRemove && !disabled && (
            <button name={`removeACMG-${code.code}`} onClick={() => onRemove(code)}>
              <MinusCircleIcon width={22} height={22} />
            </button>
          )}
        </div>

        {AcmgHelper.getRequiredFor(code, config).map((required) => {
          const colorKey = AcmgFormatter.getColorKey(required)
          return (
            <div
              key={`${code.code}_${uuidv4()}`}
              className={`acmg-${colorKey} items-center whitespace-nowrap p-2 text-center`}
            >
              <button
                className={`acmg-link-${colorKey} flex`}
                onClick={() =>
                  !disabled && onAdd && onAdd(mapFromApi({ code: required }))
                }
              >
                {AcmgFormatter.makeCodeNameReadable(
                  AcmgFormatter.formatCode(required, config),
                )}
                {!disabled && <PlusCircleIcon width={26} height={26} />}
              </button>
            </div>
          )
        })}

        {showTooltip && (
          <AcmgTooltip
            onClose={() => setShowTooltip(false)}
            code={code}
            tooltipAnchor={tooltipAnchor}
          />
        )}
      </div>

      {onRichTextChange && 'comment' in code && (
        <div className="w-full">
          <Editor
            initialValue={comment}
            placeholder={`${AcmgFormatter.makeCodeNameReadable(
              AcmgFormatter.formatCode(code.code, config),
            )}-comments`}
            onBlur={(value) => {
              onRichTextChange(value, code)
            }}
            readOnly={disabled}
          />
        </div>
      )}
    </div>
  )
}
