import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PaddedContainer from 'components/elements/Containers'
import SectionBox from 'components/elements/SectionBox'
import AnalysesTable from 'components/tables/AnalysesTable'

import {
  fetchAnalyses,
  selectFinalizedAnalyses,
  selectNotReadyAnalyses,
  selectNotStartedAnalyses,
  selectOthersOngoingAnalyses,
  selectOwnOngoingAnalyses,
  selectPendingMedicalReviewAnalyses,
  selectPendingReviewAnalyses,
} from 'store/overview/analysesOverviewSlice'

import AnalysisFilter from 'views/overview/components/AnalysesFilter'
import { sortByPriority } from 'views/overview/utils'

function Analyses() {
  const dispatch = useDispatch()
  const notReady = useSelector(selectNotReadyAnalyses)
  const notStarted = useSelector(selectNotStartedAnalyses)
  const ownOngoing = useSelector(selectOwnOngoingAnalyses)
  const othersOngoing = useSelector(selectOthersOngoingAnalyses)
  const pendingReview = useSelector(selectPendingReviewAnalyses)
  const pendingMedicalReview = useSelector(selectPendingMedicalReviewAnalyses)
  const finalized = useSelector(selectFinalizedAnalyses)

  // Lazy-instantiate filter state by passing an argment-less function
  const [filter, setFilter] = useState(() => (analyses) => analyses)

  // TODO: Should be done before rendering the component?
  useEffect(() => {
    dispatch(fetchAnalyses())
  }, [])

  function onFilterChange(filterFunc) {
    setFilter(() => filterFunc)
  }

  return (
    <PaddedContainer>
      <AnalysisFilter onChange={onFilterChange} />
      <SectionBox id="not-ready" title={`Not Ready (${notReady.length})`} color="ellared">
        <AnalysesTable overviewAnalyses={sortByPriority(filter(notReady))} />
      </SectionBox>

      <SectionBox
        id="own-ongoing"
        title={`Your analyses (${ownOngoing.length})`}
        color="ellablue-lighter"
      >
        <AnalysesTable overviewAnalyses={sortByPriority(filter(ownOngoing))} />
      </SectionBox>

      <SectionBox
        id="not-started"
        title={`Interpretation (${notStarted.length})`}
        color="ellagreen"
      >
        <AnalysesTable overviewAnalyses={sortByPriority(filter(notStarted))} />
      </SectionBox>

      <SectionBox
        id="pending-review"
        title={`Review (${pendingReview.length})`}
        color="ellapurple"
      >
        <AnalysesTable overviewAnalyses={sortByPriority(filter(pendingReview))} />
      </SectionBox>

      <SectionBox
        id="pending-medical-review"
        title={`Medical Review (${pendingMedicalReview.length})`}
        color="ellapurple"
      >
        <AnalysesTable overviewAnalyses={sortByPriority(filter(pendingMedicalReview))} />
      </SectionBox>

      <SectionBox
        id="others-ongoing"
        title={`Others' analyses (${othersOngoing.length})`}
        color="ellagray"
      >
        <AnalysesTable overviewAnalyses={sortByPriority(filter(othersOngoing))} />
      </SectionBox>

      <SectionBox id="finalized" title="Finalized" color="ellagray">
        <AnalysesTable overviewAnalyses={finalized} />
      </SectionBox>
    </PaddedContainer>
  )
}

export default Analyses
