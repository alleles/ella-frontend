import React, { useEffect } from 'react'

import Table from 'components/elements/Table'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import useAnalysisInterpretationUIContext from 'views/interpretation/analysis/UIContext'
import { AlleleTableDataRow } from 'views/interpretation/helpers/alleleTable/alleleTable'
import { columnDefinitions } from 'views/interpretation/helpers/alleleTable/columnDefinitions'
import { comparatorFunctions } from 'views/interpretation/helpers/alleleTable/comparatorFunctions'

interface AlleleTableProp {
  id?: string
  tableData: AlleleTableDataRow[]
}

export function AlleleTable({ id, tableData }: AlleleTableProp) {
  const { selectedAlleleId, setSelectedAlleleId } = useInterpretationStateContext()
  const { callerTypeMode } = useAnalysisInterpretationUIContext()

  const onRowClick = (row: AlleleTableDataRow) => {
    setSelectedAlleleId(Number(row.key))
  }

  // Set top row to selected allele
  useEffect(() => {
    const sorted = [...tableData].sort(comparatorFunctions.position)
    setSelectedAlleleId(Number(sorted[0].key))
  }, [callerTypeMode])

  return (
    <Table<AlleleTableDataRow>
      id={id}
      data={tableData}
      selectedRowKey={selectedAlleleId ? selectedAlleleId.toString() : undefined}
      columns={Object.values(columnDefinitions)}
      onRowClick={onRowClick}
      classNames={{
        table: 'w-full my-2 table-fixed',
      }}
    />
  )
}
