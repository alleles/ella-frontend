import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import Anchor from 'components/elements/Anchor'
import Button from 'components/elements/Button'
import { Card } from 'components/elements/Card'
import SectionBox from 'components/elements/SectionBox'
import { useLayout } from 'components/layout/LayoutContainer'

import { selectRichTextTemplates } from 'store/config/configSlice'
import {
  selectAnnotationByAnnotationId,
  selectReferencesByAnnotationId,
} from 'store/interpretation/annotationSlice'
import {
  selectCustomAnnotationsByAnnotationId,
  selectReferencesByCustomAnnotationId,
} from 'store/interpretation/customAnnotationSlice'
import { RootState } from 'store/store'

import { Annotation } from 'types/store/Annotation'
import { Reference } from 'types/store/Reference'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import {
  ReferenceInfo,
  ReferenceStatus,
  referenceAnnotationByStatus,
} from 'views/interpretation/sections/references/ReferenceByStatus'

interface ReferenceInfoProp {
  referenceInfo: ReferenceInfo
}

function getFormatted(reference: Reference): string {
  let shortDesc = reference.authors || 'unknown author'
  if (reference.year !== undefined && reference.year !== null && reference.year > 0) {
    shortDesc += ` (${reference.year})`
  }
  shortDesc += `, ${reference.journal}`

  return shortDesc
}

function getUrls(reference: Reference): string | undefined {
  if (!reference || !reference.pubmedId) {
    return undefined
  }
  return `https://pubmed.ncbi.nlm.nih.gov/${reference.pubmedId}`
}

interface ReferenceViewModel {
  id: number
  urls?: string
  formatted?: string
  title: string | null
  source?: string
  isPublished: boolean
}

function viewModelFromReferenceInfo(
  infoReferences: Array<ReferenceInfo>,
): Array<ReferenceViewModel> {
  return infoReferences.map((infoRef) => ({
    id: infoRef.reference.id,
    urls: getUrls(infoRef.reference),
    formatted: getFormatted(infoRef.reference),
    title: infoRef.reference.title,
    source: infoRef.source,
    isPublished: infoRef.reference.published,
  }))
}

function ReferenceInfoView({ referenceInfo }: ReferenceInfoProp) {
  const viewModel = viewModelFromReferenceInfo([referenceInfo])

  const { rightPanelState } = useLayout()

  if (rightPanelState === 'expanded') {
    return (
      <div key={referenceInfo.reference.id}>
        {viewModel.map((rv) => (
          <div key={rv.formatted} className="grid grid-cols-12 space-x-2">
            <div className="col-span-10">
              {rv.urls ? (
                <Anchor
                  href={rv.urls}
                  className="mb-2 underline hover:text-ellablue-darkest"
                >
                  {rv.title || 'unknown title'}
                </Anchor>
              ) : (
                <div className="truncate">{rv.title || 'unknown title'}</div>
              )}
              <div className="truncate italic">{rv.formatted}</div>

              {rv.isPublished ? (
                <div className="truncate pt-2">{rv.source}</div>
              ) : (
                <div>
                  <div className="pt-2 text-sm uppercase">Unpublished</div>
                  <div className="truncate">{rv.source}</div>
                </div>
              )}
            </div>
            <div className="col-span-2">
              {rv.urls ? (
                <Button>
                  <Anchor href={rv.urls} className="text-white">
                    See details
                  </Anchor>{' '}
                </Button>
              ) : (
                <Button>
                  <span>See details</span>
                </Button>
              )}
            </div>
          </div>
        ))}
      </div>
    )
  }

  return (
    <div key={referenceInfo.reference.id}>
      {viewModel.map((rv) => (
        <div key={rv.formatted} className="grid grid-cols-12 space-x-2">
          <div className="col-span-9">
            {rv.urls ? (
              <Anchor
                href={rv.urls}
                className="mb-2 underline hover:text-ellablue-darkest"
              >
                {rv.title || 'unknown title'}
              </Anchor>
            ) : (
              <div className="truncate">{rv.title || 'unknown title'}</div>
            )}
            <div className="truncate italic">{rv.formatted}</div>
          </div>
          <div className="col-span-2">
            {!rv.isPublished && (
              <span className="pr-2 text-sm uppercase">Unpublished</span>
            )}
            {rv.source}
          </div>
          <span className="col-span-1">
            {rv.urls ? (
              <Button>
                <Anchor href={rv.urls} className="text-white">
                  See details
                </Anchor>
              </Button>
            ) : (
              <Button>
                <span>See details</span>
              </Button>
            )}
          </span>
        </div>
      ))}
    </div>
  )
}

function ReferenceEvaluatedView({ referenceInfo }: ReferenceInfoProp) {
  return (
    <div className="m-2 rounded border border-ellagray p-2 text-sm">
      <ReferenceInfoView referenceInfo={referenceInfo} />
      {referenceInfo.assessment?.evaluation.comment !== '' && (
        <div className="mt-2">
          <div className="underline">EVALUATION:</div>
          <div>{referenceInfo.assessment?.evaluation.comment}</div>
        </div>
      )}
    </div>
  )
}

export default function References() {
  const { alleleState, setAlleleState, readOnly, selectedAlleleId } =
    useInterpretationStateContext()

  const { presentationAlleles } = useInterpretation()
  const { annotationId, customAnnotationId } = presentationAlleles[selectedAlleleId]

  const templates = useSelector(selectRichTextTemplates('classificationReferences'))

  const [comment, setComment] = useState(
    alleleState?.alleleAssessment.evaluation?.reference.comment,
  )

  useEffect(() => {
    setComment(alleleState?.alleleAssessment.evaluation?.reference.comment)
  }, [selectedAlleleId])

  const onChange = (value: string) => {
    setAlleleState({
      alleleAssessment: {
        evaluation: {
          reference: {
            comment: value,
          },
        },
      },
    })
  }

  const annotation: Annotation = useSelector((state: RootState) =>
    selectAnnotationByAnnotationId(state, { annotationId }),
  )

  const configurableAnnotationReferences: Reference[] = useSelector((state: RootState) =>
    selectReferencesByAnnotationId(state, { annotationId }),
  )

  const customAnnotation = useSelector(
    customAnnotationId
      ? (state: RootState) =>
          selectCustomAnnotationsByAnnotationId(state, {
            customAnnotationId,
          })
      : () => undefined,
  )

  const customisableReferenceAnnotation: Reference[] = useSelector(
    customAnnotationId
      ? (state: RootState) =>
          selectReferencesByCustomAnnotationId(state, {
            customAnnotationId,
          })
      : () => [],
  )

  const uniqueReferences = [
    ...configurableAnnotationReferences,
    ...customisableReferenceAnnotation,
  ].filter((v, i, a) => a.findIndex((t) => t.id === v.id) === i)

  const referenceByStatus: ReferenceStatus = referenceAnnotationByStatus({
    alleleState,
    references: uniqueReferences,
    annotationReferences: annotation.references,
    customAnnotationReferences: customAnnotation?.annotations.references || [],
  })

  const [showIgnored, setShowIgnored] = useState(false)

  return (
    <SectionBox title="Studies & References" color="ellapurple" initCollapsed={false}>
      <Editor
        key={`references-comment-${selectedAlleleId}`}
        id="references-comment"
        placeholder="References - comments"
        initialValue={comment}
        onBlur={onChange}
        templates={templates}
        readOnly={readOnly}
      />
      <div className="my-2 flex max-w-xs flex-row gap-x-2 py-2">
        <Button>Add studies</Button>
        <Button onClick={() => setShowIgnored(!showIgnored)}>
          Show ignored ({referenceByStatus.ignored.length})
        </Button>
      </div>

      <div>
        {referenceByStatus.evaluated.length > 0 && (
          <Card label="evaluated">
            <div className="border-collapse">
              {referenceByStatus.evaluated.map((ref) => (
                <ReferenceEvaluatedView key={ref.reference.id} referenceInfo={ref} />
              ))}
            </div>
          </Card>
        )}
      </div>
      {(referenceByStatus.pending.length > 0 || referenceByStatus.missing.length > 0) && (
        <Card label="pending">
          <div>
            {referenceByStatus.pending.map((ref) => (
              <div
                key={`pending-${ref.reference.id}`}
                className="mb-2 rounded border border-ellagray p-2 text-sm"
              >
                <ReferenceInfoView referenceInfo={ref} />
              </div>
            ))}
          </div>
          <div>
            {referenceByStatus.missing.map((ref) => (
              <div
                key={`missing-${ref.pubmedId}`}
                className="mb-2 rounded border border-ellagray p-2 text-sm"
              >
                Missing data for Pubmed ID:
                <Anchor
                  href={`https://pubmed.ncbi.nlm.nih.gov/${ref.pubmedId}`}
                  className="px-2 underline hover:text-ellablue-darkest"
                >
                  {ref.pubmedId}
                </Anchor>
                in database. Please add the reference manually.
              </div>
            ))}
          </div>
        </Card>
      )}
      <div>
        {referenceByStatus.not_relevant.length > 0 && (
          <Card label="not relevant">
            {referenceByStatus.not_relevant.map((ref) => (
              <ReferenceEvaluatedView key={ref.reference.id} referenceInfo={ref} />
            ))}
          </Card>
        )}
      </div>
      <div>
        {referenceByStatus.ignored.length > 0 && showIgnored && (
          <Card label="ignored">
            {referenceByStatus.ignored.map((ref) => (
              <ReferenceEvaluatedView key={ref.reference.id} referenceInfo={ref} />
            ))}
          </Card>
        )}
      </div>
    </SectionBox>
  )
}
