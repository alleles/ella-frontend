import * as ApiTypes from 'types/api/Pydantic'

export interface AlleleCollision {
  analysisName: string | null
  type: 'allele' | 'analysis'
  userId: number | null
  workflowStatus: 'Not ready' | 'Interpretation' | 'Review' | 'Medical review'
}

export function mapFromApi(
  alleleCollisionFromApi: ApiTypes.AlleleCollision,
): AlleleCollision {
  return {
    analysisName: alleleCollisionFromApi.analysis_name || null,
    type: alleleCollisionFromApi.type,
    userId: alleleCollisionFromApi.user?.id || null,
    workflowStatus: alleleCollisionFromApi.workflow_status,
  }
}
