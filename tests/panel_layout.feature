Feature: Expand and collapse panels

  Background:
    Given state "test-bdd"

  Scenario: Button change and visibility of panels on DETAILS page

    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "CLASSIFICATION"

    # Collapse/expand right panel
    When I collapse the right panel
    Then I should not see "CLASSIFICATION"

    When I expand the right panel
    Then I should see "CLASSIFICATION"

    # Collapse/expand bottom panel
    When I collapse the bottom panel
    Then I should not see an element with id "collapse-bottom-panel"
    And I should see "CLASSIFICATION"

    When I expand the bottom panel
    And I expand the bottom panel
    Then I should not see an element with id "expand-bottom-panel"
    And I should not see "CLASSIFICATION"
