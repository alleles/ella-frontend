import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'

import LayoutContainer from 'components/layout/LayoutContainer'

import { fetchAlleleInterpretationLogForAlleleId } from 'store/interpretation/alleleInterpretationLogSlice'
import { fetchAlleleInterpretationsForAlleleId } from 'store/interpretation/alleleInterpretationSlice'

import { AlleleInterpretationProvider } from 'views/interpretation/AlleleInterpretationContext'
import { InterpretationStateProvider } from 'views/interpretation/InterpretationStateContext'
import AlleleIntepretationActions from 'views/interpretation/components/AlleleInterpretationActions'
import { AlleleInterpretationForm } from 'views/interpretation/components/InterpretationForm'
import AlleleBar from 'views/interpretation/components/alleleBar/AlleleBar'
import Classification from 'views/interpretation/sections/Classification'

export default function AlleleInterpretation() {
  const dispatch = useDispatch()
  const { alleleId } = useParams<'alleleId'>()
  const [ready, setReady] = useState(false)

  if (!alleleId) {
    throw new Error('Missing alleleId')
  }
  useEffect(() => {
    const fetchData = async () => {
      await dispatch(fetchAlleleInterpretationsForAlleleId(Number(alleleId)))
      await dispatch(fetchAlleleInterpretationLogForAlleleId(Number(alleleId)))
    }
    fetchData()
      .then(() => {
        setReady(true)
      })
      .catch((e) => {
        console.error(e)
      })
  }, [])

  if (!ready) return null

  return (
    <AlleleInterpretationProvider alleleId={parseInt(alleleId, 10)}>
      <InterpretationStateProvider>
        <LayoutContainer
          topbar={<AlleleIntepretationActions />}
          mainPanel={<AlleleInterpretationForm />}
          rightPanel={<Classification />}
          bottomPanel={<AlleleBar />}
        />
      </InterpretationStateProvider>
    </AlleleInterpretationProvider>
  )
}
