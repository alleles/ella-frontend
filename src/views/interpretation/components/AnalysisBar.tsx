import React from 'react'

import Button from 'components/elements/Button'
import ButtonGroup from 'components/elements/ButtonGroup'
import { useLayout } from 'components/layout/LayoutContainer'

import { CallerType } from 'types/api/Pydantic'

import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'
import useAnalysisInterpretationUI, {
  InterpretationUIMode,
} from 'views/interpretation/analysis/UIContext'
import WorkflowActions from 'views/interpretation/components/WorkflowActions'
import InterpretationLog from 'views/interpretation/components/alleleBar/InterpretationLog'

// We take out 'FILTER' from the list of available modes as we want it as a separate button.
const interpretationModes = Object.fromEntries(
  Object.entries(InterpretationUIMode).filter(([key]) => key !== 'FILTER'),
)

export default function AnalysisBar() {
  const { uiMode, setUIMode, callerTypeMode, setCallerTypeMode } =
    useAnalysisInterpretationUI()
  const { sectionBoxAllCollapsed, setSectionBoxAllCollapsed } = useLayout()
  const { analysis, interpretation } = useAnalysisInterpretation()
  return (
    <div className="flex flex-row items-center justify-between shadow-md">
      <div className="flex flex-row">
        <div className="flex gap-x-2 p-2">
          <ButtonGroup
            selected={uiMode.toUpperCase()}
            options={Object.keys(interpretationModes)}
            onChange={(v) => setUIMode(v as InterpretationUIMode)}
          />
          <div className="flex w-16 shrink">
            <Button onClick={() => setUIMode(InterpretationUIMode.FILTER)}>Filter</Button>
          </div>
          <ButtonGroup
            selected={callerTypeMode.toUpperCase()}
            options={Object.values(CallerType).map((e) => e.toUpperCase())}
            onChange={(callerType) => {
              setCallerTypeMode(callerType.toLowerCase() as CallerType)
            }}
          />
        </div>
      </div>
      <div className="text-sm font-medium text-ellagray-700">
        {analysis.name} •{' '}
        <span className="uppercase">{interpretation.workflowStatus}</span>
      </div>
      <div className="flex justify-end p-2">
        <div className="w-32 pr-4">
          <Button onClick={() => setSectionBoxAllCollapsed(!sectionBoxAllCollapsed)}>
            {!sectionBoxAllCollapsed ? 'Collapse all' : 'Expand all'}
          </Button>
        </div>
        <InterpretationLog />
        <WorkflowActions />
      </div>
    </div>
  )
}
