Feature: View and edit References section

  Background:
    Given state "test-bdd"

  Scenario: Check References section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "STUDIES & REFERENCES"
    And I should see "PENDING"

    # Can edit comment field
    When I start the interpretation
    And I type "Test References comment" into the editor with xpath "//div[@id='references-comment-editor-content']"
    Then I should see "Test References comment"

    # Check annotation for 2 variants
    # BRCA2 c.10G>T
    When I select the allele row 0 from the allele table
    Then I should see "Greek BRCA1 and BRCA2 mutation spectrum"
    And I should see "Konstantopoulou"
    # BRCA2 c.51_52del
    When I select the allele row 1 from the allele table
    Then I should see "Spectrum of genetic variants of BRCA1 and BRCA2"
    And I should not see "Konstantopoulou"

# TODO: Check adding reference (PubMed, manual; published/unpublished) - pending #169
# TODO: Check evaluating references (Directly/Indirectly relevant, Ignored, Not relevant), including manually added - pending #89
# TODO: Check sorting for evaluated references section - pending above
