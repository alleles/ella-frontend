import { combineReducers, configureStore } from '@reduxjs/toolkit'

import alertsReducer from 'store/alerts/alertsSlice'
import broadcastsReducer from 'store/broadcasts/broadcastsSlice'
import configReducer from 'store/config/configSlice'
import genePanelsReducer from 'store/genePanels/genePanelsSlice'
import alleleAssessmentReducer from 'store/interpretation/alleleAssessmentSlice'
import alleleCollisionReducer from 'store/interpretation/alleleCollisionsSlice'
import alleleInterpretationLogReducer from 'store/interpretation/alleleInterpretationLogSlice'
import alleleInterpretationReducer from 'store/interpretation/alleleInterpretationSlice'
import alleleReportReducer from 'store/interpretation/alleleReportSlice'
import alleleReducer from 'store/interpretation/alleleSlice'
import analysisInterpretationLogReducer from 'store/interpretation/analysisInterpretationLogSlice'
import analysisInterpretationReducer from 'store/interpretation/analysisInterpretationSlice'
import analysisReducer from 'store/interpretation/analysisSlice'
import annotationConfigReducer from 'store/interpretation/annotationConfigSlice'
import annotationReducer from 'store/interpretation/annotationSlice'
import attachmentReducer from 'store/interpretation/attachmentSlice'
import customAnnotationReducer from 'store/interpretation/customAnnotationSlice'
import filterConfigReducer from 'store/interpretation/filterConfigSlice'
import referenceReducer from 'store/interpretation/referenceSlice'
import sampleReducer from 'store/interpretation/sampleSlice'
import allelesOverviewReducer from 'store/overview/allelesOverviewSlice'
import analysesOverviewReducer from 'store/overview/analysesOverviewSlice'
import usersReducer from 'store/users/usersSlice'

const CLEAR_STORE = 'CLEAR_STORE'

const reducer = combineReducers({
  config: configReducer,
  users: usersReducer,
  analysesOverview: analysesOverviewReducer,
  allelesOverview: allelesOverviewReducer,
  broadcasts: broadcastsReducer,
  alerts: alertsReducer,
  alleles: alleleReducer,
  alleleAssessments: alleleAssessmentReducer,
  alleleReports: alleleReportReducer,
  annotations: annotationReducer,
  alleleInterpretations: alleleInterpretationReducer,
  annotationConfigs: annotationConfigReducer,
  alleleInterpretationLog: alleleInterpretationLogReducer,
  analysisInterpretationLog: analysisInterpretationLogReducer,
  alleleCollisions: alleleCollisionReducer,
  customAnnotations: customAnnotationReducer,
  attachments: attachmentReducer,
  references: referenceReducer,
  genePanels: genePanelsReducer,
  filterConfigs: filterConfigReducer,
  analyses: analysisReducer,
  analysisInterpretations: analysisInterpretationReducer,
  samples: sampleReducer,
})

const rootReducer = (state, action) => {
  if (action.type === CLEAR_STORE) {
    return reducer(undefined, action)
  }
  return reducer(state, action)
}
export const store = configureStore({
  reducer: rootReducer,
})

// Resets the store to its initial state
export const clearStore = { type: CLEAR_STORE }
export type RootState = ReturnType<typeof reducer>
