import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PaddedContainer from 'components/elements/Containers'
import SectionBox from 'components/elements/SectionBox'
import AllelesTable from 'components/tables/AllelesTable'

import {
  fetchAlleles,
  selectFinalizedAlleles,
  selectMarkedReviewAlleles,
  selectNotStartedAlleles,
  selectOthersOngoingAlleles,
  selectOwnOngoingAlleles,
} from 'store/overview/allelesOverviewSlice'

import { sortByPriority } from 'views/overview/utils'

export default function AllelesOverview() {
  const dispatch = useDispatch()
  const ownOngoing = useSelector(selectOwnOngoingAlleles)
  const othersOngoing = useSelector(selectOthersOngoingAlleles)
  const notStarted = useSelector(selectNotStartedAlleles)
  const markedReview = useSelector(selectMarkedReviewAlleles)
  const finalized = useSelector(selectFinalizedAlleles)
  // TODO: Should be done before rendering the component?
  useEffect(() => {
    dispatch(fetchAlleles())
  }, [])

  return (
    <PaddedContainer>
      <SectionBox
        id="own-ongoing"
        title={`Your Variants (${ownOngoing.length})`}
        color="ellablue-lighter"
      >
        <AllelesTable alleles={sortByPriority(ownOngoing)} />
      </SectionBox>

      <SectionBox
        id="not-started"
        title={`Interpretation (${notStarted.length})`}
        color="ellagreen"
      >
        <AllelesTable alleles={sortByPriority(notStarted)} />
      </SectionBox>

      <SectionBox
        id="pending-review"
        title={`Pending review (${markedReview.length})`}
        color="ellapurple"
      >
        <AllelesTable alleles={sortByPriority(markedReview)} />
      </SectionBox>

      <SectionBox
        id="others-ongoing"
        title={`Others' variants (${othersOngoing.length})`}
        color="ellagray"
      >
        <AllelesTable alleles={sortByPriority(othersOngoing)} />
      </SectionBox>

      <SectionBox
        id="finalized"
        title={`Finalized (${finalized.length})`}
        color="ellagray"
      >
        {/* TODO: Pagination */}
        <AllelesTable alleles={finalized} />
      </SectionBox>
    </PaddedContainer>
  )
}
