import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { InterpretationLogItem, mapFromApi } from 'types/store/InterpretationLog'

// TODO: Rename to alleleInterpretationLogSlice

const initialState: Record<number, InterpretationLogItem[]> = {}

const fetchAlleleInterpretationLogForAlleleId = createAsyncThunk<
  typeof initialState,
  number
>('alleleInterpretationLog/fetchAlleleInterpretationLog', async (alleleId) => {
  const alleleInterpretationLog = await API.AlleleInterpreationLogs.get(alleleId).then(
    (result) => result.data,
  )

  return { [alleleId]: alleleInterpretationLog.logs.map(mapFromApi) }
})

const alleleInterpretationLogSlice = createSlice({
  name: 'alleleInterpretationLog',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAlleleInterpretationLogForAlleleId.fulfilled, (state, action) =>
      Object.assign(state, action.payload),
    )
  },
})

const selectAlleleInterpretationLogByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
) => state.alleleInterpretationLog[alleleId]

export {
  fetchAlleleInterpretationLogForAlleleId,
  selectAlleleInterpretationLogByAlleleId,
}

export default alleleInterpretationLogSlice.reducer
