import { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'

export interface Frequency {
  freq: {
    [k: string]: number
  }
  count?: {
    [k: string]: number
  }
  hom?: {
    [k: string]: number
  }
  hemi?: {
    [k: string]: number
  }
  het?: {
    [k: string]: number
  }
  num?: {
    [k: string]: number
  }
  filter?: {
    [k: string]: string[]
  }
  indications?: {
    [k: string]: {
      [k: string]: number
    }
  }
}

export interface AnnotationReference {
  pubmedId?: number
  id?: number
  source: string
  sourceInfo?: string
  [k: string]: unknown
}

export interface CustomAnnotationReference {
  id: number
  pubmedId?: number
  source: string
  sourceInfo?: string
  [k: string]: unknown
}
export interface Transcript {
  consequences: ApiTypes.Consequence[]
  hgncId: number | null
  symbol: string | null
  HGVSc: string | null
  HGVScShort: string | null
  HGVScInsertion: string | null
  HGVSp: string | null
  protein: string | null
  strand: -1 | 1
  aminoAcids: string | null
  dbsnp: string[] | null
  exon: string | null
  intron: string | null
  codons: string | null
  transcript: string
  isCanonical: boolean
  exonDistance: number | null
  codingRegionDistance: number | null
  inLastExon: 'yes' | 'no'
  splice: any[] | null
  [k: string]: unknown
}

export interface ClinvarDetailObject {
  rcv: string
  submitter: string
  traitnames?: string
  variant_id: number
  last_evaluated: Date
  clinical_significance_descr?: string
}

export interface ClinvarDetail {
  items: Array<ClinvarDetailObject>
  variant_description: string
}
export interface Annotation {
  alleleId: number
  schemaVersion: string
  annotationConfigId: number
  dateSuperseeded: string | null
  dateCreated?: string
  frequencies: {
    [k: string]: Frequency
  }
  references: Array<AnnotationReference>
  transcripts: Transcript[]
  external?: {
    [k: string]: any
  }
  filteredTranscriptsIds: String[]
}

export function mapFromApi(
  annotationsFromApi: ApiTypes.Annotation,
  alleleId: number,
  state: RootState,
): Annotation {
  // Since annotation is dynamic, we can not build this "bottom-up"
  // We need to process known keys, but keep anything else that could be part of the object
  // (this would be defined by the annotation config, and can not be handled statically)
  const {
    frequencies,
    annotation_config_id,
    schema_version,
    date_superceeded,
    references,
    transcripts,
    filtered_transcripts, // Replaced by selector
    external,
  } = annotationsFromApi

  const annotation: Annotation = {
    alleleId,
    frequencies: frequencies || {},
    external,
    annotationConfigId: annotation_config_id,
    schemaVersion: schema_version,
    dateSuperseeded: date_superceeded || null,
    references: (references || [])
      .filter((r) => r.source !== 'User')
      .map((r) => ({
        id: r.id,
        pubmedId: r.pubmed_id,
        source: r.source,
      })),
    filteredTranscriptsIds: filtered_transcripts || [],
    transcripts: (transcripts || []).map((tx) => ({
      consequences: tx.consequences,
      hgncId: tx.hgnc_id || null,
      symbol: tx.symbol || null,
      HGVSc: tx.HGVSc || null,
      HGVScShort: tx.HGVSc_short || null,
      HGVScInsertion: tx.HGVSc_insertion || null,
      HGVSp: tx.HGVSp || null,
      protein: tx.protein || null,
      strand: tx.strand,
      aminoAcids: tx.amino_acids || null,
      dbsnp: tx.dbsnp || null,
      exon: tx.exon || null,
      intron: tx.intron || null,
      codons: tx.codons || null,
      transcript: tx.transcript,
      isCanonical: tx.is_canonical,
      exonDistance: tx.exon_distance || null,
      codingRegionDistance: tx.coding_region_distance || null,
      inLastExon: tx.in_last_exon,
      splice: tx.splice || null,
    })),
  }

  // Remove custom annotation; this is fetched separately
  const { config } = state
  if (config.custom_annotation) {
    Object.entries(config.custom_annotation).forEach(([section, sectionConfig]) => {
      if (annotation[section]) {
        sectionConfig.forEach((item) => {
          delete annotation[section][item.key]
        })
      }
    })
  }

  return annotation
}
