import React, { ReactElement, ReactNode } from 'react'

import LayoutContainer from 'components/layout/LayoutContainer'

import AlleleBottomPanel from 'views/interpretation/AlleleBottomPanel'
import {
  InterpretationUIMode,
  analysisInterpretationUIContext,
  useAnalysisInterpretationUIContextState,
} from 'views/interpretation/analysis/UIContext'
import AnalysisInfo from 'views/interpretation/analysis/info/AnalysisInfo'
import AnalysisBar from 'views/interpretation/components/AnalysisBar'
import FilteredVariants from 'views/interpretation/components/FilteredVariants'
import { AlleleInterpretationForm } from 'views/interpretation/components/InterpretationForm'
import IgvJs from 'views/interpretation/genomicsViewer/IgvJs'
import TrackControl from 'views/interpretation/genomicsViewer/TrackControl'
import TrackSelectionContextProvider from 'views/interpretation/genomicsViewer/TrackSelectionContext'
import Classification from 'views/interpretation/sections/Classification'

export function AnalysisInterpretationUIProvider(): ReactElement {
  const context = useAnalysisInterpretationUIContextState()

  let layout: ReactNode = null
  switch (context.uiMode) {
    case InterpretationUIMode.INFO:
      layout = (
        <LayoutContainer
          topbar={<AnalysisBar />}
          mainPanel={<AnalysisInfo />}
          rightPanel={null}
          bottomPanel={null}
        />
      )
      break
    case InterpretationUIMode.VISUAL:
      layout = (
        <TrackSelectionContextProvider>
          <LayoutContainer
            topbar={<AnalysisBar />}
            mainPanel={<IgvJs />}
            rightPanel={<TrackControl />}
            bottomPanel={<AlleleBottomPanel />}
          />
        </TrackSelectionContextProvider>
      )
      break
    case InterpretationUIMode.REPORT:
      layout = (
        <LayoutContainer
          topbar={<AnalysisBar />}
          mainPanel={<div>Report</div>}
          rightPanel={null}
          bottomPanel={<div>Allele table placeholder</div>}
        />
      )
      break
    case InterpretationUIMode.DETAILS:
      layout = (
        <LayoutContainer
          topbar={<AnalysisBar />}
          mainPanel={<AlleleInterpretationForm />}
          rightPanel={<Classification />}
          bottomPanel={<AlleleBottomPanel />}
        />
      )
      break
    case InterpretationUIMode.FILTER:
      layout = (
        <LayoutContainer topbar={<AnalysisBar />} mainPanel={<FilteredVariants />} />
      )
      break
    default:
      throw new Error(`Internal error: non-existing view mode: ${context.uiMode}`)
  }

  return (
    <analysisInterpretationUIContext.Provider value={context}>
      {layout}
    </analysisInterpretationUIContext.Provider>
  )
}
