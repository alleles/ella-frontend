Feature: VISUAL genome browser and track selector

  Background:
    Given state "test-bdd"

  Scenario: Changing track selection and switching alleles
    Given I am logged in as "testuser4"
    When I view the analysis with id 1
    And I go to the VISUAL page

    # Check default track selection
    Then I should see the track label "Variants"
    And I should see the track label "Gene panel"
    And I should see the track label "Classifications"
    And I should see the track label "BAM: Proband"

    # Check turning on presets
    When I press "Gene/region"
    Then I should see the track label "RefSeq Curated"
    And I should see the track label "Gene annotation"
    And I should see the track label "SegDup"

    When I press "Protein"
    Then I should see the track label "UniProt domains"

    When I press "Raw"
    Then I should see the track label "BAM: Father"
    And I should see the track label "BAM: Mother"

    # Check turning off presets
    When I press "Gene/region"
    ## Becase "Gene panel" is part of Default
    Then I should see the track label "Gene panel"
    And I should not see the track label "RefSeq Curated"
    And I should not see the track label "Gene annotation"
    And I should not see the track label "SegDup"

    When I press "Protein"
    Then I should not see the track label "UniProt domains"

    When I press "Raw"
    ## Because "BAM: Proband" is part of Default
    Then I should see the track label "BAM: Proband"
    And I should not see the track label "BAM: Father"
    And I should not see the track label "BAM: Mother"

    When I press "Default"
    Then I should not see the track label "Variants"
    And I should not see the track label "Gene panel"
    And I should not see the track label "Classifications"
    And I should not see the track label "BAM: Proband"

    # Check that turning on all tracks of a preset turns on preset
    ## All tracks are now off; turn on individual tracks of preset
    When I press "Gene panel"
    And I press "RefSeq Curated"
    And I press "Gene annotation"
    And I press "SegDup"
    ## Turn off preset; if not all preset tracks were selected, this would instead turn on all preset tracks
    And I press "Gene/region"
    Then I should not see the track label "Gene panel"
    And I should not see the track label "RefSeq Curated"
    And I should not see the track label "Gene annotation"
    And I should not see the track label "SegDup"

    # Check that track is not available to usergroup not defined in "limit_to_group"
    Given I am logged in as "testuser7"
    When I view the analysis with id 1
    And I go to the VISUAL page
    Then I should not see "UniProt domains"
    And I should not see "Protein"

    # Check that position changes with selected allele
    When I select the allele row 0 from the allele table
    Then I should see that the position is "1:161,518,308-161,518,357"

    When I select the allele row 1 from the allele table
    Then I should see that the position is "1:240,371,225-240,371,274"
