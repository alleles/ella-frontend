import intersection from 'lodash/intersection'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import { Card } from 'components/elements/Card'
import SectionBox from 'components/elements/SectionBox'

import { selectRichTextTemplates } from 'store/config/configSlice'
import {
  selectAnnotationByAnnotationId,
  selectWorstConsequenceById,
} from 'store/interpretation/annotationSlice'
import {
  fetchCustomAnnotationsForAlleleId,
  selectCustomAnnotationsByAnnotationId,
} from 'store/interpretation/customAnnotationSlice'
import { selectAnnotationFilteredTranscripts } from 'store/selectors/genePanelAnnotation'
import { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import { CustomAnnotation } from 'types/store/CustomAnnotation'

import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'
import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import ConfigurableAnnotation from 'views/interpretation/components/annotation/configurable/ConfigurableAnnotation'
import updateCustomAnnotations from 'views/interpretation/helpers/annotation'
import { ConsequenceTranscript } from 'views/interpretation/sections/prediction/ConsequenceTranscript'
import { PredictionOptionSelector } from 'views/interpretation/sections/prediction/PredictionOptionSelector'

type PredictionCustomAnnotations = Required<CustomAnnotation['annotations']>['prediction']

export default function Prediction() {
  const dispatch = useDispatch()

  const { alleleState, setAlleleState, selectedAlleleId, readOnly } =
    useInterpretationStateContext()

  const { interpretation, presentationAlleles } = useInterpretation()
  const { annotationId, customAnnotationId } = presentationAlleles[selectedAlleleId]

  const { reloadAlleles } = useAnalysisInterpretation()
  const annotation = useSelector((state: RootState) =>
    selectAnnotationByAnnotationId(state, { annotationId }),
  )

  const customAnnotations = useSelector(
    customAnnotationId
      ? (state: RootState) =>
          selectCustomAnnotationsByAnnotationId(state, {
            customAnnotationId,
          })
      : () => undefined,
  )

  const filteredTranscripts = useSelector((state: RootState) =>
    selectAnnotationFilteredTranscripts(state, {
      annotationId,
      genePanelName: interpretation.genePanelName,
      genePanelVersion: interpretation.genePanelVersion,
    }),
  )

  const worstConsequence = useSelector((state: RootState) =>
    selectWorstConsequenceById(state, { annotationId }),
  )

  const currentUser = useSelector(selectCurrentUser)

  const [comment, setComment] = useState(
    alleleState?.alleleAssessment.evaluation.prediction.comment,
  )

  useEffect(() => {
    setComment(alleleState?.alleleAssessment.evaluation.prediction.comment)
  }, [selectedAlleleId])

  const onCommentChange = (value: string) => {
    setAlleleState({
      alleleAssessment: {
        evaluation: {
          prediction: {
            comment: value,
          },
        },
      },
    })
  }

  if (!currentUser) {
    throw new Error('No logged in user found.')
  }

  const annotationWithAllValuesSetToNull: PredictionCustomAnnotations = {
    dnaConservation: null,
    domain: null,
    orthologConservation: null,
    paralogConservation: null,
    repeat: null,
    spliceEffectManual: null,
  }

  const [values, setValues] = useState<PredictionCustomAnnotations>(
    customAnnotations?.annotations.prediction ?? annotationWithAllValuesSetToNull,
  )

  const saveNewCustomAnnotations = async (
    newAnnotationValues: PredictionCustomAnnotations,
  ) => {
    // Use state value for immediate response in UI,
    // But will be reset with reloadAlleles below
    setValues(newAnnotationValues)
    await updateCustomAnnotations({
      user: currentUser,
      alleleId: selectedAlleleId,
      newAnnotationValues,
      currentCustomAnnotations: customAnnotations,
      customAnnotationsType: 'prediction',
    })
    await dispatch(fetchCustomAnnotationsForAlleleId(selectedAlleleId))
    await reloadAlleles([selectedAlleleId])
  }

  return (
    <SectionBox title="Prediction" color="ellapurple" initCollapsed={false}>
      <Editor
        key={`prediction-comment-${selectedAlleleId}`}
        id="prediction-comment"
        placeholder="Prediction - comments"
        initialValue={comment}
        onBlur={onCommentChange}
        templates={useSelector(selectRichTextTemplates('classificationPrediction'))}
        readOnly={readOnly}
      />
      <div className="flex flex-wrap">
        <Card label="Consequence">
          {worstConsequence &&
            intersection(worstConsequence.transcripts, filteredTranscripts).length ===
              0 && (
              <div className="bg-ellared-dark">
                <p>
                  Worse consequence: <span>{worstConsequence.consequence}</span>
                </p>
                <p>Found in:</p>
                <ul>
                  {worstConsequence.transcripts.map((worstConsequenceTranscript) => (
                    <li>{worstConsequenceTranscript}</li>
                  ))}
                </ul>
              </div>
            )}

          {filteredTranscripts.map((transcript) => (
            <ConsequenceTranscript
              transcriptKey={transcript}
              annotation={annotation}
              key={transcript}
            />
          ))}
        </Card>

        {/* TODO: Remove headers when config labels have been updated */}
        <Card label="Conservation">
          <div className="pb-2 text-sm">Ortholog</div>
          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="orthologConservation"
            disabled={readOnly}
          />

          <div className="pb-2 text-sm">Paralog</div>
          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="paralogConservation"
            disabled={readOnly}
          />

          <div className="pb-2 text-sm">DNA</div>
          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="dnaConservation"
            disabled={readOnly}
          />
        </Card>

        <Card label="Domain">
          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="domain"
            disabled={readOnly}
          />

          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="repeat"
            disabled={readOnly}
          />
        </Card>

        <Card label="Splice site">
          <PredictionOptionSelector
            values={values}
            onChange={saveNewCustomAnnotations}
            propertyKey="spliceEffectManual"
            disabled={readOnly}
          />
        </Card>
        <ConfigurableAnnotation section="prediction" />
      </div>
    </SectionBox>
  )
}
