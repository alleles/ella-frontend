import {
  CheckCircleIcon,
  ExclamationCircleIcon,
  XCircleIcon,
  XMarkIcon,
} from '@heroicons/react/24/solid'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import FadeTransition from 'components/transitions/FadeTransition'

import { removeAlert, selectAlerts } from 'store/alerts/alertsSlice'

import { Alert as AlertType } from 'types/Alert'

interface AlertProps {
  alert: AlertType
}

type HeroIcon = (props: React.ComponentProps<'svg'>) => JSX.Element

function Alert({ alert }: AlertProps) {
  const { title, body, type } = alert
  const dispatch = useDispatch()

  let color: string
  let Icon: HeroIcon

  switch (type) {
    case 'error':
      color = 'ellared'
      Icon = XCircleIcon

      break
    case 'warning':
      color = 'ellayellow'
      Icon = ExclamationCircleIcon

      break
    default:
      color = 'ellagreen'
      Icon = CheckCircleIcon
      break
  }

  const close = (ev) => {
    ev.preventDefault()
    dispatch(removeAlert(alert.id))
  }

  return (
    <div
      className={`bg-${color}-light border border-${color}-dark mx-2 mt-16 rounded p-3 shadow`}
    >
      <div className="flex">
        <div className="shrink-0">
          <Icon className={`h-5 w-5 text-${color}-dark`} aria-hidden="true" />
        </div>
        <div className="ml-3">
          <p className={`text-sm font-bold text-${color}-dark`}>{title}</p>

          <p className={`text-sm font-medium text-${color}-dark`}>{body}</p>
        </div>
        <div className="ml-auto pl-3">
          <div className="-mx-1.5 -my-1.5">
            <button
              type="button"
              className={`inline-flex p-1.5 text-${color}-dark hover:text-${color}`}
              onClick={close}
            >
              <XMarkIcon className="h-5 w-5" aria-hidden="true" />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default function AlertBar() {
  const alerts = useSelector(selectAlerts)
  return (
    <FadeTransition show={alerts.length > 0}>
      <div className="absolute z-30 w-full">
        {alerts.map((alert) => (
          <Alert key={alert.id} alert={alert} />
        ))}
      </div>
    </FadeTransition>
  )
}
