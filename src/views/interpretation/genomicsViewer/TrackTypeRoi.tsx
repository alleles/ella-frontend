import { deepCopy } from 'utils/general'

import { TrackTypeBase } from 'views/interpretation/genomicsViewer/TrackTypeBase'

/* eslint-disable class-methods-use-this */

// ROI tracks
export class TrackTypeRoi extends TrackTypeBase {
  matchesType(type: String | null): boolean {
    return type === 'roi'
  }

  getIgvLoadedTrackIds(browser: any): String[] {
    return (browser.roi ? browser.roi : []).map((roi) => roi.name)
  }

  removeTrack(browser: any, trackId: String): void {
    browser.removeROI({ name: trackId })
  }

  async loadTrack(browser: any, igvcfg: any): Promise<void> {
    await browser.loadROI(deepCopy<any>(igvcfg))
  }
}

/* eslint-enable class-methods-use-this */
