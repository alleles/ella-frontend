// Full-width on mobile, constrained with padded content above
import React, { ReactNode } from 'react'

interface Props {
  children: ReactNode
}

export default function PaddedContainer({ children }: Props) {
  return <div className="p-2 pt-4">{children}</div>
}
