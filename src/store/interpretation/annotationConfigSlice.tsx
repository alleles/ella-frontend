import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'
import { CommonRequestBuilder, CommonRequestCondition, Requests } from 'store/utils'

import { AnnotationConfig, mapFromApi } from 'types/store/AnnotationConfig'

const initialState: Record<number, AnnotationConfig> & Requests = { requests: {} }

const fetchAnnotationConfig = createAsyncThunk<Record<number, AnnotationConfig>, number>(
  'annotationConfig/fetchAnnotationConfig',
  async (annotationConfigId) => {
    const annotationConfig = await API.AnnotationConfigs.get(annotationConfigId).then(
      (result) => result.data[0],
    )

    const { id } = annotationConfig
    return { [id]: mapFromApi(annotationConfig) }
  },
  CommonRequestCondition('annotationConfigs'),
)

const annotationConfigSlice = createSlice({
  name: 'annotationConfigs',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    CommonRequestBuilder(builder, fetchAnnotationConfig, {
      fulfilled: (state, action) => {
        Object.assign(state, action.payload)
      },
    })
  },
})

const selectAnnotationConfigById = (
  state: RootState,
  { annotationConfigId }: { annotationConfigId: number },
) => state.annotationConfigs[annotationConfigId]

export { fetchAnnotationConfig, selectAnnotationConfigById }

export default annotationConfigSlice.reducer
