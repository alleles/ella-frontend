import { ApiIgvConfig } from 'types/api/TrackConfigs'

export interface TrackConfig {
  presets: [string, ...string[]] // at leaset one preset needs to be defined
  igv: ApiIgvConfig
  type: null | 'roi'
  selected: boolean
  description: string | undefined
}
