import { ArrowLeftOnRectangleIcon } from '@heroicons/react/24/solid'
import React from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import useAuth from 'app/auth'

import Menu from 'components/elements/DropDownMenu'

import { selectCurrentUser } from 'store/users/usersSlice'

export default function UserDropDownMenu() {
  const userData = useSelector(selectCurrentUser)
  const { logout } = useAuth()
  const navigate = useNavigate()

  return (
    <Menu id="user-menu" title={userData?.abbrevName ?? 'Not logged in'}>
      <Menu.MenuItem
        title="Profile"
        onClick={() => {
          navigate('/profile')
        }}
      />
      <Menu.MenuItem
        title="Logout"
        onClick={() => {
          logout()
        }}
        LeftIcon={ArrowLeftOnRectangleIcon}
      />
    </Menu>
  )
}
