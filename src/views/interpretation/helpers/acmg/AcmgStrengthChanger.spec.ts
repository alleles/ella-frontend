import { mapFromApi } from 'types/store/Acmg'
import { Config } from 'types/store/Config'

import { AcmgStrengthChanger } from 'views/interpretation/helpers/acmg/AcmgStrengthChanger'

const config = {
  acmg: {
    codes: {
      pathogenic: ['PVS', 'PS', 'PM', 'PP', 'PNW'],
      benign: ['BNW', 'BP', 'BS', 'BA'],
      other: ['OTHER'],
    },
  },
} as Config

describe('ACMG strength changer', () => {
  it('ACMG code of type other can not be downgraded nor upgraded', () => {
    expect(
      AcmgStrengthChanger.canDowngradeCodeObj(mapFromApi({ code: 'OTHER' }), config),
    ).toEqual(false)
    expect(
      AcmgStrengthChanger.canUpgradeCodeObj(mapFromApi({ code: 'OTHER' }), config),
    ).toEqual(false)
  })

  it('ACMG code of type benign can be downgraded to the lowest level', () => {
    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BA' }), config),
    ).toEqual(mapFromApi({ code: 'BSxBA' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BS' }), config),
    ).toEqual(mapFromApi({ code: 'BPxBS' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BP' }), config),
    ).toEqual(mapFromApi({ code: 'BNWxBP' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BNW' }), config),
    ).toEqual(mapFromApi({ code: 'BNW' }))
  })

  it('ACMG code of type benign can be upgraded to the highest level', () => {
    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BNW' }), config),
    ).toEqual(mapFromApi({ code: 'BPxBNW' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BP' }), config).code,
    ).toEqual('BSxBP')

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BS' }), config).code,
    ).toEqual('BAxBS')

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BA' }), config).code,
    ).toEqual('BA')
  })

  it('ACMG code of type pathogenic can be downgraded to the lowest level', () => {
    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PVS' }), config),
    ).toEqual(mapFromApi({ code: 'PSxPVS' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PS' }), config),
    ).toEqual(mapFromApi({ code: 'PMxPS' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PM' }), config),
    ).toEqual(mapFromApi({ code: 'PPxPM' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PP' }), config),
    ).toEqual(mapFromApi({ code: 'PNWxPP' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PNW' }), config),
    ).toEqual(mapFromApi({ code: 'PNW' }))
  })

  it('ACMG code of type pathogenic can be upgraded to the highest level', () => {
    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PNW' }), config),
    ).toEqual(mapFromApi({ code: 'PPxPNW' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PP' }), config).code,
    ).toEqual('PMxPP')

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PM' }), config).code,
    ).toEqual('PSxPM')

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PS' }), config).code,
    ).toEqual('PVSxPS')

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PVS' }), config),
    ).toEqual(mapFromApi({ code: 'PVS' }))
  })

  it('ACMG code with combination of strength and code can be upgraded and downgraded', () => {
    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PMxPP' }), config),
    ).toEqual(mapFromApi({ code: 'PSxPP' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BSxBP' }), config),
    ).toEqual(mapFromApi({ code: 'BAxBP' }))
  })

  it('Helper does not upgrade/downgrade ACMG code with combination of strength and code if at top or bottom', () => {
    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PVSxPP' }), config),
    ).toEqual(mapFromApi({ code: 'PVSxPP' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BAxBP' }), config),
    ).toEqual(mapFromApi({ code: 'BAxBP' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PNWxPP' }), config),
    ).toEqual(mapFromApi({ code: 'PNWxPP' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BNWxBS' }), config),
    ).toEqual(mapFromApi({ code: 'BNWxBS' }))
  })

  it('Upgrading or downgrading ACMG code with combination of strength and code to base strength, removes modifier', () => {
    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'PMxPP' }), config),
    ).toEqual(mapFromApi({ code: 'PP' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'PNWxPP' }), config),
    ).toEqual(mapFromApi({ code: 'PP' }))

    expect(
      AcmgStrengthChanger.downgradeCodeObj(mapFromApi({ code: 'BAxBS' }), config),
    ).toEqual(mapFromApi({ code: 'BS' }))

    expect(
      AcmgStrengthChanger.upgradeCodeObj(mapFromApi({ code: 'BPxBS' }), config),
    ).toEqual(mapFromApi({ code: 'BS' }))
  })
})
