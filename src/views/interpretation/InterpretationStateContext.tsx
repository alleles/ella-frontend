import React, {
  Context,
  ReactElement,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import API from 'app/API'

import {
  processAndStoreAlleleAssessment,
  selectAlleleAssessmentsByIds,
} from 'store/interpretation/alleleAssessmentSlice'
import {
  fetchAlleleInterpretationsForAlleleId,
  selectAlleleInterpretationById,
} from 'store/interpretation/alleleInterpretationSlice'
import {
  processAndStoreAlleleReport,
  selectAlleleReportsByIds,
} from 'store/interpretation/alleleReportSlice'
import {
  fetchAnalysisInterpretationsByAnalysisId,
  selectAnalysisInterpretationById,
} from 'store/interpretation/analysisInterpretationSlice'
import { fetchAnalysisByAnalysisId } from 'store/interpretation/analysisSlice'
import { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import { mapToApiNewAlleleAssessment } from 'types/store/AlleleAssessment'
import {
  AlleleInterpretation,
  AlleleState,
  mapToApi as mapToAlleleInterpretationApi,
} from 'types/store/AlleleInterpretation'
import {
  AnalysisAlleleState,
  AnalysisInterpretation,
  mapToApi as mapToAnalysisInterpretationApi,
} from 'types/store/AnalysisInterpretation'
import { User } from 'types/store/User'

import { deepCopy, deepMerge, nonNullable } from 'utils/general'
import { DeepPartial } from 'utils/types'

import { useInterpretation } from 'views/interpretation/InterpretationContext'

let interpretationStateContext: Context<InterpretationStateContext>

const DEFAULT_ALLELE_STATE: AlleleState = {
  reuseAlleleReport: false,
  referenceAssessments: [],
  alleleAssessmentId: null,
  reuseAlleleAssessment: false,
  alleleReportId: null,
  referenceAssessmentIds: [],
  alleleReport: {
    comment: '',
  },
  alleleAssessment: {
    classification: null,
    attachmentIds: [],
    evaluation: {
      classification: {
        comment: '',
      },
      frequency: {
        comment: '',
      },
      reference: {
        comment: '',
      },
      similar: {
        comment: '',
      },
      external: {
        comment: '',
      },
      prediction: {
        comment: '',
      },
      acmg: {
        included: [],
        suggested: [],
        suggestedClassification: undefined,
      },
    },
  },
}

const DEFAULT_ANALYSIS_ALLELE_STATE: AnalysisAlleleState = {
  ...DEFAULT_ALLELE_STATE,
  analysis: {
    verification: null,
    notrelevant: false,
    comment: '',
  },
  report: {
    included: null,
  },
  workflow: {
    reviewed: false,
  },
}

export type Interpretation = AlleleInterpretation | AnalysisInterpretation
export type InterpretationState = Interpretation['state']

interface InterpretationStateContext {
  id: number
  readOnly: boolean
  alleleIds: number[]
  selectedAlleleId: number
  alleleState: AlleleState | AnalysisAlleleState
  setSelectedAlleleId: (id: number) => void
  submittedClassification: boolean
  interpretationState: InterpretationState
  setInterpretationState: (state: DeepPartial<InterpretationState>) => void
  setAlleleState: (state: DeepPartial<AlleleState | AnalysisAlleleState>) => void
  save: () => Promise<void>
  submitClassification: ({
    alleleId,
    annotationId,
    customAnnotationId,
  }: {
    alleleId: number
    annotationId: number
    customAnnotationId: number | null
  }) => Promise<void>
  submitReport: ({
    alleleId,
    annotationId,
    customAnnotationId,
  }: {
    alleleId: number
    annotationId: number
    customAnnotationId: number | null
  }) => Promise<void>
}

function useInterpretationStateContext(): InterpretationStateContext {
  const {
    id,
    type,
    presentationAlleles,
    interpretation: { id: interpretationId },
  } = useInterpretation()
  const alleleIds = Object.keys(presentationAlleles).map(Number)

  // Use alleleIds to set annotationIds, customAnnotationIds
  // Set selected{alleleId,annotationId,customAnnotationId,alleleState}
  // Export save function
  const dispatch = useDispatch()

  const [selectedAlleleId, setSelectedAlleleId] = useState<number>(alleleIds[0])

  const user = useSelector(selectCurrentUser) as User

  const defaultAlleleState =
    type === 'allele' ? DEFAULT_ALLELE_STATE : DEFAULT_ANALYSIS_ALLELE_STATE

  const interpretation = useSelector((state: RootState) =>
    type === 'allele'
      ? selectAlleleInterpretationById(state, {
          alleleInterpretationId: interpretationId,
        })
      : selectAnalysisInterpretationById(state, {
          analysisInterpretationId: interpretationId,
        }),
  )!

  // Evaluate initial interpretation state
  const initialInterpretationState = deepCopy(interpretation?.state)
  const alleleAssessments = useSelector((state: RootState) =>
    selectAlleleAssessmentsByIds(state, {
      alleleAssessmentIds: Object.values(presentationAlleles)
        .map(({ assessmentId }) => assessmentId)
        .filter(nonNullable),
    }),
  )
  const alleleReports = useSelector((state: RootState) =>
    selectAlleleReportsByIds(state, {
      alleleReportIds: Object.values(presentationAlleles)
        .map(({ reportId }) => reportId)
        .filter(nonNullable),
    }),
  )

  alleleIds.forEach((alleleId) => {
    initialInterpretationState.allele[alleleId] = initialInterpretationState.allele?.[
      alleleId
    ]
      ? initialInterpretationState.allele?.[alleleId]
      : // Make a copy of the default allele state, otherwise changes  will be propagated to all alleles
        deepCopy(defaultAlleleState)

    // Presentation alleles holds the truth for which assessments and reports should be used,
    // but the interpretation state may hold a different assessment or report.
    // Override the interpretation state with the presentation alleles.
    // If any work has been done in this interpretation on a previous assessment/report,
    // that work will not be preserved.
    const { assessmentId: assessmentIdToEnforce, reportId: reportIdToEnforce } =
      presentationAlleles[alleleId]

    if (assessmentIdToEnforce && !alleleAssessments[assessmentIdToEnforce]) {
      throw new Error(
        `Assessment with id ${assessmentIdToEnforce} not selected from store. This should not happen.`,
      )
    }

    if (reportIdToEnforce && !alleleReports[reportIdToEnforce]) {
      throw new Error(
        `Report with id ${reportIdToEnforce} not selected from store. This should not happen.`,
      )
    }

    if (
      assessmentIdToEnforce &&
      assessmentIdToEnforce !==
        initialInterpretationState.allele[alleleId].alleleAssessmentId
    ) {
      // Override state allele assessment with presentation allele assessment
      initialInterpretationState.allele[alleleId] = {
        ...initialInterpretationState.allele[alleleId],
        alleleAssessmentId: assessmentIdToEnforce,
        alleleAssessment: {
          evaluation: alleleAssessments[assessmentIdToEnforce]!.evaluation,
          classification: alleleAssessments[assessmentIdToEnforce]!.classification,
          attachmentIds: alleleAssessments[assessmentIdToEnforce]!.attachmentIds,
        },
        reuseAlleleAssessment: true,
      }
    }

    if (
      reportIdToEnforce &&
      reportIdToEnforce !== initialInterpretationState.allele[alleleId].alleleReportId
    ) {
      // Override state allele report with presentation allele report
      initialInterpretationState.allele[alleleId] = {
        ...initialInterpretationState.allele[alleleId],
        alleleReportId: reportIdToEnforce,
        alleleReport: {
          comment: alleleReports[reportIdToEnforce]!.evaluation.comment,
        },
        reuseAlleleReport: true,
      }
    }
  })

  const [interpretationState, setInterpretationState] = useState(
    initialInterpretationState,
  )

  const [pendingSave, setPendingSave] = useState(false)
  const save = async () => {
    setPendingSave(true)
  }
  useEffect(() => {
    const saveAndReFetch = async () => {
      if (type === 'allele') {
        const copiedInterpretation = deepCopy(interpretation) as AlleleInterpretation
        deepMerge(copiedInterpretation, { state: interpretationState })

        await API.SaveAlleleInterpretation.patch({
          alleleInterpretationId: interpretation.id,
          alleleId: copiedInterpretation.alleleId,
          alleleInterpretation: mapToAlleleInterpretationApi(
            copiedInterpretation,
            interpretation.id,
            user,
          ),
        })
        await dispatch(
          fetchAlleleInterpretationsForAlleleId(copiedInterpretation.alleleId),
        )
      } else {
        const copiedInterpretation = deepCopy(interpretation) as AnalysisInterpretation
        deepMerge(copiedInterpretation, { state: interpretationState })
        await API.SaveAnalysisInterpretation.patch({
          analysisInterpretationId: interpretation.id,
          analysisId: copiedInterpretation.analysisId,
          analysisInterpretation: mapToAnalysisInterpretationApi(
            copiedInterpretation,
            user,
          ),
        })
        await dispatch(fetchAnalysisByAnalysisId(copiedInterpretation.analysisId))
        await dispatch(
          fetchAnalysisInterpretationsByAnalysisId(copiedInterpretation.analysisId),
        )
      }

      setPendingSave(false)
    }
    if (pendingSave) {
      saveAndReFetch()
    }
  }, [pendingSave])

  const submitClassification = async (
    {
      alleleId,
      annotationId,
      customAnnotationId,
    }: {
      alleleId: number
      annotationId: number
      customAnnotationId: number | null
    },
    reportOnly = false,
  ) => {
    const alleleAssessment = reportOnly
      ? {
          allele_id: alleleId,
          reuse: true as const,
          presented_alleleassessment_id: alleleState.alleleAssessmentId!,
        }
      : mapToApiNewAlleleAssessment(alleleId, alleleState.alleleAssessment)

    const alleleReport = {
      allele_id: alleleId,
      reuse: false as const,
      evaluation: { comment: alleleState.alleleReport.comment },
      presented_allelereport_id: alleleState.alleleReportId ?? undefined,
    }
    const { newAlleleAssessmentId, newAlleleReportId } =
      await API.SubmitAlleleAssessment.post(id, type, {
        allele_id: alleleId,
        alleleassessment: alleleAssessment,
        allelereport: alleleReport,
        annotation_id: annotationId,
        custom_annotation_id: customAnnotationId ?? undefined,
        referenceassessments: [], // TODO: implement mapToApi for reference assessments.
      }).then((result) => {
        if (!reportOnly) {
          dispatch(processAndStoreAlleleAssessment(result.data.alleleassessment))
        }

        dispatch(processAndStoreAlleleReport(result.data.allelereport))
        return {
          newAlleleAssessmentId: result.data.alleleassessment.id,
          newAlleleReportId: result.data.allelereport.id,
        }
      })

    // Set allele assessment to be reused
    if (!reportOnly) {
      mutateAlleleState({
        alleleAssessmentId: newAlleleAssessmentId,
        reuseAlleleAssessment: true,
        alleleReportId: newAlleleReportId,
        reuseAlleleReport: true,
      })
    } else {
      mutateAlleleState({
        alleleReportId: newAlleleReportId,
        reuseAlleleReport: true,
      })
    }

    save()
  }

  const submitReport = async ({
    alleleId,
    annotationId,
    customAnnotationId,
  }: {
    alleleId: number
    annotationId: number
    customAnnotationId: number | null
  }) => submitClassification({ alleleId, annotationId, customAnnotationId }, true)

  const mutateInterpretationState = (newState: DeepPartial<InterpretationState>) => {
    const state = deepCopy(interpretationState)
    deepMerge(state, newState)
    setInterpretationState(state)
  }

  const mutateAlleleState = (newState: DeepPartial<AlleleState>, alleleId?: number) =>
    mutateInterpretationState({
      allele: {
        [alleleId === undefined ? selectedAlleleId : alleleId]: newState,
      },
    })

  const readOnly =
    interpretation &&
    (interpretation.status === 'Not started' || interpretation.userId !== user.id)

  const alleleState = interpretationState.allele[selectedAlleleId]

  // The following flag is currently used to determine whether to show the
  // "Submit" button in the Classification section as well as lock fields.
  // This is a temporary solution until we logic is readt to handle resubmitting etc.
  const submittedClassification = !!Object.values(alleleAssessments).find(
    (assessment) => assessment!.alleleId === selectedAlleleId,
  )

  return {
    id,
    readOnly,
    alleleIds,
    selectedAlleleId,
    setSelectedAlleleId,
    alleleState,
    submittedClassification,
    interpretationState,
    setInterpretationState: mutateInterpretationState,
    setAlleleState: mutateAlleleState,
    save,
    submitClassification,
    submitReport,
  }
}

export function InterpretationStateProvider({
  children,
}: {
  children: ReactNode
}): ReactElement {
  // We create and assign a ref in order to force HMR to keep the state of the context.
  // Once https://github.com/vitejs/vite/issues/3301 is resolved the ref and assignment can go away.
  const contextRef = useRef()

  // eslint-disable-next-line no-multi-assign
  interpretationStateContext = (contextRef.current as any) ??= createContext<
    DeepPartial<InterpretationStateContext>
  >({})

  const context = useInterpretationStateContext()
  return (
    <interpretationStateContext.Provider value={context}>
      {children}
    </interpretationStateContext.Provider>
  )
}

export default function InterpretationStateConsumer(): InterpretationStateContext {
  return useContext(
    interpretationStateContext as unknown as Context<InterpretationStateContext>,
  )
}
