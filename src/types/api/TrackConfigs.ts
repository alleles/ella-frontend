// TODO

// track config as received from API

export interface ApiIgvConfig {
  name: string
  [k: string]: any // we don't care in ella - we leave it up to IGV.js
}

export interface ApiTrackConfig {
  applied_rules: string[]
  presets: string[]
  igv: ApiIgvConfig
  type?: null | 'roi'
  show?: any
  description?: string
}

export interface ApiTrackConfigs {
  [k: string]: ApiTrackConfig
}
