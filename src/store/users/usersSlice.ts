import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { User, mapFromApi } from 'types/store/User'

type SliceState = { currentUserId: number | null; users: Record<number, User> }

const initialState: SliceState = { currentUserId: null, users: {} }

export const fetchCurrentUserId = createAsyncThunk<number>(
  'user/fetchCurrentUser',
  async () => {
    const user = await API.CurrentUser.get().then((response) => response.data)
    return user.id
  },
)

export const fetchUsers = createAsyncThunk<Record<number, User>>(
  'users/fetchUsers',
  async () => {
    const users = await API.Users.get().then((response) => response.data)
    const mappedUsers = users.reduce(
      (prev, current) => ({
        ...prev,
        [current.id]: mapFromApi(current),
      }),
      {} as Record<number, User>,
    )
    return mappedUsers
  },
)

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCurrentUserId.fulfilled, (state, action) =>
      Object.assign(state, { currentUserId: action.payload }),
    )
    builder.addCase(fetchUsers.fulfilled, (state, action) =>
      Object.assign(state, { users: action.payload }),
    )
  },
})

export const selectUserById = (state: RootState, { userId }: { userId: number }) =>
  state.users.users[userId]

export const selectCurrentUser = (state: RootState) => {
  if (!state.users.currentUserId) {
    return undefined
  }
  return state.users.users[state.users.currentUserId]
}

export const selectCurrentUsername = (state: RootState) =>
  selectCurrentUser(state)?.username

export const selectUsers = (state: RootState) => state.users.users

export const selectUsersInOwnGroup = createSelector(
  selectCurrentUser,
  selectUsers,
  (me, users) => {
    if (!me) {
      return []
    }
    return Object.values(users).filter(
      (user) => user.group?.id === me.group?.id && user.id !== me.id,
    )
  },
)

export default userSlice.reducer
