import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import alleleInterpretationLogReducer, {
  fetchAlleleInterpretationLogForAlleleId,
  selectAlleleInterpretationLogByAlleleId,
} from 'store/interpretation/alleleInterpretationLogSlice'
import { store } from 'store/store'

import { InterpretationLogItem } from 'types/store/InterpretationLog'

const mock = new MockAdapter(axios)

function buildInterpretationLogItem(
  override: Partial<InterpretationLogItem>,
): InterpretationLogItem {
  return {
    id: 0,
    alleleAssessment: null,
    alleleReport: null,
    dateCreated: '1970-01-01',
    message: null,
    priority: null,
    userId: null,
    warningCleared: null,
    editable: false,
    reviewComment: null,
    ...override,
  }
}

describe('allele intepretation log reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(alleleInterpretationLogReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching allele intepretation logs from the backend', async () => {
    mock.onGet(API.AlleleInterpreationLogs.path(1)).reply(200, {
      logs: [
        { id: 1, message: 'foo', editable: false },
        { id: 2, message: 'bar', editable: false },
      ],
    })

    const dispatch = jest.fn()
    await fetchAlleleInterpretationLogForAlleleId(1)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          1: [
            buildInterpretationLogItem({ id: 1, message: 'foo', editable: false }),
            buildInterpretationLogItem({ id: 2, message: 'bar', editable: false }),
          ],
        },
      }),
    )
  })

  it('should handle getting allele interpretation logs by allele id', async () => {
    const state = {
      ...store.getState(),
      alleleInterpretationLog: {
        1: [buildInterpretationLogItem({ message: 'foo' })],
        2: [buildInterpretationLogItem({ message: 'bar' })],
      },
    }

    expect(selectAlleleInterpretationLogByAlleleId(state, { alleleId: 1 })).toEqual([
      buildInterpretationLogItem({ message: 'foo' }),
    ])
    expect(selectAlleleInterpretationLogByAlleleId(state, { alleleId: 2 })).toEqual([
      buildInterpretationLogItem({ message: 'bar' }),
    ])
    expect(selectAlleleInterpretationLogByAlleleId(state, { alleleId: 3 })).toEqual(
      undefined,
    )
  })
})
