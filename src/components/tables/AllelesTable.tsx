import { format } from 'date-fns'
import React from 'react'
import { useSelector } from 'react-redux'

import Link from 'components/elements/Link'
import Tag from 'components/elements/Tag'
import Row from 'components/elements/table/Row'
import Table from 'components/elements/table/Table'

import { selectPriorityText } from 'store/config/configSlice'
import { selectHGVSgByAlleleId } from 'store/interpretation/alleleSlice'
import { selectAlleleDisplay } from 'store/selectors/genePanelAnnotation'
import { RootState } from 'store/store'

import { AlleleOverview } from 'types/store/AlleleOverview'

import InterpretationsSummary from 'views/overview/components/InterpretationsSummary'

interface Props {
  alleles: AlleleOverview[]
}

export default function AllelesTable({ alleles = [] }: Props) {
  return (
    <Table className="mb-1 -mt-1 w-full table-fixed">
      {alleles.map((allele) => (
        <AlleleItem allele={allele} key={allele.alleleId} />
      ))}
    </Table>
  )
}

interface AlleleItemProps {
  allele: AlleleOverview
}

function AlleleItem({ allele }: AlleleItemProps) {
  const { alleleId, annotationId, genePanel, reviewComment } = allele

  const priorityText = useSelector(selectPriorityText)
  const hgvsg = useSelector((state: RootState) =>
    selectHGVSgByAlleleId(state, { alleleId }),
  )
  const display = useSelector((state: RootState) =>
    selectAlleleDisplay(state, {
      alleleId,
      annotationId,
      genePanelName: genePanel?.name,
      genePanelVersion: genePanel?.version,
    }),
  )

  const formattedDisplay = display.length
    ? display.map((d) => `${d.transcript}(${d.geneSymbol}):${d.hgvsc}`).join(' | ')
    : hgvsg

  return (
    <Row>
      {allele.dateCreated !== 'N/A' && (
        <td className="w-1/12">{format(new Date(allele.dateCreated), 'yyyy-MM-dd')}</td>
      )}
      {Boolean(allele.priority) && (
        <td className="w-1/12 font-mono text-xs uppercase tracking-wide">
          {priorityText[allele.priority]}
        </td>
      )}
      <td className="flex gap-x-2 pr-2">
        <Link to={`/variants/${alleleId}`}>
          <span title={formattedDisplay}>{formattedDisplay}</span>
        </Link>
        <div className="overflow-visible whitespace-nowrap">
          <Tag tag={allele.classification ? `CLASS ${allele.classification}` : 'NEW'} />
        </div>
      </td>
      <td className="w-1/12">
        {allele.genePanel && `${allele.genePanel.name}_${allele.genePanel.version}`}
      </td>
      <td className="font-normal">{reviewComment}</td>
      <td className="text-right">
        <InterpretationsSummary interpretations={allele.interpretations} />
      </td>
    </Row>
  )
}
