import * as ApiTypes from 'types/api/Pydantic'
import * as AlleleInterpretationOverview from 'types/store/AlleleInterpretationOverview'
import * as GenePanel from 'types/store/GenePanel'

export interface AlleleOverview {
  alleleId: number
  annotationId: number
  classification: string | null
  dateCreated: string
  genePanel: GenePanel.GenePanel | null
  interpretations: AlleleInterpretationOverview.AlleleInterpretationOverview[]
  priority: number
  reviewComment?: string
}

export function mapFromApi(
  alleleOverviewFromApi: ApiTypes.AlleleOverview,
): AlleleOverview {
  return {
    alleleId: alleleOverviewFromApi.allele.id,
    annotationId: alleleOverviewFromApi.allele.annotation.annotation_id,
    classification:
      alleleOverviewFromApi.allele.allele_assessment?.classification || null,
    dateCreated: alleleOverviewFromApi.date_created,
    genePanel: alleleOverviewFromApi.interpretations.length
      ? GenePanel.mapFromApi(alleleOverviewFromApi.genepanel)
      : null,
    interpretations: alleleOverviewFromApi.interpretations.map(
      AlleleInterpretationOverview.mapFromApi,
    ),
    priority: alleleOverviewFromApi.priority,
    reviewComment: alleleOverviewFromApi.review_comment,
  }
}
