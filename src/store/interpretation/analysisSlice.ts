import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import { processAndStoreFilterConfig } from 'store/interpretation/filterConfigSlice'
import { processAndStoreSample } from 'store/interpretation/sampleSlice'
import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Analysis, mapFromApi } from 'types/store/Analysis'

const initialState: Record<number, Analysis> = {}

const fetchAnalysisByAnalysisId = createAsyncThunk<Record<number, Analysis>, number>(
  'analysis/fetchAnalysisByAnalysisId',
  async (analysisId, { dispatch }) => {
    const apiAnalysis = await API.Analysis.get(analysisId).then((result) => result.data)

    apiAnalysis.samples.forEach((sample) => dispatch(processAndStoreSample(sample)))

    const apiFilterConfigs = await API.AnalysisFilterConfigs.get(analysisId).then(
      (result) => result.data,
    )
    const filterConfigIds = apiFilterConfigs.map((fc) => fc.id)
    apiFilterConfigs.forEach((fc) => dispatch(processAndStoreFilterConfig(fc)))

    const analysis = mapFromApi(apiAnalysis, filterConfigIds)

    return { [apiAnalysis.id]: analysis }
  },
)

const processAndStoreAnalysis = createAsyncThunk<
  Record<number, Analysis>,
  ApiTypes.OverviewAnalysis
>('analysis/processAndStoreAnalysis', async (apiAnalysis, { dispatch }) => {
  const apiFilterConfigs = await API.AnalysisFilterConfigs.get(apiAnalysis.id).then(
    (result) => result.data,
  )
  const filterConfigIds = apiFilterConfigs.map((fc) => fc.id)
  apiFilterConfigs.forEach((fc) => dispatch(processAndStoreFilterConfig(fc)))
  return {
    [apiAnalysis.id]: mapFromApi(apiAnalysis, filterConfigIds),
  }
})

const analysisSlice = createSlice({
  name: 'analysis',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAnalysisByAnalysisId.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
    builder.addCase(processAndStoreAnalysis.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectAnalysisById = (
  state: RootState,
  { analysisId }: { analysisId: number },
): Analysis | undefined => state.analyses[analysisId]

export { fetchAnalysisByAnalysisId, processAndStoreAnalysis, selectAnalysisById }

export default analysisSlice.reducer
