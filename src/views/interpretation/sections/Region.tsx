import format from 'date-fns/format'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import API from 'app/API'

import Editor from 'components/editor/Editor'
import { Card } from 'components/elements/Card'
import SectionBox from 'components/elements/SectionBox'

import { selectConfig } from 'store/config/configSlice'
import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import { selectAlleleDisplay } from 'store/selectors/genePanelAnnotation'
import { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import * as StoreAlleleAssessment from 'types/store/AlleleAssessment'
import { AlleleAssessment } from 'types/store/AlleleAssessment'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

interface SimilarAllele {
  alleleId: number
  alleleAssessment: AlleleAssessment | null
  annotationId: number
}

interface SimilarAlleleData {
  alleleId: number
  classification: string | undefined
  dateCreated: Date | undefined
  formattedDisplay: string
  linkWorkflow: string
}

function SpinnerSmall() {
  return (
    <div className="flex h-full w-full items-center justify-center">
      <svg
        role="status"
        className="mx-4 inline-block h-8 w-8 animate-spin text-ellagray"
        viewBox="0 0 100 101"
        fill="#859baf" // = ellablue-darker
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
          fill="currentColor"
        />
        <path
          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
          fill="currentFill"
        />
      </svg>
    </div>
  )
}

export default function Region() {
  const { selectedAlleleId, alleleState, setAlleleState, readOnly } =
    useInterpretationStateContext()
  const { interpretation } = useInterpretation()
  const { genePanelName, genePanelVersion } = interpretation

  const [comment, setComment] = useState(
    alleleState.alleleAssessment.evaluation.similar.comment,
  )
  useEffect(() => {
    setComment(alleleState.alleleAssessment.evaluation.similar.comment)
  }, [selectedAlleleId])

  const onChange = (value: string) => {
    setAlleleState({
      alleleAssessment: {
        evaluation: {
          similar: {
            comment: value,
          },
        },
      },
    })
  }

  const [similarAlleles, setSimilarAlleles] = useState<SimilarAllele[] | 'loading'>(
    'loading',
  )
  const config = useSelector(selectConfig)
  const dispatch = useDispatch()

  // get data from api and write to store
  useEffect(() => {
    // reset data
    setSimilarAlleles('loading')
    // fetch new data
    API.SimilarAlleles.get(genePanelName, genePanelVersion, [selectedAlleleId]).then(
      (result) => {
        const similarAllelesApi: ApiTypes.SimilarAllelesResponse = result.data
        const promises = Object.values(similarAllelesApi)
          .flat()
          .map((apiAllele: ApiTypes.Allele) => dispatch(processAndStoreAllele(apiAllele)))
        // convert alleles (api -> store)
        const similarAllelesStore: SimilarAllele[] = Object.entries(
          similarAllelesApi,
        ).reduce(
          // here we only have one alleleId as input for API call -> it's fine to just flatten arrays
          (acc, [, apiAlleles]) => [
            ...acc,
            ...apiAlleles.map(
              (apiAllele: ApiTypes.Allele): SimilarAllele => ({
                alleleId: apiAllele.id,
                alleleAssessment: apiAllele.allele_assessment
                  ? StoreAlleleAssessment.mapFromApi(
                      apiAllele.allele_assessment as ApiTypes.AlleleAssessment,
                    )
                  : null,
                annotationId: apiAllele.annotation.annotation_id,
              }),
            ),
          ],
          [] as SimilarAllele[],
        )
        Promise.all(promises).then(() => setSimilarAlleles(similarAllelesStore))
      },
    )
  }, [genePanelName, genePanelVersion, selectedAlleleId])

  // get data from store
  const similarAlleleDataRows: SimilarAlleleData[] = useSelector((state: RootState) => {
    if (similarAlleles === 'loading') {
      return []
    }
    return similarAlleles.map((similarAllele) => {
      const alleleDisplay = selectAlleleDisplay(state, {
        alleleId: similarAllele.alleleId,
        annotationId: similarAllele.annotationId,
        genePanelName,
        genePanelVersion,
      })
      const saData: SimilarAlleleData = {
        alleleId: similarAllele.alleleId,
        classification: similarAllele.alleleAssessment?.classification || undefined,
        dateCreated: similarAllele.alleleAssessment
          ? new Date(similarAllele.alleleAssessment.dateCreated)
          : undefined,
        formattedDisplay: alleleDisplay
          .map((a) => `${a.geneSymbol} ${a.hgvscShort} ${a.hgvsp ? `(${a.hgvsp})` : ''}`)
          .join('; '),
        linkWorkflow: `/variants/${similarAllele.alleleId}`,
      }
      return saData
    })
  })

  const cardChildren = (() => {
    if (similarAlleles === 'loading') {
      return <SpinnerSmall />
    }
    if (!similarAlleles.length) {
      return undefined
    }
    return (
      <div className="flex-col text-sm">
        <div className="mb-2 text-lg">
          Variants +/- {config.similar_alleles.max_genomic_distance} bp (first{' '}
          {config.similar_alleles.max_variants}):
        </div>
        <table className="mt-1">
          <tbody>
            {similarAlleleDataRows.map((similarAlleleDataRow) => (
              <tr key={similarAlleleDataRow.alleleId}>
                <td className="pr-2">
                  <a
                    className="border-b border-dotted hover:text-ellablue-darker"
                    href={similarAlleleDataRow.linkWorkflow}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {similarAlleleDataRow.formattedDisplay}
                  </a>
                </td>
                <td className="px-2 uppercase">
                  {similarAlleleDataRow.classification
                    ? `Class ${similarAlleleDataRow.classification}`
                    : 'New'}
                </td>
                <td className="px-2 uppercase">
                  {similarAlleleDataRow.dateCreated
                    ? format(new Date(similarAlleleDataRow.dateCreated), 'yyyy-MM-dd')
                    : ''}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  })()

  return (
    <SectionBox title="Region" color="ellapurple" initCollapsed={false}>
      <Editor
        key={`region-comment-${selectedAlleleId}`}
        id="region-comment"
        readOnly={readOnly}
        initialValue={comment}
        onBlur={onChange}
        placeholder="Region - comments"
      />
      <div className="flex">
        <Card label="VarDB SNV">{cardChildren}</Card>
      </div>
    </SectionBox>
  )
}
