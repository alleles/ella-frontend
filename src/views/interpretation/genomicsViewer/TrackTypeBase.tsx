export abstract class TrackTypeBase {
  abstract matchesType(type: String | null): boolean
  abstract getIgvLoadedTrackIds(browser: any): String[]
  abstract removeTrack(browser: any, trackId: String): void
  abstract loadTrack(browser: any, igvcfg: any): Promise<void>
}
