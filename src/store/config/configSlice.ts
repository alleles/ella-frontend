import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { createSelector } from 'reselect'

import API from 'app/API'

import type { RootState } from 'store/store'

import { AcmgType } from 'types/store/Acmg'
import { Config } from 'types/store/Config'

const initialState: Config = {} as Config

export const fetchConfig = createAsyncThunk<Config>('config/fetchConfig', async () =>
  API.Config.get().then((response) => response.data),
)

const configSlice = createSlice({
  name: 'config',
  initialState,
  reducers: {
    setConfig: (state, action) => {
      Object.assign(state, action.payload)
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchConfig.fulfilled, (state, action) => action.payload)
  },
})

export const { setConfig } = configSlice.actions

export const selectConfig = (state: RootState) => state.config
export const selectPriorityText = (state: RootState) =>
  state.config.analysis.priority.display
export const selectRichTextTemplates = (richTextFieldKey: string) => (state: RootState) =>
  state.config.user.user_config.comment_templates?.filter((template) =>
    template.comment_fields.includes(richTextFieldKey),
  ) ?? []

export const selectPasswordPolicy = (state: RootState) => state.config.user.auth

export const selectAcmgCodesByCategory = createSelector(
  (state: RootState) => state.config.acmg.codes,
  (state: RootState) => Object.keys(state.config.acmg.explanation),
  (codeCategories, codes) => {
    const codesByCategory: Record<AcmgType, string[]> = {
      pathogenic: [],
      benign: [],
      other: [],
    }

    codes.forEach((code) => {
      const codeCategory = Object.entries(codeCategories)
        .find((categoryAndPrefixes) =>
          categoryAndPrefixes[1].find((prefix) => code.startsWith(prefix)),
        )
        ?.at(0)
      if (!codeCategory) {
        return
      }

      codesByCategory[codeCategory].push(code)
    })

    return codesByCategory
  },
)

export const selectConfigClassificationOptions = (state: RootState) =>
  state.config.classification.options

export const selectAppConfig = (state: RootState) => state.config.app

export const selectConsequences = (state: RootState): string[] =>
  state.config.transcripts.consequences

export default configSlice.reducer
