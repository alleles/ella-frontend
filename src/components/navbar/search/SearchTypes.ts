import * as ApiTypes from 'types/api/Pydantic'
import * as AlleleOverview from 'types/store/AlleleOverview'
import * as AnalysisOverview from 'types/store/AnalysisOverview'

export interface SearchResults {
  alleles: AlleleOverview.AlleleOverview[]
  analyses: AnalysisOverview.OverviewAnalysis[]
}

export interface SearchOptionUser {
  username: string
  first_name: string
  last_name: string
}
export interface SearchOptionGene {
  symbol: string
  hgnc_id: number
}

export function mapFromApi(searchResultFromApi: ApiTypes.SearchResponse): SearchResults {
  return {
    alleles: searchResultFromApi.alleles.map((entry) => {
      const { allele, interpretations } = entry

      // Search response needs some massaging to match AlleleOverview type
      return AlleleOverview.mapFromApi({
        allele,
        date_created: 'N/A',
        genepanel: interpretations.length
          ? {
              name: interpretations.at(-1)?.genepanel_name || '',
              version: interpretations.at(-1)?.genepanel_version || '',
            }
          : ({} as ApiTypes.GenepanelBasic),
        interpretations,
        priority: 0,
      })
    }),
    analyses: searchResultFromApi.analyses.map(AnalysisOverview.mapFromApi),
  }
}
