import React from 'react'

import Palette from 'views/styleguide//Palette'
import FontSizes from 'views/styleguide/FontSizes'

function StyleGuide() {
  return (
    <div className="min-h-full flex flex-col justify-center py-12 px-8">
      <div>
        <h2 className="text-3xl mb-10">Palette</h2>
        <Palette />
      </div>
      <div className="my-10">
        <h2 className="text-3xl mb-10">Font-size</h2>
        <FontSizes />
      </div>
    </div>
  )
}
export default StyleGuide
