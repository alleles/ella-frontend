import { format, formatDistance } from 'date-fns'
import React from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import Button from 'components/elements/Button'

import { selectOwnOngoingAlleles } from 'store/overview/allelesOverviewSlice'
import { selectOwnOngoingAnalyses } from 'store/overview/analysesOverviewSlice'
import { selectCurrentUser, selectUsersInOwnGroup } from 'store/users/usersSlice'

function LabelItemPair({
  label,
  item,
}: {
  label: string
  item: string | number | string[]
}) {
  return (
    <div className="col-span-1 mt-3">
      <dt className="text-sm font-medium text-ellagray-700">{label}</dt>
      {Array.isArray(item) ? (
        item.map((i) => (
          <dd key={i} className="mt-1 ml-2 text-sm text-ellagray-900">
            {i.toString()}
          </dd>
        ))
      ) : (
        <dd className="mt-1 text-sm text-ellagray-900">{item.toString()}</dd>
      )}
    </div>
  )
}

export default function Profile() {
  const currentUser = useSelector(selectCurrentUser)
  const ownAnalyses = useSelector(selectOwnOngoingAnalyses)
  const ownAlleles = useSelector(selectOwnOngoingAlleles)
  const usersInGroup = useSelector(selectUsersInOwnGroup)

  if (!currentUser) {
    throw new Error('No logged in user found.')
  }

  const { fullName, email, passwordExpiry, username, group } = currentUser
  const navigate = useNavigate()

  return (
    <div className="overflow-hidden rounded-lg bg-white shadow">
      <div className="flex justify-between px-6 py-5 ">
        <h3 className="text-lg font-medium text-ellagray-900">{currentUser.fullName}</h3>
        <div className="ml-4 ">
          <Button
            onClick={() => {
              navigate('/change_password')
            }}
          >
            Change password
          </Button>
        </div>
      </div>
      <div className="border-t border-ellablue-darker px-6 py-5">
        <dl className="grid grid-cols-3 gap-x-4 gap-y-8">
          <div className="col-span-1">
            <h4 className="text-base font-medium leading-6 text-ellagray-900">
              Personal details
            </h4>
            <LabelItemPair label="Full name" item={fullName} />
            <LabelItemPair label="Email" item={email} />
            <LabelItemPair label="Username" item={username} />
            <LabelItemPair
              label="Password expires on"
              item={`${format(new Date(passwordExpiry), 'yyyy-MM-dd')} (${formatDistance(
                new Date(passwordExpiry),
                new Date(),
                {
                  addSuffix: true,
                },
              )} from now)`}
            />
          </div>

          <div className="col-span-1">
            <h4 className="text-base font-medium leading-6 text-ellagray-900">
              User group
            </h4>
            <LabelItemPair label="Group" item={group?.name ?? ''} />
            <LabelItemPair
              label="Other users in the group"
              item={usersInGroup.map((u) => u.fullName)}
            />
          </div>

          <div className="col-span-1">
            <h4 className="text-base font-medium leading-6 text-ellagray-900">
              Genepanels and info
            </h4>
            <LabelItemPair label="Analyses worked on" item={ownAnalyses.length} />
            <LabelItemPair label="Variants worked on" item={ownAlleles.length} />
            <LabelItemPair
              label="Genepanels"
              item={group?.genePanels.map((p) => p.name) ?? []}
            />
          </div>
        </dl>
      </div>
    </div>
  )
}
