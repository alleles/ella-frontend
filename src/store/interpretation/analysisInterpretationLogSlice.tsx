import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { InterpretationLogItem, mapFromApi } from 'types/store/InterpretationLog'

const initialState: Record<number, InterpretationLogItem[]> = {}

const fetchAnalysisInterpretationLogForAnalysisId = createAsyncThunk<
  typeof initialState,
  number
>('analysisInterpretationLog/fetchAnalysisInterpretationLog', async (analysisId) => {
  const analysisInterpretationLog = await API.AnalysisInterpreationLogs.get(
    analysisId,
  ).then((result) => result.data)

  return { [analysisId]: analysisInterpretationLog.logs.map(mapFromApi) }
})

const analysisInterpretationLogSlice = createSlice({
  name: 'analysisInterpretationLog',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      fetchAnalysisInterpretationLogForAnalysisId.fulfilled,
      (state, action) => Object.assign(state, action.payload),
    )
  },
})

const selectAnalysisInterpretationLogByAnalysisId = (
  state: RootState,
  { analysisId }: { analysisId: number },
) => state.analysisInterpretationLog[analysisId]

export {
  fetchAnalysisInterpretationLogForAnalysisId,
  selectAnalysisInterpretationLogByAnalysisId,
}

export default analysisInterpretationLogSlice.reducer
