SHELL := /bin/bash
_IGNORE_VARS :=

override THIS_FILE := $(lastword $(MAKEFILE_LIST))
override THIS_DIR := $(shell dirname $$(realpath "$(THIS_FILE)"))

PKG_NAME := $(notdir ${THIS_DIR})
BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
RELEASE_TAG ?= $(shell git tag -l --points-at HEAD)
PREVIEW ?= false

DOCKER_GATEWAY = $(shell docker inspect bridge --format '{{ (index .IPAM.Config 0).Gateway }}')
DOCKER_SUBNET = $(shell docker inspect bridge --format '{{ (index .IPAM.Config 0).Subnet }}')
ifeq ($(DOCKER_GATEWAY),)
DOCKER_INTERNAL = $(patsubst %.0,%.1,$(firstword $(subst /, ,${DOCKER_SUBNET})))
else
DOCKER_INTERNAL = $(DOCKER_GATEWAY)
endif
COMPOSE_UP = time docker-compose up -d --quiet-pull
TTY_MODE = $(if $(shell test -t 0 || echo no TTY),-T)

ifeq ($(DOCKER_INTERNAL),)
$(error Unable to guess docker gateway IP, specify DOCKER_INTERNAL manually)
endif

ifneq (${RELEASE_TAG},)
IMAGE_TAG = ${RELEASE_TAG}
else
IMAGE_TAG = ${BRANCH}
endif

.DEFAULT_GOAL := help
all: help

# docker/buildkit/compose settings
# ref: https://testdriven.io/blog/faster-ci-builds-with-docker-cache/
IMAGE_NAME ?= registry.gitlab.com/alleles/ella-frontend
IMAGE_SLUG ?= ${IMAGE_NAME}:${IMAGE_TAG}
BACKEND_TAG ?= dev
BACKEND_IMAGE ?= registry.gitlab.com/alleles/ella:${BACKEND_TAG}
DOCKER_BUILDKIT ?= 1
COMPOSE_DOCKER_CLI_BUILD ?= 1
export BRANCH DOCKER_INTERNAL DOCKER_BUILDKIT COMPOSE_DOCKER_CLI_BUILD IMAGE_NAME BACKEND_IMAGE PKG_NAME

### directory set up
VAR_DIRS = var/gcm var/log var/mail var/screenshots var/sms
MAKE_DIRS = ${VAR_DIRS} .vscode-server

##---------------------------------------------
## General
##---------------------------------------------
.PHONY: env docker-build docker-push pull-dev up down update-pydantic-types

# automagically create directories with a+rwX
${MAKE_DIRS}:
	umask 000 && mkdir -p $@

# We need to update .env-file in place, since specifying .env-file is not supported:
# https://github.com/microsoft/vscode-remote-release/issues/6298
env: ##
	@envsubst < .env-template > .env

docker-build: env ## builds all docker-compose images
	docker pull ${IMAGE_SLUG} || true
	docker-compose build

docker-push: ## pushes newly created docker image
	docker push ${IMAGE_SLUG}

pull-dev: ## ensures that the latest backend image is pulled
	docker-compose pull -q backend

up: init-devcontainer ## starts docker-compose stack
	docker-compose up

down: ## stops docker-compose stack
	docker-compose down

update-pydantic-types:
	docker-compose exec ${TTY_MODE} backend /ella/scripts/pydantic2ts.sh Pydantic
	docker-compose cp backend:/ella/Pydantic.ts src/types/api/Pydantic.ts

##---------------------------------------------
## Dev
##---------------------------------------------

# run containers, excluding selenium for less mem usage
up-low-mem: init-devcontainer
	docker-compose up frontend backend nginx

# run from initializeCommand in devcontainer.json
init-devcontainer: env ${MAKE_DIRS} update-testdata

update-testdata:
	git submodule update --init --remote --recursive

TESTDATA_DIR ?= ${THIS_DIR}/ella-testdata
# only needs to be run if old clone or cloned without --recursive
setup-gitmodules:
	$(if $(shell [[ -d ${TESTDATA_DIR}/.git ]] && echo yes),git submodule absorbgitdirs)
	git submodule sync
	git submodule update --init --recursive

# When initially cloned as shallow repo, the submodule fetch strategy is restricted to its current
# branch. This persists even if you `git fetch --unshallow` later.
#     e.g., '+refs/heads/main:refs/remotes/origin/main' vs. '+refs/heads/*:refs/remotes/origin/*'
# This causes any later submodule update commands to fail, even if you explicitly fetch that branch.
# So we need to check that the value is the correct one and replace it if not.
.SILENT: set-testdata-branch
set-testdata-branch:
	$(call check_defined, REF, set REF to the branch you want to use)
	$(if \
		$(shell git -C ./ella-testdata config --get remote.origin.fetch | grep -q '/\*$$' || echo yes), \
		git -C ella-testdata config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*' \
	)
	git submodule set-branch -b ${REF} -- ella-testdata || echo "testdata branch already set to ${REF}"
	git submodule update --remote
	git diff --submodule=log
	echo "testdata branch set to ${REF}"

##---------------------------------------------
## CI
##---------------------------------------------
.PHONY: ci-testdata ci-up test-js test-bdd

init-ci: env ${MAKE_DIRS} $(if ${REF},set-testdata-branch)
	cp -v config/example/*.pem config/

## Start service one by one to avoid memory spike
ci-up: init-ci
	${COMPOSE_UP} frontend
	${COMPOSE_UP} backend
	${COMPOSE_UP} nginx
	${COMPOSE_UP} selenium-hub
	${COMPOSE_UP} chrome

test-js: ## run javascript tests
	docker-compose exec ${TTY_MODE} -e CI=true frontend npm run test
	docker-compose exec ${TTY_MODE} frontend /app/node_modules/.bin/eslint . --max-warnings=0
	docker-compose exec ${TTY_MODE} frontend /app/node_modules/.bin/prettier --check "**/*.+(js|jsx|ts|tsx|json|yml|yaml|css|md|vue)"
	docker-compose exec ${TTY_MODE} frontend /app/node_modules/.bin/tsc --noEmit

test-bdd: ## runs BDD/Behaving tests. Optionally use BDD_TAGS to specify which tests to run
	tests/run-bdd-tests.sh ${BDD_OPTS} $(if ${BDD_TAGS},--tags '${BDD_TAGS}' -k)

##---------------------------------------------
## Help / Debugging
##  Other vars: VAR_ORIGIN, FILTER_VARS
##---------------------------------------------
.PHONY: help vars local-vars

# only use ASCII codes if running in terminal (e.g., not when piped)
ifneq (${MAKE_TERMOUT},)
_CYAN = \033[36m
_RESET = \033[0m
endif

# Using the automatic `make help` generation:
# Lines matching /^## / are considered section headers
# Use a ## at the end of a rule to have it printed out
# e.g., `some_rule: ## some_rule help text` at the end of a rule to have it included in help output
help: ## prints this help message
	@grep -E -e '^[a-zA-Z_-]+:.*?## .*$$' -e '^##[ -]' ${MAKEFILE_LIST} \
		| awk 'BEGIN {prev = ""; FS = ":.*?## "}; {if (match($$1, /^#/) && match(prev, /^#/) == 0) printf "\n"; printf "${_CYAN}%-30s${_RESET} %s\n", $$1, $$2; prev = $$1}'
	@echo
	@echo "Additional comments available in this Makefile\n"

NULL_STRING :=
BLANKSPACE = ${NULL_STRING} # this is how we get a single space
_IGNORE_VARS += NULL_STRING BLANKSPACE
vars: _list_vars ## prints out variables available in the Makefile and the origin of their value
	@true

# actually prints out the desired variables, should not be called directly
# uses environment and environment_override by default, always includes file and command_line
# ref:
#        origin:  https://www.gnu.org/software/make/manual/html_node/Origin-Function.html#Origin-Function
#    .VARIABLES:  https://www.gnu.org/software/make/manual/html_node/Special-Variables.html#Special-Variables
# overall magic:  https://stackoverflow.com/a/59097246/5791702
FILTER_VARS ?=
_list_vars:
	$(eval filtered_vars = $(sort $(filter-out $(sort $(strip ${FILTER_VARS} ${_IGNORE_VARS})),${.VARIABLES})))
	$(eval VAR_ORIGIN ?= environment environment_override)
	$(eval override VAR_ORIGIN += file command_line)
	$(info VAR_ORIGIN = ${VAR_ORIGIN})
	$(foreach v, ${filtered_vars}, $(if $(filter $(sort ${VAR_ORIGIN}),$(subst ${BLANKSPACE},_,$(origin ${v}))), $(info ${v} ($(origin ${v})) = ${${v}})))

_disable_origins:
	$(eval VAR_ORIGIN = )

ENV = $(shell which env)
local-vars: _disable_origins _list_vars ## print out vars set by command line and in the Makefile
	@true
