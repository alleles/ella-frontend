import { selectDefaultFilterConfigId } from 'store/selectors/interpretationSelectors'
import { RootState, store } from 'store/store'

import { Analysis } from 'types/store/Analysis'
import { AnalysisInterpretation } from 'types/store/AnalysisInterpretation'

describe('select default filter config id', () => {
  const stateFactory = (
    interpretationFilterConfigId: number | null,
    analysisFilterConfigIds: number[],
  ): RootState => ({
    ...store.getState(),
    analyses: {
      1: {
        filterConfigIds: analysisFilterConfigIds,
      } as Analysis,
    },
    analysisInterpretations: {
      2: {
        state: {
          filterConfigId: interpretationFilterConfigId,
        },
      } as AnalysisInterpretation,
    },
  })

  it('selects filter config from interpretation when available', () => {
    expect(
      selectDefaultFilterConfigId(stateFactory(1, [2, 3, 4]), {
        analysisId: 1,
        analysisInterpretationId: 2,
      }),
    ).toEqual(1)

    expect(
      selectDefaultFilterConfigId(stateFactory(2, []), {
        analysisId: 1,
        analysisInterpretationId: 2,
      }),
    ).toEqual(2)
  })

  it('selects filter config from analysis when not available in interpretation', () => {
    expect(
      selectDefaultFilterConfigId(stateFactory(null, [2, 3, 4]), {
        analysisId: 1,
        analysisInterpretationId: 2,
      }),
    ).toEqual(2)

    expect(
      selectDefaultFilterConfigId(stateFactory(null, [12]), {
        analysisId: 1,
        analysisInterpretationId: 2,
      }),
    ).toEqual(12)
  })
})
