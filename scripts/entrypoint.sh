#!/bin/bash

if [[ "${PREVIEW}" != "true" ]]; then
    echo "Running vite dev server"
    npm start
else
    echo "Running production preview"
    npm run build && npm run preview
fi