import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import analysisReducer, {
  fetchAnalysisByAnalysisId,
  processAndStoreAnalysis,
  selectAnalysisById,
} from 'store/interpretation/analysisSlice'
import { RootState, store } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Analysis } from 'types/store/Analysis'

const mock = new MockAdapter(axios)

function buildAnalysis(override: Partial<Analysis>): Analysis {
  return {
    name: '',
    dateRequested: null,
    dateDeposited: '1970-01-01',
    filterConfigIds: [],
    interpretationIds: [],
    genePanelName: 'Dabla',
    genePanelVersion: 'v01',
    sampleIds: [],
    warnings: null,
    report: null,
    ...override,
  }
}

const apiAnalysis: ApiTypes.AnalysisResponse = {
  id: 1,
  name: 'foobar',
  date_requested: '2022-01-27',
  date_deposited: '2022-01-28',
  interpretations: [{ id: 20 }, { id: 30 }] as ApiTypes.AnalysisInterpretationOverview[],
  genepanel: {
    name: 'Foo',
    version: 'v01',
    official: true,
  },
  samples: [{ id: 10 }] as ApiTypes.Sample[],
  report: 'report',
  warnings: 'warnings',
  attachments: [],
}

const apiOverviewAnalysis: ApiTypes.OverviewAnalysis = {
  ...apiAnalysis,
  priority: 1,
}

const mappedAnalysis: Analysis = {
  name: 'foobar',
  dateRequested: '2022-01-27',
  dateDeposited: '2022-01-28',
  filterConfigIds: [1, 3, 5],
  genePanelName: 'Foo',
  genePanelVersion: 'v01',
  interpretationIds: [20, 30],
  sampleIds: [10],
  warnings: 'warnings',
  report: 'report',
}

describe('analysis reducer', () => {
  mock.onGet(API.AnalysisFilterConfigs.path(1)).reply(200, [
    {
      id: 1,
    },
    {
      id: 3,
    },
    {
      id: 5,
    },
  ])

  it('should handle initial state', () => {
    expect(analysisReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('handles fetching analyses from API', async () => {
    mock.onGet(API.Analysis.path(1)).reply(200, apiAnalysis)

    const dispatch = jest.fn()
    await fetchAnalysisByAnalysisId(1)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'analysis/fetchAnalysisByAnalysisId/fulfilled',
        payload: {
          1: mappedAnalysis,
        },
      }),
    )
  })

  it('handles processing API analyses', async () => {
    const dispatch = jest.fn()
    await processAndStoreAnalysis(apiOverviewAnalysis)(dispatch, () => undefined, {})
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'analysis/processAndStoreAnalysis/fulfilled',
        payload: {
          1: mappedAnalysis,
        },
      }),
    )
  })

  it('should handle getting analysis by id', async () => {
    const state: RootState = {
      ...store.getState(),
      analyses: {
        1: buildAnalysis({ name: 'foo' }),
        2: buildAnalysis({ name: 'bar' }),
      },
    }

    expect(selectAnalysisById(state, { analysisId: 1 })).toEqual(
      buildAnalysis({ name: 'foo' }),
    )
    expect(selectAnalysisById(state, { analysisId: 2 })).toEqual(
      buildAnalysis({ name: 'bar' }),
    )
    expect(selectAnalysisById(state, { analysisId: 3 })).toEqual(undefined)
  })
})
