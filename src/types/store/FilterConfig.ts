import * as ApiTypes from 'types/api/Pydantic'

export type FilterNames =
  | 'frequency'
  | 'consequence'
  | 'region'
  | 'classification'
  | 'external'
  | 'ppy'
  | 'quality'
  | 'segregation'
  | 'inheritancemodel'
  | 'size'
  | 'callertype'
export interface FilterConfig {
  name: string
  filterConfig: Filters
  active: boolean
}
export interface Filters {
  filters: FilterWithExceptions[]
}
export interface FilterWithExceptions {
  name: FilterNames
  exceptions?: FilterWithoutExceptions[]
  config: {
    groups?: {
      [k: string]: {
        [provider: string]: string[]
      }
    }
    num_thresholds?: {
      [provider: string]: {
        [population: string]: number
      }
    }
    [k: string]: any
  }
}
export interface FilterWithoutExceptions {
  name: FilterNames
  config: {
    [k: string]: any
  }
}

export function mapFromApi(filterConfigFromApi: ApiTypes.FilterConfig): FilterConfig {
  return {
    name: filterConfigFromApi.name,
    filterConfig: {
      filters: filterConfigFromApi.filterconfig.filters.map((fc) => ({
        name: fc.name,
        exceptions: (fc.exceptions || []).map((filterException) => ({
          name: filterException.name,
          config: filterException.config,
        })),
        config: fc.config,
      })),
    },
    active: filterConfigFromApi.active,
  }
}
