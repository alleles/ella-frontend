import React from 'react'

function FontSizes() {
  return (
    <div className="rounded-t-xl overflow-hidden p-6 bg-white rounded shadow">
      <dl>
        <div className="flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-3">xs</dt>
          <dd className="truncate text-xs">All work and no play makes Jack a dull boy</dd>
        </div>
        <div className="mt-6 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-4">sm</dt>
          <dd className="truncate text-sm">All work and no play makes Jack a dull boy</dd>
        </div>
        <div className="mt-6 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-5">base</dt>
          <dd className="truncate text-base">
            All work and no play makes Jack a dull boy
          </dd>
        </div>
        <div className="mt-6 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-6">lg</dt>
          <dd className="truncate text-lg">All work and no play makes Jack a dull boy</dd>
        </div>
        <div className="mt-6 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-6">xl</dt>
          <dd className="truncate text-xl">All work and no play makes Jack a dull boy</dd>
        </div>
        <div className="mt-8 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-6">2xl</dt>
          <dd className="truncate text-2xl">
            All work and no play makes Jack a dull boy
          </dd>
        </div>
        <div className="mt-10 flex items-start">
          <dt className="w-16 shrink-0 text-sm font-mono leading-6">3xl</dt>
          <dd className="truncate text-3xl">
            All work and no play makes Jack a dull boy
          </dd>
        </div>
      </dl>
    </div>
  )
}
export default FontSizes
