from behave import then
from behaving.web.steps.basic import _retry


@then('I should see that the position is "{value}"')
def igvsearch_value(context, value: str):
    wait_time = context.browser.wait_time or 0
    cbc = context.browser.find_by_css
    css_selector = "input.igv-search-input"

    def check():
        return cbc(css_selector).value == value

    assert _retry(
        check, wait_time
    ), f"Values do not match, expected {value}, got {cbc(css_selector).first.value}"


@then('I should see the track label "{value}"')
def tracklabel_value(context, value: str):
    context.execute_steps(
        f"""
        Then I should see an element with xpath "//*[@class='igv-track-label'][text()='{value}']"
        """
    )


@then('I should not see the track label "{value}"')
def tracklabel_novalue(context, value: str):
    context.execute_steps(
        f"""
        Then I should not see an element with xpath "//*[@class='igv-track-label'][text()='{value}']"
        """
    )
