import { ExclamationCircleIcon } from '@heroicons/react/24/solid'
import React, { ReactNode } from 'react'

import Button from 'components/elements/Button'
import Modal from 'components/elements/modals/Modal'

interface Props {
  onYes: () => void
  onNo: () => void
  children: ReactNode
}

export function YesNoModal({ onNo, onYes, children }: Props) {
  return (
    <Modal onClose={onNo}>
      <div className="flex justify-center pb-3">
        <ExclamationCircleIcon
          className="h-12 w-12 text-ellayellow-dark"
          aria-hidden="true"
        />
      </div>
      <div className="mb-4 max-w-sm">{children}</div>
      <div className="flex gap-x-2">
        <Button onClick={onYes}>Yes</Button>
        <Button onClick={onNo} color="cancel">
          No
        </Button>
      </div>
    </Modal>
  )
}
