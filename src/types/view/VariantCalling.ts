import * as ApiTypes from 'types/api/Pydantic'

export type VariantCalling = {
  variantQuality: number | null
  filterStatus: string
  multiAllelic: boolean | null
}

export function mapFromApi(sample: ApiTypes.Sample): VariantCalling {
  return {
    variantQuality: sample.genotype?.variant_quality || null,
    filterStatus: sample.genotype?.filter_status || '.',
    multiAllelic: sample.genotype?.multiallelic || null,
  }
}
