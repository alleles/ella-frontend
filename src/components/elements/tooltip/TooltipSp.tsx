import { XCircleIcon } from '@heroicons/react/24/solid'
import * as PopperJS from '@popperjs/core'
import React, { PropsWithChildren, ReactNode, useEffect, useState } from 'react'
import { usePopper } from 'react-popper'

import { generateArrowStyles } from 'components/elements/tooltip/generateArrowStyles'

export class TooltipController {
  tooltips: {
    [id: string]: {
      open: () => Promise<void>
      close: () => Promise<void>
    }
  } = {}

  register(id: string, open: () => Promise<void>, close: () => Promise<void>) {
    this.tooltips[id] = {
      open,
      close,
    }
  }

  async open(id: string) {
    this.tooltips[id].open()
    // close all others
    Promise.all(
      Object.entries(this.tooltips)
        .filter(([k]) => k !== id)
        .map(([, v]) => v.close()),
    )
  }

  unregister(k: string) {
    this.tooltips[k].close()
    delete this.tooltips[k]
  }
}

interface Props {
  children: ReactNode
  tooltipContent: ReactNode
  placement?: PopperJS.Placement
  onClose?: () => void
  controller: TooltipController
  id: string
  title?: string
}

function TooltipSp({
  children,
  onClose = () => {},
  placement = 'auto',
  controller,
  tooltipContent,
  id,
  title = undefined,
}: Props): JSX.Element {
  const [popperElement, setPopperElement] = useState<HTMLElement | null>(null)
  const [arrowElement, setArrowElement] = useState<HTMLElement | null>(null)
  const [showTooltip, setShowTooltip] = useState<boolean>(false)
  const [spanElement, setSpanElement] = useState<HTMLElement | null>(null)

  const open = async () => setShowTooltip(true)

  const close = () => {
    if (showTooltip === false) {
      return
    }
    setShowTooltip(false)
    onClose()
  }

  controller.register(
    id,
    async () => open(),
    async () => close(),
  )

  const { styles, attributes } = usePopper(spanElement, popperElement, {
    placement,
    modifiers: [
      { name: 'arrow', options: { element: arrowElement } },
      { name: 'offset', options: { offset: [10, 20] } },
    ],
  })

  const handleClickOutside = (event) => {
    if (!popperElement?.contains(event.target) && popperElement !== event.target) {
      onClose()
    }
  }

  useEffect(() => {
    if (popperElement) {
      setTimeout(() => document.body.addEventListener('click', handleClickOutside))

      return () => {
        document.body.removeEventListener('click', handleClickOutside)
      }
    }

    return () => {}
  }, [popperElement])

  // don't show anything if there is no tooltip
  if (tooltipContent === undefined && children) {
    // linter hack - we want to return children without creating a parent
    return <>{React.Children.toArray(children)}</>
  }

  return showTooltip ? (
    <>
      <div
        ref={setPopperElement}
        style={styles.popper}
        {...attributes.popper}
        className="z-10 max-w-2xl rounded-lg border-2 bg-white py-2 px-3 text-sm text-black shadow"
      >
        <div>
          <button
            className="absolute top-0 right-0 inline-block cursor-pointer p-2 text-black"
            onClick={() => close()}
          >
            <XCircleIcon className="h-5 w-5 text-ellagray-500" />
          </button>
        </div>
        <h2>{title || '\u00A0'}</h2>
        {tooltipContent}
        <div
          ref={setArrowElement}
          style={generateArrowStyles(
            attributes.popper ? attributes.popper['data-popper-placement'] : 'right',
            styles.arrow,
          )}
          className="arrow"
        />
      </div>
      <span ref={setSpanElement}>{children}</span>
    </>
  ) : (
    <span className="underline" onClick={() => controller.open(id)}>
      {children}
    </span>
  )
}

function P({ children }: PropsWithChildren<{}>) {
  return <div className="my-3">{children}</div>
}

function H1({ children }: PropsWithChildren<{}>) {
  return <div className="text-lg">{children}</div>
}

function H2({ children }: PropsWithChildren<{}>) {
  return <div className="text-base">{children}</div>
}

TooltipSp.H1 = H1
TooltipSp.H2 = H2
TooltipSp.P = P

export { TooltipSp }
