# ELLA's future web client using React.js

[![pipeline status](https://gitlab.com/alleles/ella-frontend/badges/main/pipeline.svg)](https://gitlab.com/alleles/ella-frontend/-/commits/main)

## Quick start

- `git clone https://gitlab.com/alleles/ella-frontend && cd ella-frontend`
- `make start`
- open [http://localhost:8090] in your webbrowser

## Requirements

- docker
- docker-compose
- make
- envsubst

## Development

Start this repo in devcontainer.
To reset the db do `docker exec ella.backend make dbreset`

## SSL setup (optional)

Setup mkcert on your machine to create a self-signed certificate.
Instructions can be found here for [linux](https://github.com/FiloSottile/mkcert#linux), [mac](https://github.com/FiloSottile/mkcert#macos) and [windows](https://github.com/FiloSottile/mkcert#windows).

After you install mkcert, make sure you run it with the `--install` flag.

```bash
mkcert -install
```

You can now create local certificates for nginx:

```bash
cd config
mkcert "*.local.allel.es"
```

And edit `/etc/hosts` to include the following lines:

```text
127.0.0.1 api.local.allel.es app.local.allel.es
```

Open [http://app.local.allel.es:8090] in your webbrowser

## Testing

To run unit tests open a terminal within the devcontainer and run:

```bash
npm test
```

For BDD end-to-end tests:

```bash
behave tests/

# for SSL use:
# SITE_URL="https://app.local.allel.es:8080" behave tests/
```

## Resources

### [Redux](https://redux.js.org)

- [Redux Fundamentals](https://redux.js.org/tutorials/fundamentals/part-1-overview)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Redux Toolkit API](https://redux-toolkit.js.org/api/configureStore)

### [tailwindcss](https://tailwindcss.com/)

#### Component libraries

- [tailwindUI](https://tailwindui.com/) (requested license)
- [headlessUI](https://headlessui.dev/)
