import React, { ReactNode, createContext, useState } from 'react'

import { TrackConfig } from 'views/interpretation/genomicsViewer/TrackConfig'

interface TrackSelection {
  trackConfigs: TrackConfig[]
  setTrackConfigs: (t: TrackConfig[]) => void
}

export const TrackSelectionContext = createContext<TrackSelection>({
  trackConfigs: [],
  setTrackConfigs: () => {},
})

export default function TrackSelectionContextProvider({
  children,
}: {
  children: ReactNode
}) {
  const [trackConfigs, setTrackConfigs] = useState<TrackConfig[]>([])

  const value = React.useMemo(
    (): TrackSelection => ({
      trackConfigs,
      setTrackConfigs: (t: TrackConfig[]) => {
        setTrackConfigs(t)
      },
    }),
    [trackConfigs],
  )

  return (
    <TrackSelectionContext.Provider value={value}>
      {children}
    </TrackSelectionContext.Provider>
  )
}
