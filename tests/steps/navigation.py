from typing_extensions import Literal

from behave import step, when, then
from behaving.personas.persona import persona_vars


# Go to Overview page
@step("I go to the overview")
@step("I go to analyses overview")
def step_analyses_overview(context):
    go_to_overview(context, "analyses")


@step("I go to variants overview")
def step_variants_overview(context):
    go_to_overview(context, "variants")


def go_to_overview(context, overview_type: Literal["analyses", "variants"]):
    context.browser.visit("{}/overview/{}".format(context.base_url, overview_type))
    context.execute_steps('Then I should see "$abbrev_name"')


# See on Overview page
@persona_vars
@then('I should see "{name}" in "{overview_section}"')
def view_in_section(context, name: str, overview_section: str):
    if overview_section == "Not ready":
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='not-ready']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section in ["Your variants", "Your analyses"]:
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='own-ongoing']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section == "Interpretation":
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='not-started']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section == "Pending review":
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='pending-review']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section == "Pending medical review":
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='pending-medical-review']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section in ["Others' variants", "Others' analyses"]:
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='others-ongoing']//*[contains(text(), '{name}')]"
            """
        )
    if overview_section == "Finalized":
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//section[@id='finalized']//*[contains(text(), '{name}')]"
            """
        )


# View analysis
@when("I view the analysis with id {analysis_id:d}")
def view_analysis_by_id(context, analysis_id: int):
    context.browser.visit("{}/analyses/{}".format(context.base_url, analysis_id))
    # Wait till we see something making sure the analysis is loaded.
    context.execute_steps(
        """
        Then I should see an element with id "busy-indicator" within 30 seconds
        Then I should not see an element with id "busy-indicator" within 30 seconds
        # Wait for allele table to be sorted
        Then I wait for 2 seconds
        """
    )


@persona_vars
@when('I view the analysis with name "{analysis_name}"')
def view_analysis_by_name(context, analysis_name: str):
    context.browser.visit("{}/".format(context.base_url))
    context.execute_steps(
        f"""
        When I press "{analysis_name}"
        Then I should see an element with id "busy-indicator" within 30 seconds
        Then I should not see an element with id "busy-indicator" within 30 seconds
        # Wait for allele table to be sorted
        Then I wait for 2 seconds
        """
    )


# View variant
@when("I view the allele with id {allele_id:d}")
def view_allele(context, allele_id: int):
    context.browser.visit("{}/variants/{}".format(context.base_url, allele_id))
    # Wait till we see something making sure the analysis is loaded.
    context.execute_steps(
        """
        Then I should see "CLASSIFICATION"
        """
    )


@persona_vars
@when('I view the variant with name "{variant_name}"')
def view_variant_by_name(context, variant_name: str):
    context.browser.visit("{}/overview/variants".format(context.base_url))
    context.execute_steps(
        f"""
        When I press "{variant_name}"
        Then I should see "CLASSIFICATION"
        """
    )


# Go to different pages in opened analysis
@when("I go to the INFO page")
def select__info_page(context):
    context.execute_steps(
        """
        When I press "INFO"
        Then I should see "ANALYSIS INFO"
        And I should see "PIPELINE REPORT"
        """
    )


@when("I go to the DETAILS page")
def select_details_page(context):
    context.execute_steps(
        """
        When I press "DETAILS"
        Then I should see "CLASSIFICATION"
        And I should see "REGION"
        """
    )


@when("I go to the VISUAL page")
def select_visual_page(context):
    context.execute_steps(
        """
        When I press "VISUAL"
        Then I should see "PRESET SELECTION"
        And I should see "TRACK SELECTION"
        And I should see "Cursor Guide"
        """
    )


# TODO: Change when #154, #155, #156 are resolved
@when("I go to the REPORT page")
def select_report_page(context):
    context.execute_steps(
        """
        When I press "REPORT"
        Then I should see "Report"
        And I should see "Allele table placeholder"
        """
    )


# Switch between allele table tabs Unclassified, Classified, Not Relevant, Technical
@when('I select the "{tab_name}" tab')
def select_classified_tab(context, tab_name: str):
    context.execute_steps(
        f"""
        When I press the element with xpath "//footer//span[text() = '{tab_name}']"
        """
    )


# Select variants in Allele table
@when("I select the allele row {row_number:d} from the allele table")
def select_allele_row(context, row_number: int):
    try:
        rows = context.browser.find_by_xpath("//table[@id='allele-table']/tbody/tr")
        rows[row_number].find_by_tag("td").first.click()
    except Exception:
        assert False, f"Could not select allele row {row_number}"


# Interpretation worklog step to handle changes in headlessui switch component
@when("I check interpretation-log-messages-only")
def toggle_interpretation_log_messages(context):
    try:
        context.browser.find_by_xpath(
            "//input[@name='interpretation-log-messages-only']/following-sibling::button"
        ).click()
    except Exception:
        assert False, "Could not click worklog messages button"


# Collapse/expand panels
@when("I collapse the right panel")
def collapse_right_panel(context):
    context.execute_steps(
        """
        When I press the element with id "collapse-right-panel"
        Then I should not see an element with id "collapse-right-panel"
        """
    )


@when("I expand the right panel")
def expand_right_panel(context):
    context.execute_steps(
        """
        When I press the element with id "expand-right-panel"
        Then I should not see an element with id "expand-right-panel"
        """
    )


@when("I collapse the bottom panel")
def collapse_bottom_panel(context):
    context.execute_steps(
        """
        When I press the element with id "collapse-bottom-panel"
        """
    )


@when("I expand the bottom panel")
def expand_bottom_panel(context):
    context.execute_steps(
        """
        When I press the element with id "expand-bottom-panel"
        """
    )
