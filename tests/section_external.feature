Feature: View and edit External section

  Background:
    Given state "test-bdd"

  Scenario: Check External section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "EXTERNAL"
    And I should see "CLINVAR"
    And I should see "HGMD PRO"

    # Can edit comment field
    When I start the interpretation
    And I type "Test External comment" into the editor with xpath "//div[@id='external-comment-editor-content']"
    Then I should see "Test External comment"

    # Check annotation for 2 variants
    # BRCA2 c.10G>T
    When I select the allele row 0 from the allele table
    Then I should see "2020-01-01"
    And I should see "CM082514"
    # BRCA2 c.51_52del
    When I select the allele row 1 from the allele table
    Then I should see "2020-01-15"
    And I should see "CD961847"
