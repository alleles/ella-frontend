import React from 'react'

interface Props {
  error: string
}

export default function Error({ error }: Props) {
  if (!error) return null
  return <div className="p-1 text-center text-sm text-ellared-dark">{error}</div>
}
