import * as ApiTypes from 'types/api/Pydantic'

export interface Genotype {
  type: 'Homozygous' | 'Heterozygous' | 'Reference' | 'No coverage'
  multiallelic: boolean
  genotypeQuality: number | null
  sequencingDepth: number | null
  needsVerification: boolean | null
  filterStatus: string | null
  alleleRatio: number | null
  pDenovo: number | null
  variantQuality: number | null
  formatted: string | null
  id: number
  alleleDepth: {
    [k: string]: any
  } | null
  copyNumber: number | null
  mother: Genotype | null
  father: Genotype | null
  siblings: Genotype[]
}

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined
}

export function mapFromApi(sample: ApiTypes.Sample): Genotype | null {
  if (!sample.genotype) return null
  const apiGenotype = sample.genotype
  const { mother, father, siblings } = sample
  const siblingGenotypes: Genotype[] = siblings
    ? siblings.map((sibling) => mapFromApi(sibling)).filter(notEmpty)
    : ([] as Genotype[])

  return {
    id: apiGenotype.id,
    type: apiGenotype.type,
    multiallelic: apiGenotype.multiallelic,
    genotypeQuality: apiGenotype.genotype_quality || null,
    sequencingDepth: apiGenotype.sequencing_depth || null,
    needsVerification: apiGenotype.needs_verification || null,
    filterStatus: apiGenotype.filter_status || null,
    alleleRatio: apiGenotype.allele_ratio || null,
    pDenovo: apiGenotype.p_denovo || null,
    formatted: apiGenotype.formatted || null,
    variantQuality: apiGenotype.variant_quality || null,
    alleleDepth: apiGenotype.allele_depth || null,
    copyNumber: apiGenotype.copy_number || null,
    mother: mother ? mapFromApi(mother) : null,
    father: father ? mapFromApi(father) : null,
    siblings: siblingGenotypes,
  }
}
