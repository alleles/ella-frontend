# BDD Testing

## Documentation

- Behaving - <https://github.com/ggozad/behaving> - Framework based on Behave and Splinter with pre-defined libraries and steps for common tasks
- Behave - <https://behave.readthedocs.io/en/stable/> - The framework behaving is based on. When writing custom steps, this will be the best reference

## Troubleshooting

### Local testing

If you are running the tests locally, it is recommended to disable React's strict mode.
You can do so by adding the file `.env.development` containing the following line:

```env
VITE_DISABLE_STRICT_MODE=true
```

in your project root. You will need to restart the container for changes to take effect.

### General

When having problems with the tests locally, it can be really useful to see what is happening as
the tests are still running. The selenium-hub image in the compose stack lets you do this. Visit
<http://localhost:4444> and you can see any active sessions for the different browser types. For
specific details, check out the [SeleniumHQ/docker-selenium documentation](https://github.com/SeleniumHQ/docker-selenium).

### In CI

It often happens that something passes when run locally, but not in CI. This can be due to
uncommitted local changes or files, but there can also be programmatic causes from the different
environments. Below are some problems / solutions we've encountered. If you find any new ones,
please update this doc.

#### Look in the CI job artifacts

When a CI job fails, a bunch of potentially useful data is attached to the job as an artifact. The
contents of `var/screenshots` is particularly useful, as they are taken when a test fails.
Container logs are also a good source of errors that may not be in the bdd job log itself.

#### Check for uncommitted local data

- `git status --ignored`
  - Look for any files that have not been added to the repo or have uncommitted changes
- `rm -rf ella-testdata/dumps`
  - Testdata can be loaded from locally stored dumps as well as remote. If a local dump was created
    with the same name as one of the web dumps, it will be loaded instead. Clearing this directory
    will ensure that does not happen.

#### Running locally from inside / outside the devcontainer

For general testing during development, it is simplest to open a console from inside VS Code and
run `behave tests/`. This is the primary baseline for working tests.

You can also run `make test-bdd` from the repo root outside the container. This can sometimes
reproduce errors seen in CI... somehow.

Having a clean repo directory on focus that you use just for running tests can also help catch any
subtle environment differences between your local and CI.

### Specific errors / solutions

#### Login screen when visiting a URL even though a persona is logged in

Check that your `When I visit "$URL"` line is using a relative and not an absolute URL. This
ensures the same domain is used when viewing pages, which login credentials are tied to.

_e.g.,_

```python
# Builds off of configured site URL
When I visit "overview/variants"

# May not work everywhere
When I visit "nginx:8090/overview/variants"
# see also:
#   app.local.allel.es:8090
#   $DOCKER_GATEWAY:8090
#   etc.
```

#### Flaky timing

Resources of the VM running the CI tests are likely different than one's local dev machine, so
timing for certain actions should be a bit more generous. You'll often want to use the
`within X seconds` suffix with, _e.g._, `I should see ...` steps that are executed after a click
or other interaction. It is important to still keep these within reasonble limits though.
`within 5 seconds` fine, `within 60 seconds` not so much.
