import React, { HTMLProps } from 'react'

interface TableProps extends HTMLProps<any> {
  children
}

export default function Table({ children, ...props }: TableProps) {
  return (
    <table
      className={`max w-full overflow-hidden bg-white ${props.className}`}
      {...props}
    >
      <tbody>{children}</tbody>
    </table>
  )
}
