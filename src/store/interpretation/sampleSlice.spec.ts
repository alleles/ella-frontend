import sampleReducer, {
  processAndStoreSample,
  selectSampleById,
} from 'store/interpretation/sampleSlice'
import { RootState, store } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Sample } from 'types/store/Sample'

export function buildSample(override: Partial<Sample>): Sample {
  return {
    id: 1,
    sex: null,
    identifier: '',
    sampleType: 'HTS',
    affected: true,
    proband: true,
    familyId: null,
    fatherId: null,
    motherId: null,
    siblingId: null,
    ...override,
  }
}

describe('sample reducer', () => {
  it('should handle initial state', () => {
    expect(sampleReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle processing samples', async () => {
    const inputSample: ApiTypes.Sample = {
      id: 1,
      identifier: 'foo',
      sample_type: 'Sanger',
      date_deposited: '1970-01-01',
      affected: false,
      proband: false,
    }

    const dispatch = jest.fn()
    await processAndStoreSample(inputSample)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        // Cleans out unwanted
        payload: {
          '1': {
            ...buildSample({
              identifier: 'foo',
              sampleType: 'Sanger',
              affected: false,
              proband: false,
            }),
          },
        },
      }),
    )
  })

  it('should handle getting sample by id', async () => {
    const state: RootState = {
      ...store.getState(),
      samples: {
        1: buildSample({ identifier: 'foo' }),
        2: buildSample({ identifier: 'bar' }),
      },
    }

    expect(selectSampleById(state, { sampleId: 1 })).toEqual(
      buildSample({ identifier: 'foo' }),
    )
    expect(selectSampleById(state, { sampleId: 2 })).toEqual(
      buildSample({ identifier: 'bar' }),
    )
    expect(selectSampleById(state, { sampleId: 3 })).toEqual(undefined)
  })
})
