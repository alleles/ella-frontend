import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import SectionBox from 'components/elements/SectionBox'

import { selectRichTextTemplates } from 'store/config/configSlice'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import ConfigurableAnnotation from 'views/interpretation/components/annotation/configurable/ConfigurableAnnotation'

export default function Frequency() {
  const { alleleState, setAlleleState, readOnly, selectedAlleleId } =
    useInterpretationStateContext()

  const templates = useSelector(selectRichTextTemplates('classificationFrequency'))

  const [comment, setComment] = useState(
    alleleState?.alleleAssessment.evaluation.frequency.comment,
  )

  const onChange = (value: string) => {
    setAlleleState({
      alleleAssessment: {
        evaluation: {
          frequency: {
            comment: value,
          },
        },
      },
    })
  }

  useEffect(() => {
    setComment(alleleState?.alleleAssessment.evaluation.frequency.comment)
  }, [selectedAlleleId])

  return (
    <SectionBox title="Frequency" color="ellapurple" initCollapsed={false}>
      <Editor
        key={`frequency-comment-${selectedAlleleId}`}
        id="frequency-comment"
        placeholder="Frequency - comments"
        initialValue={comment}
        onBlur={onChange}
        templates={templates}
        readOnly={readOnly}
      />
      <ConfigurableAnnotation section="frequency" />
    </SectionBox>
  )
}
