Feature: Variants workflow

  Background:
    Given state "test-bdd"

  Scenario: Start and finsh variant interpretation in all workflow steps
    Given I am logged in as "testuser1"
    When I set "variant" to "NM_000059.3(BRCA2):c.10G>T"
    And I go to variants overview

    # For each step, check that variant is listed under the correct Overview section and possible to start and finish.
    # Also tests reassigning and that links on Overview page work ("When I press"). #

    ## Default: In "Interpretation"
    Then I should see "$variant" in "Interpretation"

    ## After start: In "Your analyses"
    When I view the variant with name "$variant"
    And I start the interpretation
    And I go to variants overview
    Then I should see "$variant" in "Your variants"

    ## For testuser2: In "Others' variants"
    Given I am logged in as "testuser2"
    When I set "variant" to "NM_000059.3(BRCA2):c.10G>T"
    And I go to variants overview
    Then I should see "$variant" in "Others' variants"

    ## testuser2 can reassign interpretation
    When I view the variant with name "$variant"
    And I reassign to me

    ## Reassign and continue with testuser1
    Given I am logged in as "testuser1"
    When I view the variant with name "$variant"
    And I reassign to me

    ## In "Pending review"
    When I finish "Interpretation" and send to "Review"
    Then I should see "$variant" in "Pending review"

    ## Classify variant and finalize interpretatoin; In "Finalized"
    When I press "$variant"
    And I start "Review"
    And I submit the class as "Class 3"
    And I finish "Review" and send to "Finalized"
    Then I should see "$variant" in "Finalized"

  Scenario: Add comment to each section and verify with other user
    Given I am logged in as "testuser1"
    # Using c.97G>T
    When I view the allele with id 852

    # Verify comment fields are read-only before start
    Then I should see an element with xpath "//div[@id='evaluation-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='classification-report-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='region-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='frequency-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='prediction-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='external-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='references-comment-editor-content']//div[@contenteditable='false']"

    # Add a comment to each section
    When I start the interpretation
    And I type "Test Evaluation comment" into the editor with xpath "//div[@id='evaluation-comment-editor-content']"
    And I type "Test Report comment" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I type "Test Region comment" into the editor with xpath "//div[@id='region-comment-editor-content']"
    And I type "Test Frequency comment" into the editor with xpath "//div[@id='frequency-comment-editor-content']"
    And I type "Test Prediction comment" into the editor with xpath "//div[@id='prediction-comment-editor-content']"
    And I type "Test External comment" into the editor with xpath "//div[@id='external-comment-editor-content']"
    And I type "Test References comment" into the editor with xpath "//div[@id='references-comment-editor-content']"

    # Check that comment is visible to other user for correct allele only
    When I save the interpretation

    Given I am logged in as "testuser2"
    When I view the allele with id 852
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

    When I view the allele with id 849
    Then I should not see "Test Evaluation comment"
    And I should not see "Test Report comment"
    And I should not see "Test Region comment"
    And I should not see "Test Frequency comment"
    And I should not see "Test Prediction comment"
    And I should not see "Test External comment"
    And I should not see "Test References comment"

    # Check that comment is visible for the allele c.97G>T (submitted class) in an analysis
    Given I am logged in as "testuser1"
    When I view the allele with id 852
    And I submit the class as "Class 3"

    Given I am logged in as "testuser2"
    When I view the analysis with id 10
    And I select the "Classified" tab
    And I select the allele row 0 from the allele table
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

  Scenario: Edit a Report comment for a submitted, still valid classification
    Given I am logged in as "testuser1"
    When I view the allele with id 852
    And I start the interpretation
    And I type "Test Report comment" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I submit the class as "Class 3"

    Given I am logged in as "testuser2"
    When I view the allele with id 852
    And I reassign to me
    And I type " edited" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I press "Submit Report"
    Then I should see "Test Report comment edited"

    # Check that edits are visible for other user in an analysis
    Given I am logged in as "testuser1"
    When I view the analysis with id 10
    And I select the "Classified" tab
    And I select the allele row 0 from the allele table
    Then I should see "Test Report comment edited"

# TODO: Add and check further changes to Prediction and Reference sections