import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { AlleleCollision, mapFromApi } from 'types/store/AlleleCollisions'

const initialState: Record<number, AlleleCollision[]> = {}

const fetchAlleleCollisions = createAsyncThunk<typeof initialState, number>(
  'alleleCollisions/fetchAlleleCollisions',
  async (alleleId: number) => {
    const alleleCollisions = await API.AlleleCollisions.get(alleleId).then(
      (result) => result.data,
    )
    return { [alleleId]: alleleCollisions.map(mapFromApi) }
  },
)

const alleleCollisionsSlice = createSlice({
  name: 'alleleCollisions',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAlleleCollisions.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectAlleleCollisionsByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
) => state.alleleCollisions[alleleId]

export { fetchAlleleCollisions, selectAlleleCollisionsByAlleleId }

export default alleleCollisionsSlice.reducer
