import React, {
  Context,
  ReactElement,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import API from 'app/API'

import { processAndStoreAlleleAssessment } from 'store/interpretation/alleleAssessmentSlice'
import {
  fetchAlleleInterpretationLogForAlleleId,
  selectAlleleInterpretationLogByAlleleId,
} from 'store/interpretation/alleleInterpretationLogSlice'
import {
  fetchAlleleInterpretationsForAlleleId,
  selectDefaultAlleleInterpretationByAlleleId,
} from 'store/interpretation/alleleInterpretationSlice'
import { processAndStoreAlleleReport } from 'store/interpretation/alleleReportSlice'
import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleInterpretation } from 'types/store/AlleleInterpretation'
import {
  AlleleInterpretationContext,
  PresentationAllele,
  createPresentationAllele,
} from 'types/view/Interpretation'

import { DeepPartial } from 'utils/types'

import { InterpretationState } from 'views/interpretation/InterpretationStateContext'

let alleleInterpretationContext: Context<AlleleInterpretationContext>

function useAlleleInterpretationContext(alleleId: number): AlleleInterpretationContext {
  const dispatch = useDispatch()
  const [ready, setReady] = useState(false)
  const [presentationAlleles, setPresentationAlleles] = useState<
    Record<number, PresentationAllele>
  >({})
  const interpretation = useSelector(
    (state: RootState) =>
      selectDefaultAlleleInterpretationByAlleleId(state, {
        alleleId,
      }) as AlleleInterpretation,
  )

  const deleteMessage = async (id: number) => {
    await API.AlleleInterpreationLogs.del(alleleId, id)
    await dispatch(fetchAlleleInterpretationLogForAlleleId(alleleId))
  }

  const finalizeWorkflow = async (interpretationState: InterpretationState) => {
    await API.FinalizeAlleleInterpretation.post(
      alleleId,
      interpretationState,
      presentationAlleles,
    )
    await loadAllele()
  }

  const changeWorkflowStatus = async (
    newWorkflowStatus: AlleleInterpretation['workflowStatus'],
    interpretationState: InterpretationState,
  ) => {
    await API.ChangeAlleleInterpretationWorkflowStatus.post(
      alleleId,
      interpretationState,
      newWorkflowStatus,
      presentationAlleles,
    )
    await loadAllele()
  }

  const updateMessage = async (id: number, updatedMessage: string) => {
    await API.AlleleInterpreationLogs.patch(alleleId, id, { message: updatedMessage })
    await dispatch(fetchAlleleInterpretationLogForAlleleId(alleleId))
  }

  const updateInterpretationLog = async (payload) => {
    await API.AlleleInterpreationLogs.post(alleleId, payload)
    await dispatch(fetchAlleleInterpretationLogForAlleleId(alleleId))
  }

  const interpretationLogItems = useSelector((state: RootState) =>
    selectAlleleInterpretationLogByAlleleId(state, { alleleId }),
  )

  const loadAllele = async () => {
    await API.AlleleInterpretationAllele.get(alleleId, interpretation.id)
      .then((result) => result.data)
      .then(([allele]) => {
        const promises = [] as Promise<unknown>[]
        promises.push(dispatch(processAndStoreAllele(allele)))
        if (allele.allele_assessment) {
          promises.push(
            dispatch(
              processAndStoreAlleleAssessment(
                allele.allele_assessment as ApiTypes.AlleleAssessment,
              ),
            ),
          )
        }
        if (allele.allele_report) {
          promises.push(dispatch(processAndStoreAlleleReport(allele.allele_report)))
        }
        setPresentationAlleles(createPresentationAllele(allele))
        return Promise.all(promises)
      })
  }

  useEffect(() => {
    loadAllele().then(() => setReady(true))
  }, [])

  const reassignToMe = async () => {
    await API.ReassignAlleleInterpretationToMe.post(alleleId)
    await dispatch(fetchAlleleInterpretationsForAlleleId(alleleId))
    await loadAllele()
  }

  const start = async () => {
    await API.StartAlleleInterpretationOrReview.post({
      alleleId,
      genePanelVersion: interpretation.genePanelVersion,
      genePanelName: interpretation.genePanelName,
    })
    await dispatch(fetchAlleleInterpretationsForAlleleId(alleleId))
    await loadAllele()
  }

  const reopen = async () => {
    await API.ReopenAlleleInterpretation.post(alleleId)
    await dispatch(fetchAlleleInterpretationsForAlleleId(alleleId))
    await loadAllele()
  }

  return {
    id: alleleId,
    interpretation,
    presentationAlleles,
    reloadAlleles: loadAllele,
    start,
    reassignToMe,
    reopen,
    finalizeWorkflow,
    changeWorkflowStatus,
    deleteMessage,
    updateMessage,
    updateInterpretationLog,
    interpretationLogItems,
    ready,
  }
}

export function AlleleInterpretationProvider({
  alleleId,
  children,
}: {
  alleleId: number
  children: ReactNode
}): ReactElement | null {
  // We create and assign a ref in order to force HMR to keep the state of the context.
  // Once https://github.com/vitejs/vite/issues/3301 is resolved the ref and assignment can go away.
  const contextRef = useRef()
  const context = useAlleleInterpretationContext(alleleId)

  // eslint-disable-next-line no-multi-assign
  alleleInterpretationContext = (contextRef.current as any) ??= createContext<
    DeepPartial<AlleleInterpretationContext>
  >({})

  if (!context.ready) {
    return null
  }

  return (
    <alleleInterpretationContext.Provider value={context}>
      {children}
    </alleleInterpretationContext.Provider>
  )
}

export default function AlleleInterpretationConsumer(): AlleleInterpretationContext {
  return useContext(
    alleleInterpretationContext ??
      (createContext<DeepPartial<AlleleInterpretationContext>>(
        {},
      ) as unknown as Context<AlleleInterpretationContext>),
  )
}
