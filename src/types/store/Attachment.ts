import * as ApiTypes from 'types/api/Pydantic'

export interface Attachment {
  filename: string
  size: number
  dateCreated: string
  mimeType: string | null
  extension: string
  thumbnail: string | null
  userId: number
}

export function mapFromApi(attachmentFromApi: ApiTypes.Attachment): Attachment {
  return {
    userId: attachmentFromApi.user.id,
    filename: attachmentFromApi.filename,
    size: attachmentFromApi.size,
    dateCreated: attachmentFromApi.date_created,
    mimeType: attachmentFromApi.mimetype || null,
    extension: attachmentFromApi.extension,
    thumbnail: attachmentFromApi.thumbnail || null,
  }
}
