// Due to https://github.com/tailwindlabs/prettier-plugin-tailwindcss/issues/31
// the tailwind plugin does not work well with other plugins, namely prettier-plugin-sort-imports in our case.
// Here we make our own custom plugin and parse sequentially with both to solve the issue.

const pluginSortImports = require('@trivago/prettier-plugin-sort-imports')
const pluginTailwindcss = require('prettier-plugin-tailwindcss')

/** @type {import("prettier").Parser}  */
const ellaParser = {
  ...pluginSortImports.parsers.typescript,
  parse: pluginTailwindcss.parsers.typescript.parse,
}

/** @type {import("prettier").Plugin}  */
const ellaPlugin = {
  parsers: {
    typescript: ellaParser,
  },
}

module.exports = {
  printWidth: 90,
  trailingComma: 'all',
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  importOrder: [
    '<THIRD_PARTY_MODULES>',
    '^apis/(.*)$',
    '^app/(.*)$',
    '^components/(.*)$',
    '^config/(.*)$',
    '^helpers/(.*)$',
    '^hooks/(.*)$',
    '^resources/(.*)$',
    '^store/(.*)$',
    '^types/(.*)$',
    '^utils/(.*)$',
    '^views/(.*)$',
  ],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
  plugins: [ellaPlugin],
}
