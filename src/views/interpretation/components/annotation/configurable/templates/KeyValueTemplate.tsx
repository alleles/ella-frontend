import React from 'react'

import { KeyValueSchema } from 'types/store/AnnotationConfig'

interface KeyValueTemplateProp {
  keyValueData: any
  schema: KeyValueSchema
}

function wrapData<T>(data: T): Array<T> {
  return Array.isArray(data) ? data : [data]
}

type KeyValue = {
  key: string
  value: string
}

function keyValueTable(dataInstance: any, schema: KeyValueSchema): Array<KeyValue> {
  const data = wrapData(dataInstance)
  return Object.entries(schema.config.names).reduce((acc, keyValue) => {
    const [key, keyName] = [keyValue[0], keyValue[1]]
    if (key in data[0]) {
      return [...acc, { key: keyName, value: data[0][key] }]
    }
    return [...acc, { key: keyName, value: '-' }]
  }, [] as Array<KeyValue>)
}

export function KeyValueTemplate({ keyValueData, schema }: KeyValueTemplateProp) {
  const keyValues = keyValueTable(keyValueData, schema)
  return (
    <div className="p-4">
      {keyValues.map(({ key, value }) => (
        <div className="grid grid-cols-2 gap-4" key={key}>
          <span className="text-sm">{key}:</span>
          <span className="text-sm">{value}</span>
        </div>
      ))}
    </div>
  )
}
