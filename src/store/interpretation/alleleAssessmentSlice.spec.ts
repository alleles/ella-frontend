import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import alleleAssessmentReducer, {
  fetchAlleleAssessmentForAlleleId,
  selectAlleleAssessmentById,
} from 'store/interpretation/alleleAssessmentSlice'
import { store } from 'store/store'

import { AlleleAssessment } from 'types/store/AlleleAssessment'

const mock = new MockAdapter(axios)

function buildAlleleAssessment(override: Partial<AlleleAssessment>): AlleleAssessment {
  return {
    id: -1,
    alleleId: -1,
    dateCreated: '1970-01-01',
    secondsSinceUpdate: 1000,
    dateSuperseded: null,
    classification: null,
    genePanelName: '',
    genePanelVersion: '',
    annotationId: 0,
    customAnnotationId: 0,
    previousAssessmentId: null,
    userId: 0,
    evaluation: {} as AlleleAssessment['evaluation'],
    attachmentIds: [],
    ...override,
  }
}

describe('allele assessment reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(alleleAssessmentReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching allele assessments from the backend', async () => {
    mock.onGet(API.AlleleAssessments.path).reply((config) => {
      if (config.params.q.allele_id === 1) {
        return [
          200,
          [
            {
              id: 2,
              classification: '1',
              attachment_ids: [],
              user_id: 3,
              allele_id: 'REMOVED',
              user: 'REMOVED',
            },
          ],
        ]
      }
      return [400]
    })

    const dispatch = jest.fn()
    await fetchAlleleAssessmentForAlleleId(1)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          2: expect.objectContaining({
            classification: '1',
            attachmentIds: [],
            userId: 3,
          }),
        },
      }),
    )
  })

  it('should handle getting allele assessments by id', async () => {
    const state = {
      ...store.getState(),
      alleleAssessments: {
        1: buildAlleleAssessment({ classification: '1' }),
        2: buildAlleleAssessment({ classification: '2' }),
      },
    }

    expect(selectAlleleAssessmentById(state, { assessmentId: 1 })).toEqual(
      buildAlleleAssessment({ classification: '1' }),
    )
    expect(selectAlleleAssessmentById(state, { assessmentId: 2 })).toEqual(
      buildAlleleAssessment({ classification: '2' }),
    )
    expect(selectAlleleAssessmentById(state, { assessmentId: 3 })).toEqual(undefined)
  })
})
