import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import customAnnotationReducer, {
  fetchCustomAnnotationsForAlleleId,
  selectCustomAnnotationsByAnnotationId,
} from 'store/interpretation/customAnnotationSlice'
import { store } from 'store/store'

import { CustomAnnotation as ApiCustomAnnotation } from 'types/api/Pydantic'
import { CustomAnnotation } from 'types/store/CustomAnnotation'

export function buildCustomAnnotation(
  override: Partial<CustomAnnotation['annotations']>,
): CustomAnnotation {
  return {
    alleleId: 1,
    annotations: {
      ...override,
    },
    userId: 1,
    dateCreated: '1970-01-01',
  }
}

const mock = new MockAdapter(axios)

describe('custom annotation reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(customAnnotationReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching custom annotations from the backend', async () => {
    const response: ApiCustomAnnotation[] = [
      {
        user_id: 1,
        date_created: '',
        id: 1, // Removed
        allele_id: 0,
        annotations: {
          references: [
            { id: 1, source: 'User' },
            { id: 2, source: 'User' },
          ],
          prediction: {
            dna_conservation: '',
            domain: '',
            ortholog_conservation: '',
            paralog_conservation: '',
            repeat: '',
            splice_Effect_manual: '',
          },
          external: {
            foo: 'bar',
          },
        },
      },
    ]

    mock.onGet(API.CustomAnnotations.path).reply(200, response)

    const dispatch = jest.fn()
    await fetchCustomAnnotationsForAlleleId(1)(dispatch, () => {}, undefined)

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          1: {
            userId: 1,
            dateCreated: '',
            alleleId: 0,
            annotations: {
              references: [
                { id: 1, source: 'User' },
                { id: 2, source: 'User' },
              ],
              external: {
                foo: 'bar',
              },
              prediction: {
                dnaConservation: null,
                domain: null,
                orthologConservation: null,
                paralogConservation: null,
                repeat: null,
                spliceEffectManual: null,
              },
            },
          },
        },
      }),
    )
  })

  it('should handle getting custom annotations by id', async () => {
    const state = {
      ...store.getState(),
      customAnnotations: {
        1: buildCustomAnnotation({ references: [{ source: 'User', id: 1 }] }),
        2: buildCustomAnnotation({ references: [{ source: 'User', id: 2 }] }),
      },
    }

    expect(
      selectCustomAnnotationsByAnnotationId(state, { customAnnotationId: 1 }),
    ).toEqual(buildCustomAnnotation({ references: [{ source: 'User', id: 1 }] }))
    expect(
      selectCustomAnnotationsByAnnotationId(state, { customAnnotationId: 2 }),
    ).toEqual(buildCustomAnnotation({ references: [{ source: 'User', id: 2 }] }))
    expect(
      selectCustomAnnotationsByAnnotationId(state, { customAnnotationId: 3 }),
    ).toEqual(undefined)
  })
})
