Feature: Feature name
  Background:
    Given state "test-bdd"

  Scenario: Scenario name
    Given I am logged in as "testuser1"
    When I view the analysis with id 8
    And I start the interpretation
    And I type "Hello world" into the editor with xpath "//div[@id='evaluation-comment-editor-content']"
    And I press "Save"
