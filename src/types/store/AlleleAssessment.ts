import * as ApiTypes from 'types/api/Pydantic'
import * as AcmgTypes from 'types/store/Acmg'
import {
  StateAlleleAssessment,
  mapFromApiStateAlleleAssessment,
} from 'types/store/AlleleInterpretation'

export interface AlleleAssessment extends StateAlleleAssessment {
  id: number
  alleleId: number
  dateCreated: string
  dateSuperseded: string | null
  secondsSinceUpdate: number
  genePanelName: string
  genePanelVersion: string
  annotationId: number
  customAnnotationId: number | null
  previousAssessmentId: number | null
  userId: number
}

export function mapFromApi(
  alleleAssessmentFromApi: ApiTypes.AlleleAssessment,
): AlleleAssessment {
  const {
    id,
    allele_id,
    evaluation,
    attachment_ids,
    classification,
    date_created,
    date_superceeded,
    seconds_since_update,
    genepanel_name,
    genepanel_version,
    annotation_id,
    custom_annotation_id,
    previous_assessment_id,
    user_id,
  } = alleleAssessmentFromApi
  return {
    id,
    alleleId: allele_id,
    dateCreated: date_created,
    dateSuperseded: date_superceeded || null,
    secondsSinceUpdate: seconds_since_update,
    genePanelName: genepanel_name,
    genePanelVersion: genepanel_version,
    annotationId: annotation_id as number, // Checked database - all rows in alleleassessment have an annotation id
    customAnnotationId: custom_annotation_id || null,
    previousAssessmentId: previous_assessment_id || null,
    userId: user_id,
    ...mapFromApiStateAlleleAssessment({ evaluation, attachment_ids, classification }),
  }
}

export function mapToApiNewAlleleAssessment(
  alleleId: number,
  alleleAssessment: StateAlleleAssessment,
): ApiTypes.NewAlleleAssessment {
  return {
    allele_id: alleleId,
    reuse: false,
    evaluation: {
      external: {
        comment: alleleAssessment.evaluation?.external?.comment || '',
      },
      frequency: {
        comment: alleleAssessment.evaluation?.frequency?.comment || '',
      },
      reference: {
        comment: alleleAssessment.evaluation?.reference?.comment || '',
      },
      prediction: {
        comment: alleleAssessment.evaluation?.prediction?.comment || '',
      },
      classification: {
        comment: alleleAssessment.evaluation?.classification?.comment || '',
      },
      similar: {
        comment: alleleAssessment.evaluation?.similar?.comment || '',
      },
      acmg: {
        suggested:
          alleleAssessment.evaluation?.acmg.suggested?.map(
            AcmgTypes.mapToApiSuggestedItem,
          ) ?? [],
        included:
          alleleAssessment.evaluation?.acmg?.included?.map(
            AcmgTypes.mapToApiIncludedItem,
          ) ?? [],
        suggested_classification:
          alleleAssessment.evaluation?.acmg.suggestedClassification,
      },
      // presented_alleleassessment_id: ? This attribute is missing in the API
    },
    attachment_ids: alleleAssessment.attachmentIds,
    classification:
      alleleAssessment.classification as ApiTypes.AlleleAssessmentClassification,
  }
}
