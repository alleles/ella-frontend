import { Tab } from '@headlessui/react'
import React from 'react'

const StyledTabGroup = Tab.Group
const StyledTabPanel = Tab.Panel
const StyledTabPanels = Tab.Panels

function StyledTabList(props: Parameters<typeof Tab.List>[0]) {
  return <Tab.List className="flex h-16 pt-3" {...props} />
}

function StyledTab(props: Parameters<typeof Tab>[0]) {
  return (
    <Tab
      className={({ selected }) =>
        `inline-flex items-center border-b-2 border-transparent px-4 pt-1 text-sm font-medium text-ellagray-500 ${
          selected
            ? 'border-ellablue-darker text-ellagray-900'
            : 'hover:border-ellagray hover:text-ellagray-700'
        }`
      }
      {...props}
    />
  )
}

StyledTab.Group = StyledTabGroup
StyledTab.List = StyledTabList
StyledTab.Panel = StyledTabPanel
StyledTab.Panels = StyledTabPanels

export default StyledTab
