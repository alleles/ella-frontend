import { AlleleState } from 'types/store/AlleleInterpretation'

import { referenceAnnotationByStatus } from 'views/interpretation/sections/references/ReferenceByStatus'

const reference = [
  {
    id: 13,
    authors: 'Szabo C et al.',
    title: 'The breast cancer information core: database design, structure, and scope.',
    journal: 'Hum. Mutat.: 16(2), 123-31.',
    abstract:
      'The Breast Cancer Information Core (BIC) is an open access, on-line mutation database for breast cancer susceptibility genes. In addition to creating a catalogue of all mutations and polymorphisms in breast cancer susceptibility genes, a principle aim of the BIC is to facilitate the detection and characterization of these genes by providing technical support in the form of mutation detection protocols, primer sequences, and reagent access. Additional information at the site includes a literature review compiled from published studies, links to other internet-based, breast cancer information and research resources, and an interactive discussion forum which enables investigators to post or respond to questions and/or comments on a bulletin board. Hum Mutat 16:123-131, 2000. Published 2000 Wiley-Liss, Inc.',
    pubmedId: 10923033,
    published: true,
    year: 2000,
  },
  {
    id: 127,
    authors: 'Cvok ML et al.',
    title:
      'New sequence variants in BRCA1 and BRCA2 genes detected by high-resolution melting analysis in an elderly healthy female population in Croatia.',
    journal: 'Clin. Chem. Lab. Med.: 46(10), 1376-83.',
    abstract:
      'Mutations in BRCA1 and BRCA2 genes are associated with family predisposition to breast and ovarian cancer. Novel screening methods are required for efficient and rapid detection of sequence variants in cancer patients and their family members.\nThe screening for variants in the breast and ovarian cancer susceptibility genes BRCA1 and BRCA2 in Croatia was performed by a high-resolution melting approach, which is based on differences in melting curves caused by variations in nucleotide sequence. This is the first screening in Croatia on elderly healthy women with no family history of cancer. BRCA1 screening was performed on 220 and BRCA2 screening on 115 samples.\nIn a population well beyond the average age of breast/ovarian cancer onset, 21 different sequence variants in the BRCA1 gene (one novel: c.5193+49_50delTA) and 36 variants in the BRCA2 gene (7 novel: c.459A>C, c.3318C>A, c.4412_ 4414delGAA, c.4790C>A, c.6264T>C, c.9087G>A, and c.9864A>G) were detected.\nNine BRCA1 and seven BRCA2 known variants appeared with such high frequencies that they could be declared as harmless in this population. Eight BRCA1 high frequency variants, located further from the promoter region, appear to be strongly correlated. Three novel variants that changed the amino acid sequence of the BRCA2 protein (two missense base substitutions, c.3318C>A and c.4790C>A, and one codon deletion c.4412_4414delGAA), appearing only once, were predicted to have no potential effect on protein structure and function.',
    pubmedId: 18844490,
    published: true,
    year: 2008,
  },
  {
    id: 134,
    authors: 'Caux-Moncoutier V et al.',
    title:
      'Impact of BRCA1 and BRCA2 variants on splicing: clues from an allelic imbalance study.',
    journal: 'Eur. J. Hum. Genet.: 17(11), 1471-80.',
    abstract:
      'Nearly one-half of BRCA1 and BRCA2 sequence variations are variants of uncertain significance (VUSs) and are candidates for splice alterations for example, by disrupting/creating splice sites. As out-of-frame splicing defects lead to a marked reduction of the level of the mutant mRNA cleared through nonsense-mediated mRNA decay, a cDNA-based test was developed to show the resulting allelic imbalance (AI). Fifty-four VUSs identified in 53 hereditary breast/ovarian cancer (HBOC) patients without BRCA1/2 mutation were included in the study. Two frequent exonic single-nucleotide polymorphisms on both BRCA1 and BRCA2 were investigated by using a semiquantitative single-nucleotide primer extension approach and the cDNA allelic ratios obtained were corrected using genomic DNA ratios from the same sample. A total of five samples showed AI. Subsequent transcript analyses ruled out the implication of VUS on AI and identified a deletion encompassing BRCA2 exons 12 and 13 in one sample. No sequence abnormality was found in the remaining four samples, suggesting implication of cis- or trans-acting factors in allelic expression regulation that might be disease causative in these HBOC patients. Overall, this study showed that AI screening is a simple way to detect deleterious splicing defects and that a major role for VUSs and deep intronic mutations in splicing anomalies is unlikely in BRCA1/2 genes. Methods to analyze gene expression and identify regulatory elements in BRCA1/2 are now needed to complement standard approaches to mutational analysis.',
    pubmedId: 19471317,
    published: true,
    year: 2009,
  },
  {
    id: 342,
    authors: 'Meisel C et al.',
    title:
      'Spectrum of genetic variants of BRCA1 and BRCA2 in a German single center study.',
    journal: 'Arch. Gynecol. Obstet.: 295(5), 1227-1238.',
    abstract:
      'Determination of mutation status of BRCA1 and BRCA2 has become part of the clinical routine. However, the spectrum of genetic variants differs between populations. The aim of this study was to deliver a comprehensive description of all detected variants.\nIn families fulfilling one of the German Consortium for Hereditary Breast and Ovarian Cancer (GC-HBOC) criteria for genetic testing, one affected was chosen for analysis. DNA of blood lymphocytes was amplified by PCR and prescreened by DHPLC. Aberrant fragments were sequenced. All coding exons and splice sites of BRCA1 and BRCA2 were analyzed. Screening for large rearrangements in both genes was performed by MLPA.\nOf 523 index patients, 121 (23.1%) were found to carry a pathogenic or likely pathogenic (class 4/5) mutation. A variant of unknown significance (VUS) was detected in 73/523 patients (13.9%). Two mutations p.Gln1756Profs*74 and p.Cys61Gly comprised 42.3% (n = 33/78) of all detected pathogenic mutations in BRCA1. Most of the other mutations were unique mutations. The most frequently detected mutation in BRCA2 was p.Val1283Lys (13.9%; n = 6/43). Altogether, 101 different neutral genetic variants were counted in BRCA1 (n = 35) and in BRCA2 (n = 66).\nThe two most frequently detected mutations are founder mutations in Poland and Czech Republic. More similarities seem to be shared with our direct neighbor countries compared to other European countries. For comparison of the extended genotype, a shared database is needed.',
    pubmedId: 28324225,
    published: true,
    year: 2017,
  },
  {
    id: 371,
    authors: '',
    title: 'some article',
    journal: 'HGMD.',
    abstract: null,
    pubmedId: null,
    published: true,
    year: 2021,
  },
  {
    id: 372,
    authors: '',
    title: 'some article unpubs',
    journal: 'HGMD.',
    abstract: null,
    pubmedId: null,
    published: false,
    year: 2021,
  },
  {
    id: 373,
    authors: '',
    title: '',
    journal: '',
    abstract: null,
    pubmedId: null,
    published: true,
    year: 2010,
  },
  {
    id: 374,
    authors: '',
    title: 'some earlier unpublished',
    journal: 'OUS/AMG.',
    abstract: null,
    pubmedId: null,
    published: false,
    year: 2009,
  },
  {
    id: 375,
    authors: '',
    title: 'unpub2',
    journal: 'OUS/AMG.',
    abstract: null,
    pubmedId: null,
    published: false,
    year: 1997,
  },
  {
    id: 376,
    authors: '',
    title: 'last unpub',
    journal: 'OUS/AMG.',
    abstract: 'should be on top',
    pubmedId: null,
    published: false,
    year: 2022,
  },
]

const customAnnotation = {
  references: [
    {
      id: 372,
      source: 'User',
    },
    {
      id: 373,
      source: 'User',
    },
    {
      id: 374,
      source: 'User',
    },
    {
      id: 376,
      source: 'User',
    },
    {
      id: 371,
      source: 'User',
    },
    {
      id: 375,
      source: 'User',
    },
  ],
}

const configurableAnnotation = {
  references: [
    {
      pubmedId: 19471317,
      source: 'CLINVAR',
    },
    {
      pubmedId: 28324225,
      source: 'HGMD',
    },
    {
      pubmedId: 18844490,
      source: 'CLINVAR',
    },
    {
      pubmedId: 10923033,
      source: 'CLINVAR',
    },
  ],
}

const alleleState = {
  alleleAssessmentId: null,
  reuseAlleleAssessment: false,
  alleleReportId: null,
  reuseAlleleReport: false,
  referenceAssessmentIds: [],
  alleleReport: {
    comment: '',
  },
  alleleAssessment: {
    evaluation: {},
  },
  referenceAssessments: [
    {
      referenceId: 374,
      dateCreated: 'N/A',
      evaluation: {
        comment: 'supergood',
        relevance: 'Yes',
        refQuality: 'excellent',
      },
    },
    {
      referenceId: 372,
      dateCreated: 'N/A',
      evaluation: {
        comment: 'supergood2',
        relevance: 'Yes',
        refQuality: 'excellent',
      },
    },
    {
      referenceId: 376,
      dateCreated: 'N/A',
      evaluation: {
        comment: 'supergood3',
        relevance: 'Yes',
        refQuality: 'excellent',
      },
    },
    {
      referenceId: 134,
      dateCreated: 'N/A',
      evaluation: {
        comment: 'not relevant',
        relevance: 'No',
      },
    },
    {
      referenceId: 127,
      dateCreated: 'N/A',
      evaluation: {
        comment: 'not relevant 2',
        relevance: 'No',
      },
    },
  ],
}

describe('references', () => {
  it('should sort references', () => {
    const referenceByStatus = referenceAnnotationByStatus({
      alleleState: alleleState as unknown as AlleleState,
      references: reference,
      annotationReferences: configurableAnnotation.references,
      customAnnotationReferences: customAnnotation.references,
    })

    expect(referenceByStatus.evaluated.map((e) => e.reference.year)).toEqual([
      2022, 2021, 2009,
    ])
    expect(referenceByStatus.pending.map((e) => e.reference.year)).toEqual([
      1997, 2021, 2017, 2010, 2000,
    ])
    expect(referenceByStatus.not_relevant.map((e) => e.reference.year)).toEqual([
      2009, 2008,
    ])
  })
})
