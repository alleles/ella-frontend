import { createSelector } from 'reselect'

import { selectAnalysisInterpretationById } from 'store/interpretation/analysisInterpretationSlice'
import { selectAnalysisById } from 'store/interpretation/analysisSlice'

export const selectDefaultFilterConfigId = createSelector(
  selectAnalysisById,
  selectAnalysisInterpretationById,
  (analysis, interpretation) => {
    if (!analysis || !interpretation) {
      return -1
    }
    return interpretation.state.filterConfigId || analysis.filterConfigIds[0]
  },
)
