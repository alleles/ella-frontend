import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
import commonJs from '@rollup/plugin-commonjs'
import rollupNodeResolve from '@rollup/plugin-node-resolve'
import react from '@vitejs/plugin-react'
import tailwindcss from 'tailwindcss'
import { defineConfig } from 'vite'
import eslint from 'vite-plugin-eslint'
import svgrPlugin from 'vite-plugin-svgr'
import tsconfigPaths from 'vite-tsconfig-paths'

export default defineConfig({
  server: {
    port: 3000,
    hmr: {
      path: '/hmr',
    },
  },
  optimizeDeps: {
    esbuildOptions: {
      define: { global: 'globalThis' },
      plugins: [NodeGlobalsPolyfillPlugin({ buffer: true })],
    },
  },
  build: {
    commonjsOptions: {
      include: ['src'],
    },
    rollupOptions: {
      plugins: [rollupNodeResolve() as any, commonJs()],
    },
  },
  plugins: [
    tailwindcss(),
    eslint({ failOnError: true, cache: true, exclude: 'node_modules/**' }),
    react(),
    tsconfigPaths(),
    svgrPlugin({
      svgrOptions: { icon: true },
    }),
  ],
})
