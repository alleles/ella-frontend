import { Switch } from '@headlessui/react'
import React from 'react'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}
interface ToggleProps {
  name?: string
  onChange: (value: boolean) => void
  checked: boolean
  label?: string
}
export default function Toggle({ name, onChange, checked, label }: ToggleProps) {
  return (
    <div className="flex flex-row gap-x-1">
      <span className="text-n mr-1 text-sm">{label}</span>
      <Switch
        name={name}
        checked={checked}
        onChange={onChange}
        className={classNames(
          checked ? 'bg-ellablue-darkest' : 'bg-ellagray',
          'relative inline-flex h-6 w-11 shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-ellablue-darker focus:ring-offset-2',
        )}
      >
        <span
          aria-hidden="true"
          className={classNames(
            checked ? 'translate-x-5' : 'translate-x-0',
            'pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out',
          )}
        />
      </Switch>
    </div>
  )
}
