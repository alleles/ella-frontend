// TODO: Merge this with alleleinterpretation
import * as ApiTypes from 'types/api/Pydantic'
import * as User from 'types/store/User'

export interface AlleleInterpretationOverview {
  id: number
  status: ApiTypes.InterpretationStatus
  finalized?: boolean
  workflowStatus: ApiTypes.AlleleInterpretationWorkflowStatus
  alleleId: number
  genePanelName: string
  genePanelVersion: string
  dateLastUpdate: string
  userId?: number
  user: User.User | null
}

export function mapFromApi(
  alleleInterpretationOverviewFromApi: ApiTypes.AlleleInterpretationOverview,
): AlleleInterpretationOverview {
  return {
    alleleId: alleleInterpretationOverviewFromApi.allele_id,
    dateLastUpdate: alleleInterpretationOverviewFromApi.date_last_update,
    finalized: alleleInterpretationOverviewFromApi.finalized,
    genePanelName: alleleInterpretationOverviewFromApi.genepanel_name,
    genePanelVersion: alleleInterpretationOverviewFromApi.genepanel_version,
    id: alleleInterpretationOverviewFromApi.id,
    status: alleleInterpretationOverviewFromApi.status,
    user: alleleInterpretationOverviewFromApi.user
      ? User.mapFromApi(alleleInterpretationOverviewFromApi.user as ApiTypes.UserResponse)
      : null,
    userId: alleleInterpretationOverviewFromApi.user_id,
    workflowStatus: alleleInterpretationOverviewFromApi.workflow_status,
  }
}
