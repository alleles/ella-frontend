import { deepCopy } from 'utils/general'

import { TrackTypeBase } from 'views/interpretation/genomicsViewer/TrackTypeBase'

/* eslint-disable class-methods-use-this */

// "normal" tracks
export class TrackTypeDefault extends TrackTypeBase {
  matchesType(type: String | null): boolean {
    return type !== 'roi'
  }

  getIgvLoadedTrackIds(browser: any): String[] {
    return browser.trackViews
      .filter((tv) => !['ideogram', 'sequence', 'ruler'].includes(tv.track.type))
      .map((tv) => tv.track.name)
  }

  removeTrack(browser: any, trackId: String): void {
    browser.removeTrackByName(trackId)
  }

  async loadTrack(browser: any, igvcfg: any): Promise<void> {
    await browser.loadTrack(deepCopy<any>(igvcfg))
  }
}

/* eslint-enable class-methods-use-this */
