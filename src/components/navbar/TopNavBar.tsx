import { Disclosure } from '@headlessui/react'
import React from 'react'
import { NavLink } from 'react-router-dom'

import AlertBar from 'components/navbar/AlertBar'
import BroadcastNotifier from 'components/navbar/BroadcastNotifier'
import UserDropDownMenu from 'components/navbar/UserDropDownMenu'
import Search from 'components/navbar/search/Search'

import logo from 'resources/logo_white.svg'

const navLinks = [
  {
    to: '/overview/analyses',
    title: 'Analyses',
  },
  {
    to: '/overview/variants',
    title: 'Variants',
  },
]

export default function TopNavBar() {
  return (
    <div className="sticky z-20">
      <Disclosure as="nav" className="bg-ellablue-darkest">
        {() => (
          <>
            <div className="mx-auto px-4">
              <div className="flex h-12 justify-between">
                <div className="flex">
                  <NavLink to="/" className="flex shrink-0 items-center">
                    <img className="block h-6 w-auto" src={logo} alt="ELLA" />
                  </NavLink>
                  <div className="ml-4 flex">
                    {navLinks.map((link) => (
                      <NavLink
                        key={link.title}
                        to={link.to}
                        className={({ isActive }) =>
                          isActive
                            ? 'inline-flex items-center border-b-2 border-white bg-ellablue-darker px-4 text-sm font-medium tracking-wide text-white'
                            : 'inline-flex items-center border-b-2 border-transparent px-4 text-sm font-medium tracking-wide text-white hover:border-ellablue-lighter hover:text-ellablue-lighter'
                        }
                      >
                        {link.title}
                      </NavLink>
                    ))}
                    <div className="flex items-center">
                      <Search />
                    </div>
                  </div>
                </div>
                <div className="inline-flex content-start items-center space-x-4 self-center text-white">
                  <BroadcastNotifier aria-hidden="true" />
                  <UserDropDownMenu />
                </div>
              </div>
            </div>

            <Disclosure.Panel>
              <div className="space-y-1 pt-2 pb-3">
                {navLinks.map((link) => (
                  <NavLink
                    key={link.title}
                    to={link.to}
                    className={(isActive) =>
                      isActive
                        ? 'border-ellablue-darker bg-ellablue-lightest text-ellablue-darkest'
                        : 'block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-ellagray-500 hover:border-ellagray hover:bg-ellagray-100 hover:text-ellagray-700'
                    }
                  >
                    {link.title}
                  </NavLink>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
      <AlertBar />
    </div>
  )
}
