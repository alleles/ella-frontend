import React, {
  Context,
  ReactElement,
  ReactNode,
  createContext,
  useContext,
  useRef,
  useState,
} from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import API from 'app/API'

import { fetchConfig } from 'store/config/configSlice'
import { fetchGenePanels } from 'store/genePanels/genePanelsSlice'
import { clearStore } from 'store/store'
import { fetchCurrentUserId, fetchUsers } from 'store/users/usersSlice'

interface AuthContext {
  authenticated: boolean
  login: (username?: string, password?: string) => void
  logout: () => void
}

let authContext: Context<AuthContext>

// A custom hook that exposes the authentication state, a login() and a logout() function.
function useAuth(): AuthContext {
  const [authenticated, setAuthenticated] = useState(false)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const postAuth = () => {
    const promises = [
      dispatch(fetchConfig()),
      dispatch(fetchCurrentUserId()),
      dispatch(fetchUsers()),
      dispatch(fetchGenePanels()),
    ]

    Promise.all(promises).then(() => setAuthenticated(true))
  }
  return {
    authenticated,
    login(username, password) {
      if (username === undefined || password === undefined) {
        return postAuth()
      }
      return API.Login.post(username, password).then(async () => {
        await postAuth()
      })
    },
    logout() {
      return API.Logout.post().then(() => {
        dispatch(clearStore)
        setAuthenticated(false)
        navigate('/')
      })
    },
  }
}

export function AuthProvider({ children }: { children: ReactNode }): ReactElement {
  // We create and assign a ref in order to force HMR to keep the state of the context.
  // Once https://github.com/vitejs/vite/issues/3301 is resolved the ref and assignment can go away.
  const contextRef = useRef()
  const auth = useAuth()

  // eslint-disable-next-line no-multi-assign
  authContext = (contextRef.current as any) ??= createContext<Partial<AuthContext>>({})
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

export default function AuthConsumer(): AuthContext {
  return useContext(authContext as Context<AuthContext>)
}
