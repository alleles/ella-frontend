import React, { useEffect, useState } from 'react'

export type ColumnDefinition<TRow> = {
  field: keyof TRow
  title: string
  comparator?: (a: TRow, b: TRow) => number
  render: (v: TRow) => JSX.Element | string
  tooltip?: ((v: TRow) => string) | boolean
  titleTooltip?: string
  alignx?: 'left' | 'right'
}

export interface ClassNames {
  table?: string
}

interface Props<TRow> {
  id?: string
  data: TRow[]
  selectedRowKey?: string
  columns: ColumnDefinition<TRow>[]
  onRowClick?: (row: TRow) => void
  classNames?: ClassNames
}

enum SortOrder {
  ascending = 1,
  descending = -1,
}

function sortOrderSymbol(selectedField, activeField, sortOrder) {
  if (selectedField.field !== activeField.field) {
    return '\u00a0'
  }
  return sortOrder === SortOrder.ascending
    ? '\u2191'
    : [sortOrder === SortOrder.descending ? '\u2193' : '\u00a0']
}

export interface RowKey {
  key: string
}

export default function Table<TRow extends RowKey>({
  id,
  data,
  columns,
  selectedRowKey = undefined,
  onRowClick,
  classNames = {},
}: Props<TRow>) {
  const [columnIndexToSortOn, setColumnIndexToSortOn] = useState<number>(0)
  const [sortOrder, setSortOrder] = useState<SortOrder>(SortOrder.ascending)
  const [sortedData, setSortedData] = useState<TRow[]>(data)

  const toggleSortOrder = (): void => {
    switch (sortOrder) {
      case SortOrder.ascending:
        setSortOrder(SortOrder.descending)
        break
      case SortOrder.descending:
        setSortOrder(SortOrder.ascending)
        setColumnIndexToSortOn(0)
        break
      default:
        setSortOrder(SortOrder.ascending)
        break
    }
  }

  const unsortableCols = columns
    .filter((col) => !col.comparator)
    .map((col) => col.field.toString())

  const onColumnClick = (e, col): void => {
    if (!unsortableCols.includes(col.field.toString())) {
      if (col.field === columns[columnIndexToSortOn].field) {
        toggleSortOrder()
      } else {
        setSortOrder(SortOrder.ascending)
        setColumnIndexToSortOn(columns.indexOf(col))
      }
    }
  }

  useEffect(() => {
    if (
      columns[columnIndexToSortOn] !== undefined &&
      columns[columnIndexToSortOn].comparator !== undefined
    )
      setSortedData(
        [...data].sort(
          (a, b) => (columns[columnIndexToSortOn] as any).comparator(a, b) * sortOrder,
        ),
      )
  }, [sortOrder, columnIndexToSortOn, data])

  return (
    <table id={id} className={classNames.table || 'inline-block'}>
      <thead>
        <tr>
          {columns.map((col: ColumnDefinition<TRow>) => (
            <th
              className={`text-left text-sm font-medium uppercase tracking-wider text-ellagray-700 ${
                col.comparator ? 'cursor-pointer hover:text-ellablue-darker' : ''
              } border-b border-ellagray`}
              key={col.field.toString()}
              title={col.titleTooltip}
              onClick={(e) => {
                onColumnClick(e, col)
              }}
            >
              {col.title}
              <span className="font-mono text-xl">
                {sortOrderSymbol(columns[columnIndexToSortOn], col, sortOrder)}
              </span>
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {sortedData.map((row: TRow) => {
          const isSelectedRow: boolean =
            selectedRowKey !== undefined && row.key === selectedRowKey
          const bgcolDefault = 'bg-white'
          const bgcolHover = 'bg-ellablue-lightest'
          const bgcolSelected = 'bg-ellablue-lighter'
          return (
            <tr
              className={`cursor-pointer ${
                isSelectedRow ? bgcolSelected : bgcolDefault
              } text-sm text-ellagray-900 hover:${
                isSelectedRow ? bgcolSelected : bgcolHover
              }`}
              onClick={
                onRowClick
                  ? () => {
                      onRowClick(row)
                    }
                  : undefined
              }
              key={row.key}
            >
              {columns.map((colDef: ColumnDefinition<TRow>) => {
                const child = colDef.render(row)
                const tooltip: string | undefined = (() => {
                  if (!colDef.tooltip) {
                    return undefined
                  }
                  // use child element as tooltip - useful for capped columns
                  if (colDef.tooltip === true) {
                    return child.toString()
                  }
                  return colDef?.tooltip(row)
                })()
                return (
                  <td
                    key={colDef.field.toString()}
                    className={`truncate whitespace-nowrap ${
                      colDef.alignx === 'right' ? 'text-right' : ''
                    }`}
                    title={tooltip}
                  >
                    {child}
                  </td>
                )
              })}
            </tr>
          )
        })}
        {/* additional row to fix rounded corners + bg col: */}
        <tr className="h-1 cursor-pointer bg-ellagray-100 text-sm text-ellagray-900" />
      </tbody>
    </table>
  )
}
