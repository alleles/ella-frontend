import * as ApiTypes from 'types/api/Pydantic'
import * as AnalysisInterpretationOverview from 'types/store/AnalysisInterpretationOverview'

export interface OverviewAnalysis {
  analysisId: number
  date: string
  name: string
  priority: number
  overviewInterpretations: AnalysisInterpretationOverview.AnalysisInterpretationOverview[]
  reviewComment: string | null
  tags: ('WARNING' | 'HTS' | 'SANGER')[]
}

function getTagsFromAnalysis(
  analysis: ApiTypes.OverviewAnalysis,
): OverviewAnalysis['tags'] {
  const tags: OverviewAnalysis['tags'] = []
  if (analysis.warnings != null) {
    tags.push('WARNING')
  }
  if (analysis.samples.some((sample) => sample.sample_type === 'HTS')) {
    tags.push('HTS')
  }
  if (analysis.samples.some((sample) => sample.sample_type === 'Sanger')) {
    tags.push('SANGER')
  }

  return tags
}

export function mapFromApi(analysisFromApi: ApiTypes.OverviewAnalysis): OverviewAnalysis {
  return {
    analysisId: analysisFromApi.id,
    name: analysisFromApi.name,
    date: analysisFromApi.date_requested || analysisFromApi.date_deposited,
    overviewInterpretations: analysisFromApi.interpretations
      ? analysisFromApi.interpretations.map(AnalysisInterpretationOverview.mapFromApi)
      : [],
    priority: analysisFromApi.priority,
    reviewComment: analysisFromApi.review_comment || null,
    tags: getTagsFromAnalysis(analysisFromApi),
  }
}
