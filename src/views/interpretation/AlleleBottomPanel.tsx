import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import { useLayout } from 'components/layout/LayoutContainer'

import { RootState } from 'store/store'

import { AnalysisInterpretation } from 'types/store/AnalysisInterpretation'

import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'
import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import useAnalysisInterpretationUIContext from 'views/interpretation/analysis/UIContext'
import { AlleleTable } from 'views/interpretation/components/AlleleTable'
import { selectTableDataByCallerAndCategory } from 'views/interpretation/helpers/alleleTable/alleleTable'

interface TabTitle {
  title: string
  badge: string
}

interface TabViewProps {
  tabs: TabTitle[]
  tabContents: JSX.Element[]
  buttons?: JSX.Element[]
}

function TabView({ tabs, tabContents, buttons }: TabViewProps): JSX.Element {
  const [selectedTabIndex, setSelectedTabIndex] = useState<number>(0)

  return (
    <>
      <div className="border-b border-ellablue-lighter bg-ellablue-lightest shadow">
        <div className="flex flex-wrap">
          {buttons}
          <ul className="-mb-px flex flex-wrap">
            {tabs.map((tab, index) => {
              const isSelected = selectedTabIndex === index
              const textColor = isSelected
                ? 'text-ellablue-darkest border-ellablue-darkest active'
                : 'text-ellagray-400 hover:border-ellagray-400 hover:text-ellagray-500'
              const badgeColor = isSelected
                ? 'text-ellagray-100 bg-ellablue-darkest'
                : 'text-ellagray-100 bg-ellagray-500'
              return (
                <li
                  key={tab.title}
                  className={`${isSelected ? 'bg-ellablue-lighter' : 'cursor-pointer'}`}
                  onClick={isSelected ? undefined : () => setSelectedTabIndex(index)}
                >
                  <span
                    className={`inline-block py-2 px-4 text-center text-sm font-medium ${textColor} border-b-2 border-transparent`}
                  >
                    {tab.title}
                    <span
                      className={`mx-2 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none ${badgeColor} rounded-full`}
                    >
                      {tab.badge}
                    </span>
                  </span>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
      <div className="h-[88.5%] overflow-auto px-2 pb-2">
        {tabContents[selectedTabIndex]}
      </div>
    </>
  )
}

export default function AlleleBottomPanel() {
  const { presentationAlleles, interpretation } = useInterpretation()
  const { filterConfigId } = useAnalysisInterpretation()
  const { callerTypeMode } = useAnalysisInterpretationUIContext()
  const { interpretationState } = useInterpretationStateContext()

  const { bottomPanelState } = useLayout()
  const { genePanelName, genePanelVersion } = interpretation

  const tableData = useSelector((state: RootState) =>
    selectTableDataByCallerAndCategory(state, {
      presentationAlleles: Object.values(presentationAlleles),
      genePanelName,
      genePanelVersion,
      filterConfigId,
      interpretationState: interpretationState as AnalysisInterpretation['state'],
    }),
  )

  const categories = [
    ['Unclassified', 'unclassified'],
    ['Classified', 'classified'],
    ['Not Relevant', 'notRelevant'],
    ['Technical', 'technical'],
  ]

  const noVariantsElement = (
    <p className="mt-2 text-center text-lg italic text-ellagray-500">No variants</p>
  )

  if (['collapsed', 'expanded'].includes(bottomPanelState)) {
    const titles = categories.map(
      ([title, key]) =>
        ({ title, badge: tableData[callerTypeMode][key].length } as TabTitle),
    )
    const tabContents = categories.map(([, key]) =>
      Object.keys(tableData[callerTypeMode][key]).length ? (
        <AlleleTable
          id="allele-table"
          key={key}
          tableData={tableData[callerTypeMode][key]}
        />
      ) : (
        noVariantsElement
      ),
    )
    return <TabView tabs={titles} tabContents={tabContents} />
  }
  return (
    <div className="mt-3 space-y-2">
      {categories.map(([textLabel, key]) => (
        <div key={key}>
          <div className="bg-ellablue-lightest px-2 py-1 text-sm tracking-wider text-ellagray-900">
            {textLabel} Variants ({tableData[callerTypeMode][key].length})
          </div>
          {Object.keys(tableData[callerTypeMode][key]).length ? (
            <AlleleTable tableData={tableData[callerTypeMode][key]} />
          ) : (
            noVariantsElement
          )}
        </div>
      ))}
    </div>
  )
}
