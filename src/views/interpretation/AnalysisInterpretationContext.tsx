import React, {
  Context,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import API from 'app/API'

import useBusyIndicator from 'components/busy/BusyIndicator'

import { processAndStoreAlleleAssessment } from 'store/interpretation/alleleAssessmentSlice'
import { processAndStoreAlleleReport } from 'store/interpretation/alleleReportSlice'
import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import {
  fetchAnalysisInterpretationLogForAnalysisId,
  selectAnalysisInterpretationLogByAnalysisId,
} from 'store/interpretation/analysisInterpretationLogSlice'
import {
  fetchAnalysisInterpretationsByAnalysisId,
  selectDefaultAnalysisInterpretation,
} from 'store/interpretation/analysisInterpretationSlice'
import {
  fetchAnalysisByAnalysisId,
  selectAnalysisById,
} from 'store/interpretation/analysisSlice'
import { selectSamplesByAnalysisId } from 'store/interpretation/sampleSlice'
import { selectDefaultFilterConfigId } from 'store/selectors/interpretationSelectors'
import { RootState } from 'store/store'

import { AlleleAssessment } from 'types/api/Pydantic'
import { Analysis } from 'types/store/Analysis'
import {
  AnalysisInterpretation,
  AnalysisInterpretationState,
} from 'types/store/AnalysisInterpretation'
import { Sample } from 'types/store/Sample'
import {
  AnalysisInterpretationContext,
  AnalysisPresentationAllele,
  FilteredAlleles,
  createAnalysisPresentationAlleles,
} from 'types/view/Interpretation'

import { nonNullable } from 'utils/general'
import { DeepPartial } from 'utils/types'

import { InterpretationState } from 'views/interpretation/InterpretationStateContext'

let analysisInterpretationContext: Context<AnalysisInterpretationContext>

// A custom hook that exposes basic analysis interpretation info as well as the state of the interpretation, a state mutator and a save function.
function useAnalysisInterpretationContext(
  analysisId: number,
): AnalysisInterpretationContext {
  const dispatch = useDispatch()
  const { addWork } = useBusyIndicator()

  const [filteredAlleles, setFilteredAlleles] = useState<FilteredAlleles>({
    alleleIds: [],
    excludedAllelesByCallerType: {
      cnv: {},
      snv: {},
    },
  })

  const [presentationAlleles, setPresentationAlleles] = useState<
    Record<number, AnalysisPresentationAllele>
  >({})
  const [ready, setReady] = useState(false)

  const interpretation = useSelector((state: RootState) =>
    selectDefaultAnalysisInterpretation(state, { analysisId }),
  )!

  const samples: Record<number, Sample> = useSelector((state: RootState) =>
    selectSamplesByAnalysisId(state, { analysisId }),
  )

  const filterConfigId = useSelector((state: RootState) =>
    selectDefaultFilterConfigId(state, {
      analysisId,
      analysisInterpretationId: interpretation.id,
    }),
  )

  const analysis: Analysis = useSelector((state: RootState) =>
    selectAnalysisById(state, { analysisId }),
  )!

  // Fetch filter results
  // TODO: Investigate whether data can be preloaded and potentially avoid useEffect.
  useEffect(() => {
    const filterAlleles = async () => {
      const alleleCount = await API.AnalysisStats.get(analysisId).then(
        (result) => result.data.allele_count,
      )
      const { allele_ids, excluded_alleles_by_caller_type } = await addWork(
        API.FilteredAlleles.get(analysisId, interpretation.id, filterConfigId),
        `Filtering ${alleleCount} alleles`,
      ).then((result) => result.data)

      setFilteredAlleles({
        alleleIds: allele_ids,
        excludedAllelesByCallerType: excluded_alleles_by_caller_type,
      })
    }
    filterAlleles()
  }, [filterConfigId])

  const loadAlleles = async (alleleIds: number[]) => {
    await dispatch(fetchAnalysisInterpretationsByAnalysisId(Number(analysisId)))

    const fetch = async () =>
      API.AnalysisInterpretationAlleles.get(
        analysisId,
        interpretation.id,
        alleleIds,
        interpretation.status,
        filterConfigId,
      ).then((result) => result.data)

    // Don't show spinner if we're only loading a single allele
    const alleles =
      alleleIds.length > 1
        ? await addWork(fetch(), `Loading ${alleleIds.length} alleles`)
        : await fetch()

    // Ensure we have all the required data in the store
    const promises = [
      ...alleles.map((allele) => dispatch(processAndStoreAllele(allele))),
      ...alleles
        .map((allele) => allele.allele_assessment)
        .filter(nonNullable)
        .map((alleleAssessment) =>
          dispatch(processAndStoreAlleleAssessment(alleleAssessment as AlleleAssessment)),
        ),
      ...alleles
        .map((allele) => allele.allele_report)
        .filter(nonNullable)
        .map((alleleReport) => dispatch(processAndStoreAlleleReport(alleleReport))),
    ]

    setPresentationAlleles({
      ...presentationAlleles,
      ...createAnalysisPresentationAlleles(alleles),
    })
    return Promise.all(promises)
  }

  // Load alleles
  useEffect(() => {
    if (filteredAlleles.alleleIds.length === 0) {
      return
    }
    loadAlleles(filteredAlleles.alleleIds).then(() => setReady(true))
  }, [filteredAlleles])

  const finalizeWorkflow = async (interpretationState: AnalysisInterpretationState) => {
    await API.FinalizeAnalysisInterpretation.post(
      analysisId,
      interpretationState,
      presentationAlleles,
      filteredAlleles,
    )
    await dispatch(fetchAnalysisByAnalysisId(analysisId))
    await dispatch(fetchAnalysisInterpretationsByAnalysisId(analysisId))
  }
  const changeWorkflowStatus = async (
    newWorkflowStatus: AnalysisInterpretation['workflowStatus'],
    interpretationState: InterpretationState,
  ) => {
    await API.ChangeAnalysisInterpretationWorkflowStatus.post(
      analysisId,
      interpretationState as AnalysisInterpretationState,
      newWorkflowStatus,
      presentationAlleles,
      filteredAlleles,
    )
    dispatch(fetchAnalysisByAnalysisId(analysisId))
    dispatch(fetchAnalysisInterpretationsByAnalysisId(analysisId))
  }

  // Interpretation log
  const deleteMessage = async (id: number) => {
    await API.AnalysisInterpreationLogs.del(analysisId, id)
    await dispatch(fetchAnalysisInterpretationLogForAnalysisId(analysisId))
  }

  const updateMessage = async (id: number, updatedMessage: string) => {
    await API.AnalysisInterpreationLogs.patch(analysisId, id, { message: updatedMessage })
    await dispatch(fetchAnalysisInterpretationLogForAnalysisId(analysisId))
  }

  const updateInterpretationLog = async (payload) => {
    await API.AnalysisInterpreationLogs.post(analysisId, payload)
    await dispatch(fetchAnalysisInterpretationLogForAnalysisId(analysisId))
  }

  const interpretationLogItems = useSelector((state: RootState) =>
    selectAnalysisInterpretationLogByAnalysisId(state, { analysisId }),
  )

  const start = async () => {
    await API.StartAnalysisInterpretation.post({
      analysisId,
    })
    await dispatch(fetchAnalysisByAnalysisId(analysisId))
    await dispatch(fetchAnalysisInterpretationsByAnalysisId(analysisId))
  }
  const reassignToMe = async () => {
    await API.ReassignAnalysisInterpretationToMe.post(analysisId)
    await dispatch(fetchAnalysisByAnalysisId(analysisId))
    await dispatch(fetchAnalysisInterpretationsByAnalysisId(analysisId))
  }

  const reopen = async () => {
    await API.ReopenAnalysisInterpretation.post(analysisId)
    await dispatch(fetchAnalysisByAnalysisId(analysisId))
    await dispatch(fetchAnalysisInterpretationsByAnalysisId(analysisId))
  }

  return {
    id: analysisId,
    analysis,
    samples,
    interpretation,
    presentationAlleles,
    filterConfigId,
    filteredAlleles,
    ready,
    reloadAlleles: loadAlleles,
    start,
    reassignToMe,
    reopen,
    changeWorkflowStatus,
    finalizeWorkflow,
    updateInterpretationLog,
    interpretationLogItems,
    updateMessage,
    deleteMessage,
  }
}

export function AnalysisInterpretationProvider({
  analysisId,
  children,
}: {
  analysisId: number
  children: ReactNode
}) {
  // We create and assign a ref in order to force HMR to keep the state of the context.
  // Once https://github.com/vitejs/vite/issues/3301 is resolved the ref and assignment can go away.
  const contextRef = useRef()

  // eslint-disable-next-line no-multi-assign
  analysisInterpretationContext = (contextRef.current as any) ??= createContext<
    DeepPartial<AnalysisInterpretationContext>
  >({})

  const context = useAnalysisInterpretationContext(analysisId)

  if (!context.ready) {
    return null
  }

  return (
    <analysisInterpretationContext.Provider value={context}>
      {children}
    </analysisInterpretationContext.Provider>
  )
}

export default function useAnalysisInterpretation(): AnalysisInterpretationContext {
  return useContext(
    analysisInterpretationContext ??
      (createContext<DeepPartial<AnalysisInterpretationContext>>(
        {},
      ) as unknown as Context<AnalysisInterpretationContext>),
  )
}
