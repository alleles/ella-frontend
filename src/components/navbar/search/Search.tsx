import { Popover } from '@headlessui/react'
import { MagnifyingGlassIcon } from '@heroicons/react/24/solid'
import React, { ReactNode, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import API from 'app/API'

import Input from 'components/elements/Input'
import { AsyncSelect } from 'components/elements/Select'
import Tab from 'components/elements/Tab'
import {
  SearchOptionGene,
  SearchOptionUser,
  SearchResults,
  mapFromApi,
} from 'components/navbar/search/SearchTypes'
import UserSelect from 'components/navbar/search/UserSelect'
import { fetchGeneOptions, shouldSearch } from 'components/navbar/search/searchLogic'
import AllelesTable from 'components/tables/AllelesTable'
import AnalysesTable from 'components/tables/AnalysesTable'
import FadeTransition from 'components/transitions/FadeTransition'

import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import { processAndStoreAnalysis } from 'store/interpretation/analysisSlice'

const searchTypes = ['analyses', 'alleles'] as const
type SearchType = 'analyses' | 'alleles'
const getSearchTypeKeyByTabIndex = (index: number): SearchType => searchTypes[index]

interface FormContainerProps {
  children: ReactNode
}

function FormContainer({ children }: FormContainerProps) {
  return (
    <div className="w-full rounded-lg shadow-lg">
      <div className="rounded bg-white px-6 pb-6">
        <div>{children}</div>
      </div>
    </div>
  )
}

interface AllelesSearchFormProps {
  searchText: string
  setSearchText: (newValue: string) => void
  setGene
  gene
  setUser
}

function AllelesSearchForm({
  gene,
  searchText,
  setGene,
  setSearchText,
  setUser,
}: AllelesSearchFormProps) {
  return (
    <div className="grid grid-cols-2 items-start gap-4 pt-5">
      <div className="col-span-2 flex max-w-lg rounded-md shadow-sm">
        <Input
          label="HGVS/Genomic"
          type="text"
          name="analysisText"
          id="analysisText"
          value={searchText}
          onChange={(ev) => {
            setSearchText(ev.target.value)
          }}
        />
      </div>
      <div className="max-w-lg ">
        <AsyncSelect
          onChange={(v) => {
            setGene(v)
          }}
          loadOptions={fetchGeneOptions}
          placeholder="Search gene"
          selected={gene}
        />
      </div>
      <div className="max-w-lg ">
        <UserSelect onChange={(v) => setUser(v)} />
      </div>
    </div>
  )
}

interface AnalysesSearchFormProps {
  searchText: string
  setSearchText: (newValue: string) => void
  setUser
}

function AnalysesSearchForm({
  searchText,
  setSearchText,
  setUser,
}: AnalysesSearchFormProps) {
  return (
    <div className="grid grid-cols-2 items-start gap-4 pt-5">
      <div className="col-span-2 flex max-w-lg rounded-md shadow-sm">
        <Input
          label="Analysis name"
          type="text"
          name="analysisText"
          id="analysisText"
          value={searchText}
          onChange={(ev) => {
            setSearchText(ev.target.value)
          }}
        />
      </div>

      <div className="max-w-lg">
        <UserSelect onChange={(v) => setUser(v)} />
      </div>
    </div>
  )
}

export default function Search() {
  const [searchType, setSearchType] = useState<SearchType>('analyses')
  const [searchText, setSearchText] = useState('')
  const [gene, setGene] = useState<SearchOptionGene | null>(null)
  const [user, setUser] = useState<SearchOptionUser | null>(null)
  const [results, setResults] = useState<SearchResults>({
    alleles: [],
    analyses: [],
  })

  const resetForm = () => {
    setSearchText('')
    setGene(null)
    setUser(null)
    setResults({ alleles: [], analyses: [] })
  }
  const dispatch = useDispatch()

  // TODO: Remove unecessary useEffect. Could be a simple function.
  useEffect(() => {
    if (shouldSearch({ gene, searchText, searchType, user })) {
      API.SearchAllelesAndAnalyses.get(searchText, gene, user, searchType)
        .then((response) => response.data)
        .then((searchResult) => {
          searchResult.alleles.map((allele) =>
            dispatch(processAndStoreAllele(allele.allele)),
          )
          searchResult.analyses.map((analysis) =>
            dispatch(processAndStoreAnalysis(analysis)),
          )
          return mapFromApi(searchResult)
        })
        .then((searchResult) => setResults(searchResult))
    }
  }, [searchText, gene, user, searchType])

  return (
    <div className="w-4 px-4">
      <Popover className="relative">
        {({ open }) => (
          <>
            <Popover.Button>
              <MagnifyingGlassIcon
                className="mt-2 h-5 w-5 text-white"
                aria-hidden="true"
              />
            </Popover.Button>
            {open && (
              <FadeTransition>
                <Popover.Panel className="absolute z-10 mt-3 w-screen max-w-4xl rounded px-0 ring-1 ring-black/5">
                  <FormContainer>
                    <Tab.Group
                      onChange={(index) => {
                        setSearchType(getSearchTypeKeyByTabIndex(index))
                        resetForm()
                      }}
                      defaultIndex={0}
                    >
                      <Tab.List>
                        <Tab>Analyses</Tab>
                        <Tab>Variants</Tab>
                      </Tab.List>
                      <Tab.Panels>
                        <Tab.Panel>
                          <AnalysesSearchForm
                            searchText={searchText}
                            setSearchText={setSearchText}
                            setUser={setUser}
                          />
                          <div className="mt-6">
                            <AnalysesTable overviewAnalyses={results.analyses} />
                          </div>
                        </Tab.Panel>
                        <Tab.Panel>
                          <>
                            <AllelesSearchForm
                              searchText={searchText}
                              setSearchText={setSearchText}
                              setGene={setGene}
                              gene={gene}
                              setUser={setUser}
                            />
                            <div className="mt-6">
                              <AllelesTable alleles={results.alleles} />
                            </div>
                          </>
                        </Tab.Panel>
                      </Tab.Panels>
                    </Tab.Group>
                  </FormContainer>
                </Popover.Panel>
              </FadeTransition>
            )}
          </>
        )}
      </Popover>
    </div>
  )
}
