import alleleReducer, {
  processAndStoreAllele,
  selectAlleleById,
} from 'store/interpretation/alleleSlice'
import { store } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Allele } from 'types/store/Allele'

function buildAllele(override: Partial<Allele>): Allele {
  return {
    callerType: '',
    changeFrom: '',
    changeTo: '',
    changeType: '',
    chromosome: '',
    genomeReference: 'GRCh37',
    length: 0,
    openEndPosition: 0,
    startPosition: 0,
    vcfAlt: '',
    vcfPos: 0,
    vcfRef: '',
    ...override,
  }
}

describe('allele reducer', () => {
  it('should handle initial state', () => {
    expect(alleleReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching alleles from the backend', async () => {
    const genomeRef: 'GRCh37' | 'GRCh38' = 'GRCh37'
    const inputAllele = {
      // REMOVED
      allele_assessment: {} as ApiTypes.AlleleAssessment,
      allele_report: {} as ApiTypes.AlleleReport,
      annotation: {} as ApiTypes.Annotation,
      id: 1,
      interpretations: 'REMOVED',
      reference_assessments: [],
      tags: ['REMOVED'],
      warnings: {} as ApiTypes.Warnings,
      // KEPT
      caller_type: 'string',
      change_from: 'string',
      change_to: 'string',
      change_type: 'string',
      chromosome: 'string',
      genome_reference: genomeRef,
      length: 1,
      open_end_position: 124,
      start_position: 123,
      vcf_pos: 123,
      vcf_ref: 'string',
      vcf_alt: 'string',
    }

    const dispatch = jest.fn()
    await processAndStoreAllele(inputAllele)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        // Cleans out unwanted
        payload: {
          '1': {
            callerType: 'string',
            changeFrom: 'string',
            changeTo: 'string',
            changeType: 'string',
            chromosome: 'string',
            genomeReference: 'GRCh37',
            length: 1,
            openEndPosition: 124,
            startPosition: 123,
            vcfAlt: 'string',
            vcfPos: 123,
            vcfRef: 'string',
          },
        },
      }),
    )
  })

  it('should handle getting allele by id', async () => {
    const state = {
      ...store.getState(),
      alleles: {
        1: buildAllele({ vcfPos: 100 }),
        2: buildAllele({ vcfPos: 200 }),
      },
    }

    expect(selectAlleleById(state, { alleleId: 1 })).toEqual(buildAllele({ vcfPos: 100 }))
    expect(selectAlleleById(state, { alleleId: 2 })).toEqual(buildAllele({ vcfPos: 200 }))
    expect(selectAlleleById(state, { alleleId: 3 })).toEqual(undefined)
  })
})
