import './plugin/references'
import './plugin/signature'

/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { Editor as TinyMCE } from '@tinymce/tinymce-react'
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'

/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import tinymce from 'tinymce'
import 'tinymce/icons/default'
import 'tinymce/models/dom/model'
import 'tinymce/plugins/advlist'
import 'tinymce/plugins/anchor'
import 'tinymce/plugins/autolink'
import 'tinymce/plugins/charmap'
import 'tinymce/plugins/code'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/help'
import 'tinymce/plugins/image'
import 'tinymce/plugins/insertdatetime'
import 'tinymce/plugins/link'
import 'tinymce/plugins/lists'
import 'tinymce/plugins/media'
import 'tinymce/plugins/preview'
import 'tinymce/plugins/searchreplace'
import 'tinymce/plugins/table'
import 'tinymce/plugins/template'
import 'tinymce/plugins/visualblocks'
import 'tinymce/plugins/wordcount'
import 'tinymce/skins/ui/oxide/skin.min.css'
import 'tinymce/themes/silver'

import generateEditorConfig from 'components/editor/config'

import { selectReferencesByAnnotationId } from 'store/interpretation/annotationSlice'
import { selectReferencesByCustomAnnotationId } from 'store/interpretation/customAnnotationSlice'
import { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import { CommentTemplates } from 'types/api/Pydantic'
import { User } from 'types/store/User'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

interface EditorProps {
  id?: string
  readOnly: boolean
  templates?: CommentTemplates[]
  placeholder?: string
  initialValue?: string
  onBlur?: (a: any) => void
  onFocus?: (a: any) => void
  collapsed?: boolean
  setCollapsed?: (a: any) => void
}

export default function Editor({
  id,
  readOnly = false,
  templates,
  placeholder = 'Comment',
  initialValue,
  onBlur,
  onFocus,
  collapsed = false,
  setCollapsed,
}: EditorProps) {
  const [isEmpty, setIsEmpty] = useState<boolean>(initialValue === '')
  const userData = useSelector(selectCurrentUser) as User
  const toolbarContainer = `${id}-editor-toolbar`

  const ref = useRef<any>(null)

  const saveOnBlur = () => {
    if (onBlur && ref) {
      ref.current.editor.setDirty(false)
      onBlur(ref.current.editor.getContent())
    }
    if (ref) setIsEmpty(ref.current.editor.getContent({ format: 'text' }) === '')
  }

  const { selectedAlleleId } = useInterpretationStateContext()
  const { presentationAlleles } = useInterpretation()
  const { annotationId, customAnnotationId } = presentationAlleles[selectedAlleleId]
  const annotationReferences = useSelector(
    annotationId
      ? (state: RootState) =>
          selectReferencesByAnnotationId(state, {
            annotationId,
          })
      : () => [],
  )
  const customAnnotationReferences = useSelector(
    customAnnotationId
      ? (state: RootState) =>
          selectReferencesByCustomAnnotationId(state, {
            customAnnotationId,
          })
      : () => [],
  )

  const references = new Set([...customAnnotationReferences, ...annotationReferences])

  useEffect(() => {
    if (
      ref &&
      ref.current.editor !== undefined &&
      ref.current.editor.bodyElement !== undefined
    ) {
      // we also need to check for empty, otherwise entered value will persist when
      // changing allele
      setIsEmpty(ref.current.editor.getContent({ format: 'text' }) === '')
      // we need to add placeholder label to fake its presence in tinymce inline
      // editor
      ref.current.editor.bodyElement.setAttribute('aria-placeholder', placeholder)
    }
  }, [ref?.current, initialValue])

  return (
    <div id={`${id}-editor`} className="relative h-full flex-col">
      <div id={`${toolbarContainer}`} className="relative" />
      <div
        onMouseDown={() => {
          if (collapsed && !readOnly && setCollapsed) setCollapsed(false)
        }}
        id={`${id}-editor-content`}
        className={`${collapsed ? 'line-clamp-3' : ''} ${
          isEmpty ? 'no-content' : ''
        } border border-ellagray p-2`}
      >
        <TinyMCE
          ref={ref}
          initialValue={initialValue}
          onBlur={saveOnBlur}
          onFocus={onFocus}
          init={generateEditorConfig(
            templates,
            userData.username,
            placeholder,
            toolbarContainer,
            references,
          )}
          disabled={readOnly}
          onInit={() => {
            setIsEmpty(ref.current.editor.getContent({ format: 'text' }) === '')
            ref.current.editor.bodyElement.setAttribute('aria-placeholder', placeholder)
          }}
        />
      </div>
    </div>
  )
}
