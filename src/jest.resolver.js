module.exports = (path, options) =>
  // Call the defaultResolver
  options.defaultResolver(path, {
    ...options,
    packageFilter: (pkg) => {
      // This is a workaround for https://github.com/uuidjs/uuid/pull/616
      if (pkg.name === 'uuid') {
        // eslint-disable-next-line no-param-reassign
        delete pkg.exports
        // eslint-disable-next-line no-param-reassign
        delete pkg.module
      }
      return pkg
    },
  })
