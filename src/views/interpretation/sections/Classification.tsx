import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import Button from 'components/elements/Button'
import ConfirmableAction from 'components/elements/ConfirmableAction'
import SectionBox from 'components/elements/SectionBox'

import { selectRichTextTemplates } from 'store/config/configSlice'
import { selectAlleleReportById } from 'store/interpretation/alleleReportSlice'
import { RootState } from 'store/store'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import ClassificationDropdown from 'views/interpretation/components/ClassificationDropdown'
import Acmg from 'views/interpretation/components/acmg/Acmg'
import AcmgDropdown from 'views/interpretation/components/acmg/AcmgDropdown'

export default function Classification() {
  const [suggestedAcmgClass, setSuggestedAcmgClass] = useState('')
  const {
    alleleState,
    setAlleleState,
    readOnly,
    selectedAlleleId,
    submitClassification,
    submittedClassification,
    submitReport,
  } = useInterpretationStateContext()

  const { presentationAlleles, reloadAlleles } = useInterpretation()

  const [evaluationComment, setEvaluationComment] = useState(
    alleleState?.alleleAssessment.evaluation.classification.comment,
  )
  const onEvaluationChange = (value: string) => {
    setAlleleState({
      alleleAssessment: {
        evaluation: {
          classification: {
            comment: value,
          },
        },
      },
    })
  }

  const [reportComment, setReportComment] = useState(alleleState?.alleleReport.comment)
  const onReportChange = (value: string) => {
    setAlleleState({
      alleleReport: {
        comment: value,
      },
    })
  }

  useEffect(() => {
    setEvaluationComment(alleleState?.alleleAssessment.evaluation.classification.comment)
    setReportComment(alleleState?.alleleReport.comment)
  }, [selectedAlleleId])

  const storeAlleleReport = useSelector((state: RootState) =>
    alleleState.alleleReportId
      ? selectAlleleReportById(state, { alleleReportId: alleleState.alleleReportId })
      : undefined,
  )

  const reportDirty = Boolean(
    alleleState.alleleReportId &&
      storeAlleleReport?.evaluation.comment !== alleleState.alleleReport.comment,
  )

  const [triggerUndo, setTriggerUndo] = useState(0)

  return (
    <SectionBox title="Classification" color="ellapurple" initCollapsed={false}>
      <div className="flex justify-end gap-x-2 p-2">
        {reportDirty ? (
          <>
            <div className="shrink">
              <Button
                onClick={async () => {
                  await submitReport(presentationAlleles[selectedAlleleId])
                  await reloadAlleles([selectedAlleleId])
                }}
              >
                Submit Report
              </Button>
            </div>
            <div className="shrink">
              <ConfirmableAction
                confirmLabel="Confirm Undo"
                cancelLabel="Cancel Undo"
                onConfirm={() => {
                  setTriggerUndo(triggerUndo + 1)
                  setAlleleState({
                    alleleReport: {
                      comment: storeAlleleReport?.evaluation.comment,
                    },
                  })
                }}
              >
                Undo report changes
              </ConfirmableAction>
            </div>
          </>
        ) : null}
        {!(readOnly || submittedClassification) ? <AcmgDropdown /> : null}
        <ClassificationDropdown disabled={readOnly || submittedClassification} />
        {!(readOnly || submittedClassification) ? (
          <div className="flex w-20">
            <Button
              disabled={!alleleState?.alleleAssessment.classification}
              onClick={async () => {
                await submitClassification(presentationAlleles[selectedAlleleId])
                await reloadAlleles([selectedAlleleId])
              }}
            >
              Submit
            </Button>
          </div>
        ) : null}
      </div>
      <div className="comment-field-label ml-1 italic text-ellagray-700">Evaluation</div>
      <Editor
        key={`classification-evaluation-comment-${selectedAlleleId}`}
        id="evaluation-comment"
        placeholder="Evaluation"
        initialValue={evaluationComment}
        onBlur={onEvaluationChange}
        templates={useSelector(selectRichTextTemplates('classificationEvaluation'))}
        readOnly={readOnly || submittedClassification}
      />
      <div className="comment-field-label ml-1 italic text-ellagray-700">Report</div>
      <Editor
        key={`classification-report-comment-${selectedAlleleId}-${triggerUndo}`}
        id="classification-report-comment"
        placeholder="Report"
        onBlur={onReportChange}
        initialValue={reportComment}
        templates={useSelector(selectRichTextTemplates('classificationReport'))}
        readOnly={readOnly}
      />
      <div className="pt-6 text-xl">ACMG</div>
      <div className="mt-2 inline-flex rounded bg-ellagray-100 py-2 px-3">
        Added criteria - suggested class: {suggestedAcmgClass}
      </div>
      <Acmg
        onSuggestedClassChange={(newClass) => setSuggestedAcmgClass(newClass)}
        disabled={readOnly}
      />
    </SectionBox>
  )
}
