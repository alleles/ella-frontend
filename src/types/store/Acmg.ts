import * as ApiTypes from 'types/api/Pydantic'

export type AcmgType = 'benign' | 'pathogenic' | 'other'
export const acmgTypes: AcmgType[] = ['benign', 'pathogenic', 'other']
export type AcmgStrength = 'pn' | 'pv' | 'pp' | 'pm' | 'ps' | 'bp' | 'bs' | 'ba'

export interface AcmgCode {
  code: string
  source: string | null
  value: string[]
  match: string[]
  op: string
  comment: string
  uuid: string
  [k: string]: unknown
}

export function mapFromApi(
  acmgCodeFromApi: ApiTypes.ACMGCode | ApiTypes.IncludedItem | ApiTypes.SuggestedItem,
): AcmgCode {
  return {
    comment: '',
    match: [],
    op: '',
    source: null,
    uuid: '',
    value: [],
    ...acmgCodeFromApi,
  }
}

export function mapToApiIncludedItem(
  normalizedAcmgCode: AcmgCode,
): ApiTypes.IncludedItem {
  return {
    code: normalizedAcmgCode.code,
    match: normalizedAcmgCode.match,
    op: normalizedAcmgCode.op,
    source: normalizedAcmgCode.source ?? undefined,
    uuid: normalizedAcmgCode.uuid,
    comment: normalizedAcmgCode.comment,
  }
}

export function mapToApiSuggestedItem(
  normalizedAcmgCode: AcmgCode,
): ApiTypes.SuggestedItem {
  return {
    code: normalizedAcmgCode.code,
    match: normalizedAcmgCode.match,
    op: normalizedAcmgCode.op,
    source: normalizedAcmgCode.source ?? undefined,
  }
}

export function mapToApiACMGCode(normalizedAcmgCode: AcmgCode): ApiTypes.ACMGCode {
  return {
    code: normalizedAcmgCode.code,
    match: normalizedAcmgCode.match,
    op: normalizedAcmgCode.op,
    source: normalizedAcmgCode.source ?? '',
    value: normalizedAcmgCode.value,
  }
}
