export function sortByPriority<T extends { priority: number }>(x: T[]): T[] {
  return [...x].sort(({ priority: p1 }, { priority: p2 }) => p2 - p1)
}
