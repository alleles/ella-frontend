import requests
from behave import given, when

STATE_API_URL = "http://backend:23232"


@given('state "{state}"')
def load_state(context, state: str):
    res = requests.post(f"{STATE_API_URL}/database/reset", data={"testset": state})
    assert res.status_code == 200, "Failed to load state"


@when('I save the state as "{state}"')
def save_state(context, state: str):
    res = requests.post(f"{STATE_API_URL}/database/dump", data={"testset": state})
    assert res.status_code == 200, "Failed to save state"
