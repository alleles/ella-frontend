Feature: Analysis workflow

  Background:
    Given state "test-bdd"

  Scenario: Start and finsh analysis in all workflow steps
    Given I am logged in as "testuser1"
    When I set "analysis" to "brca_sample_1.HBOCUTV_v01"

    # For each step, check that analysis is listed under the correct Overview section and possible to start and finish.
    # Also tests reassigning and that links on Overview page work ("When I press"). #

    ## Default: In "Interpretation"
    Then I should see "$analysis" in "Interpretation"

    ## After start: In "Your analyses"
    When I view the analysis with name "$analysis"
    And I start the interpretation
    And I go to analyses overview
    Then I should see "$analysis" in "Your analyses"

    ## For testuser2: In "Others' analyses"
    Given I am logged in as "testuser2"
    When I set "analysis" to "brca_sample_1.HBOCUTV_v01"
    Then I should see "$analysis" in "Others' analyses"

    ## testuser2 can reassign analysis
    When I view the analysis with name "$analysis"
    And I reassign to me

    ## Reassign and continue with testuser1
    Given I am logged in as "testuser1"
    When I view the analysis with name "$analysis"
    And I reassign to me

    ## In "Pending review"
    When I finish "Interpretation" and send to "Review"
    Then I should see "$analysis" in "Pending review"

    ## In "Pending medical review"
    When I press "$analysis"
    And I start "Review" and send to "Medical review"
    Then I should see "$analysis" in "Pending medical review"

    ## In "Not ready"
    When I press "$analysis"
    And I start "Medical review" and send to "Not ready"
    Then I should see "$analysis" in "Not ready"

    ## Classify all variants and finalize analysis; In "Finalized"
    When I press "$analysis"
    And I start "Not ready"
    ### c.10G>T
    And I submit the class as "Class 1"
    ### c.51_52del
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 2"
    ### c.67+2T>A
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 3"
    ### c.72A>T
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 4"
    ### c.97G>T
    And I select the allele row 0 from the allele table
    And I submit the class as "Class 5"
    ### c.198A>G
    And I select the allele row 0 from the allele table
    And I submit the class as "Not provided"
    ### Can finalize analysis
    And I finish "Not ready" and send to "Finalized"
    Then I should see "$analysis" in "Finalized"

    ## testuser2 can reopen finalized analysis
    Given I am logged in as "testuser2"
    When I view the analysis with name "$analysis"
    And I press "Reopen"
    And I start "Not ready"

  # TODO: Test that allele interpretation can be changed (pending #189)

  Scenario: Add comment to each section on DETAILS page and verify with other user
    Given I am logged in as "testuser1"
    When I view the analysis with id 4

    # Verify comment fields are read-only before start
    Then I should see an element with xpath "//div[@id='evaluation-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='classification-report-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='analysis-specific-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='region-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='frequency-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='prediction-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='external-comment-editor-content']//div[@contenteditable='false']"
    And I should see an element with xpath "//div[@id='references-comment-editor-content']//div[@contenteditable='false']"

    # Add a comment to each section for c.97G>T (allele id 852)
    When I start the interpretation
    And I select the allele row 4 from the allele table
    And I type "Test Evaluation comment" into the editor with xpath "//div[@id='evaluation-comment-editor-content']"
    And I type "Test Report comment" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I type "Test Analysis specific comment" into the editor with xpath "//div[@id='analysis-specific-comment-editor-content']"
    And I type "Test Region comment" into the editor with xpath "//div[@id='region-comment-editor-content']"
    And I type "Test Frequency comment" into the editor with xpath "//div[@id='frequency-comment-editor-content']"
    And I type "Test Prediction comment" into the editor with xpath "//div[@id='prediction-comment-editor-content']"
    And I type "Test External comment" into the editor with xpath "//div[@id='external-comment-editor-content']"
    And I type "Test References comment" into the editor with xpath "//div[@id='references-comment-editor-content']"

    # Check that comment is stored in interpretation round to the correct allele
    And I select the allele row 0 from the allele table
    Then I should not see "Test Evaluation comment"
    And I should not see "Test Report comment"
    And I should not see "Test Analysis specific comment"
    And I should not see "Test Region comment"
    And I should not see "Test Frequency comment"
    And I should not see "Test Prediction comment"
    And I should not see "Test External comment"
    And I should not see "Test References comment"

    When I select the allele row 4 from the allele table
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    And I should see "Test Analysis specific comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

    # Check that comment is visible to other user for correct allele only
    When I save the interpretation

    Given I am logged in as "testuser2"
    When I view the analysis with id 4
    And I select the allele row 4 from the allele table
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    And I should see "Test Analysis specific comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

    When I select the allele row 0 from the allele table
    Then I should not see "Test Evaluation comment"
    And I should not see "Test Report comment"
    And I should not see "Test Analysis specific comment"
    And I should not see "Test Region comment"
    And I should not see "Test Frequency comment"
    And I should not see "Test Prediction comment"
    And I should not see "Test External comment"
    And I should not see "Test References comment"

    # Check that comment is visible for the stand-alone allele (c.97G>T = id 852, submitted class)
    Given I am logged in as "testuser1"
    When I select the allele row 4 from the allele table
    And I submit the class as "Class 3"

    Given I am logged in as "testuser2"
    When I view the allele with id 852
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    # Because this is analysis specific ..
    And I should not see "Test Analysis specific comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

    # Check that comment is visible in another analysis with same variant (submitted class)
    Given I am logged in as "testuser2"
    When I view the analysis with id 10
    And I select the "Classified" tab
    And I select the allele row 0 from the allele table
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
    # Analysis specific comments should NOT be visible in other analyses
    And I should not see "Test Analysis specific comment"
    And I should see "Test Region comment"
    And I should see "Test Frequency comment"
    And I should see "Test Prediction comment"
    And I should see "Test External comment"
    And I should see "Test References comment"

  Scenario: Edit a Report comment for a submitted, still valid classification

    # Submit c.97G>T (allele id 852) with a Report comment
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    And I start the interpretation
    And I select the allele row 4 from the allele table
    And I type "Test Report comment" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I submit the class as "Class 3"

    Given I am logged in as "testuser2"
    When I view the analysis with id 10
    And I start the interpretation
    And I select the "Classified" tab
    And I select the allele row 0 from the allele table
    And I type " edited" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    And I press "Submit Report"
    Then I should see "Test Report comment edited"

    # Check that edits are visible for other user in other analysis
    Given I am logged in as "testuser1"
    When I reload
    And I select the "Classified" tab
    And I select the allele row 0 from the allele table
    Then I should see "Test Report comment edited"

# TODO: Add and check further changes to Prediction and Reference sections
