import React, { MouseEvent, ReactNode } from 'react'

interface Props {
  children: ReactNode
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void
  type?: 'button' | 'submit' | 'reset'
  color?: 'primary' | 'danger' | 'cancel'
  disabled?: boolean
}

export default function Button({
  children,
  onClick,
  type = 'button',
  color = 'primary',
  disabled = false,
}: Props) {
  let colorClass
  switch (color) {
    case 'danger':
      colorClass =
        'text-white bg-ellared-dark shadow-sm hover:shadow-md hover:bg-ellared focus:ring-ellared-light'
      break

    case 'cancel':
      colorClass =
        'text-ellagray-700 bg-ellagray shadow-sm hover:shadow-md hover:bg-ellagray-400 hover:text-ellagray-900 focus:ring-ellagray-500'
      break
    default:
      colorClass =
        'text-white bg-ellablue-darkest shadow-sm hover:shadow-md hover:bg-ellablue-darker focus:ring-ellablue-lighter'
      break
  }

  return (
    <button
      className={`flex w-full justify-center whitespace-nowrap rounded-md py-2 px-4 text-sm font-medium tracking-wide focus:outline-none ${
        disabled
          ? 'cursor-default bg-ellagray-100 text-ellagray'
          : `${colorClass} focus:ring-2`
      }`}
      onClick={(event) => {
        if (!disabled && onClick) {
          onClick(event)
        }
      }}
      type={type}
    >
      {children}
    </button>
  )
}
