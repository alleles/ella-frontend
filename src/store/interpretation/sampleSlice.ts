import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Sample, mapFromApi } from 'types/store/Sample'

const initialState: Record<number, Sample> = {}

const processAndStoreSample = createAsyncThunk<Record<number, Sample>, ApiTypes.Sample>(
  'sample/processAndStoreSample',
  async (apiSample) => ({
    [apiSample.id]: mapFromApi(apiSample),
  }),
)

const SampleSlice = createSlice({
  name: 'Sample',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(processAndStoreSample.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectSampleById = (
  state: RootState,
  { sampleId }: { sampleId: number },
): Sample | undefined => state.samples[sampleId]

const selectSamplesByAnalysisId = (
  state: RootState,
  { analysisId }: { analysisId: number },
): Record<number, Sample> => {
  const analysis = state.analyses[analysisId]
  if (!analysis) {
    return []
  }
  return analysis.sampleIds.reduce(
    (curr, sampleId) => ({
      ...curr,
      [sampleId]: state.samples[sampleId],
    }),
    {},
  )
}

export { processAndStoreSample, selectSampleById, selectSamplesByAnalysisId }

export default SampleSlice.reducer
