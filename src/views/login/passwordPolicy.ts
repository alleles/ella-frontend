import * as yup from 'yup'

interface PasswordConfig {
  password_minimum_length: number
  password_match_groups_descr: string[]
  password_match_groups: string[]
  password_num_match_groups: number
}
// Build a yup validator from a password policy config
const buildValidatorFromPasswordPolicy = (config: PasswordConfig) => {
  const {
    password_minimum_length,
    password_match_groups_descr,
    password_match_groups,
    password_num_match_groups,
  } = config

  const validators = password_match_groups.map((regexp, i) =>
    yup.string().label(password_match_groups_descr[i]).matches(RegExp(regexp)),
  )

  return yup
    .string()
    .min(
      password_minimum_length,
      `Password must be at least ${password_minimum_length} characters long`,
    )
    .test(
      'regexps',
      `Password must meet at least ${password_num_match_groups} password policies`,
      (value) => {
        const valid = validators.filter((validator) => validator.isValidSync(value))
        return valid.length >= password_num_match_groups
      },
    )
}

export default buildValidatorFromPasswordPolicy
