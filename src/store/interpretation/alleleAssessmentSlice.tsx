import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import { fetchAttachments } from 'store/interpretation/attachmentSlice'
import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleAssessment, mapFromApi } from 'types/store/AlleleAssessment'

import { compareStrings } from 'utils/general'

const initialState: Record<number, AlleleAssessment> = {}

const fetchAlleleAssessmentForAlleleId = createAsyncThunk<
  Record<number, AlleleAssessment>,
  number
>(
  'alleleassessments/fetchAlleleAssessmentsForAlleleId',
  async (alleleId, { dispatch }) => {
    const apiAlleleAssessments = await API.AlleleAssessments.get(alleleId).then(
      (result) => result.data,
    )
    const alleleAssessments = apiAlleleAssessments.reduce(
      (prev, current) => ({
        ...prev,
        [current.id]: mapFromApi(current),
      }),
      {} as Record<number, AlleleAssessment>,
    )

    const attachmentIds = Object.values(alleleAssessments).flatMap(
      (assessment) => assessment.attachmentIds,
    )
    await dispatch(fetchAttachments(attachmentIds))
    return alleleAssessments
  },
)

export const processAndStoreAlleleAssessment = createAsyncThunk<
  Record<number, AlleleAssessment>,
  ApiTypes.AlleleAssessment
>('alleleAssessment/processAndStoreAlleleAssessment', (alleleAssessment) => ({
  [alleleAssessment.id]: mapFromApi(alleleAssessment),
}))

const alleleAssessmentSlice = createSlice({
  name: 'alleleAssessments',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchAlleleAssessmentForAlleleId.fulfilled, (state, action) => {
        Object.assign(state, action.payload)
      })
      .addCase(processAndStoreAlleleAssessment.fulfilled, (state, action) => {
        Object.assign(state, action.payload)
      })
  },
})

const selectAlleleAssessmentById = (
  state: RootState,
  { assessmentId }: { assessmentId: number },
): AlleleAssessment | undefined => {
  const alleleAssesment = state.alleleAssessments[assessmentId]
  return alleleAssesment
}

const selectAlleleAssessmentsByIds = (
  state: RootState,
  { alleleAssessmentIds }: { alleleAssessmentIds: number[] },
) =>
  alleleAssessmentIds.reduce((acc, assessmentId) => {
    acc[assessmentId] = selectAlleleAssessmentById(state, { assessmentId })
    return acc
  }, {} as Record<number, AlleleAssessment | undefined>)

const selectAlleleAssessmentsByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
): AlleleAssessment[] =>
  Object.values(state.alleleAssessments).filter((a) => a.alleleId === alleleId)

const selectAlleleAssessmentIsOutdated = createSelector(
  (state: RootState) => state.config.classification.options,
  selectAlleleAssessmentById,
  (classificationOptions, alleleAssesment): boolean => {
    if (alleleAssesment === undefined) {
      return false
    }
    const classificationValue = alleleAssesment.classification

    if (!classificationValue) {
      return false
    }

    const classificationOption = classificationOptions.find(
      (c) => c.value === classificationValue,
    )

    const outdatedAfterDays = classificationOption?.outdated_after_days
    if (!outdatedAfterDays) {
      return false
    }

    const now = new Date()
    const dateCreated = new Date(alleleAssesment.dateCreated)
    const daysSinceUpdate = Math.round(
      (now.valueOf() - dateCreated.valueOf()) / (1000 * 60 * 60 * 24),
    )

    return daysSinceUpdate >= outdatedAfterDays
  },
)

const selectLatestAlleleAssessmentByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
) =>
  selectAlleleAssessmentsByAlleleId(state, { alleleId }).sort(
    (a, b) => compareStrings(a.dateCreated, b.dateCreated) * -1,
  )[0] // descending order + select first

export {
  fetchAlleleAssessmentForAlleleId,
  selectAlleleAssessmentById,
  selectAlleleAssessmentsByIds,
  selectAlleleAssessmentsByAlleleId,
  selectAlleleAssessmentIsOutdated,
  selectLatestAlleleAssessmentByAlleleId,
}

export default alleleAssessmentSlice.reducer
