import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import attachmentReducer, {
  fetchAttachments,
  selectAttachmentById,
} from 'store/interpretation/attachmentSlice'
import { store } from 'store/store'

import { Attachment } from 'types/store/Attachment'

const mock = new MockAdapter(axios)

function buildAttachment(override: Partial<Attachment>): Attachment {
  return {
    filename: '',
    size: 0,
    userId: 0,
    extension: '',
    dateCreated: '1970-01-01',
    mimeType: '',
    thumbnail: null,
    ...override,
  }
}

describe('attachment reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(attachmentReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching attachments from the backend', async () => {
    mock.onGet(API.Attachments.path).reply(200, [
      {
        id: 1,
        sha256: '1234567',
        filename: 'foo.txt',
        size: 512,
        date_created: '2021-11-22',
        mimetype: 'text/plain',
        extension: 'txt',
        thumbnail: 'base64 encoded',
        user: {
          id: 2,
          first_name: 'dabla',
        },
      },
    ])

    const dispatch = jest.fn()
    await fetchAttachments([1])(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining<{ payload: Record<number, Attachment> }>({
        payload: {
          1: {
            filename: 'foo.txt',
            size: 512,
            dateCreated: '2021-11-22',
            mimeType: 'text/plain',
            extension: 'txt',
            thumbnail: 'base64 encoded',
            userId: 2,
          },
        },
      }),
    )
  })

  it('should handle getting attachment by id', async () => {
    const state = {
      ...store.getState(),
      attachments: {
        1: buildAttachment({ filename: 'bar' }),
        2: buildAttachment({ filename: 'dabla' }),
      },
    }

    expect(selectAttachmentById(state, 1)).toEqual(
      buildAttachment({
        filename: 'bar',
      }),
    )
    expect(selectAttachmentById(state, 2)).toEqual(
      buildAttachment({
        filename: 'dabla',
      }),
    )
    expect(selectAttachmentById(state, 3)).toEqual(undefined)
  })
})
