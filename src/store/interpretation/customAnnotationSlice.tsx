import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import {
  fetchReferences,
  selectReferencesByIds,
} from 'store/interpretation/referenceSlice'
import type { RootState } from 'store/store'

import { CustomAnnotation, mapFromApi } from 'types/store/CustomAnnotation'
import { Reference } from 'types/store/Reference'

const initialState: Record<number, CustomAnnotation> = {}
const fetchCustomAnnotationsForAlleleId = createAsyncThunk<
  Record<number, CustomAnnotation>,
  number
>(
  'customAnnotations/fetchCustomAnnotationsForAlleleId',
  async (alleleId, { dispatch }) => {
    const apiCustomAnnotations = await API.CustomAnnotations.get(alleleId).then(
      (result) => result.data,
    )

    const customAnnotations = apiCustomAnnotations.reduce(
      (prev, curr) => ({
        ...prev,
        [curr.id]: mapFromApi(curr),
      }),
      {} as Record<number, CustomAnnotation>,
    )

    const referenceIds: Set<number> = new Set()
    Object.values(customAnnotations).forEach((customAnnotation) => {
      if (customAnnotation.annotations?.references) {
        customAnnotation.annotations.references.forEach((reference) => {
          referenceIds.add(reference.id)
        })
      }
    })

    dispatch(fetchReferences({ ids: [...referenceIds], pubMedIds: [] }))

    return customAnnotations
  },
)

const customAnnotationsSlice = createSlice({
  name: 'customAnnotations',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCustomAnnotationsForAlleleId.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectCustomAnnotationsByAnnotationId = (
  state: RootState,
  { customAnnotationId }: { customAnnotationId: number },
) => state.customAnnotations[customAnnotationId]

const selectCustomAnnotationsByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
) => state.customAnnotations[alleleId]

const selectLatestCustomAnnotationIdForAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
): number | null =>
  Object.entries(state.customAnnotations)
    .filter(([, an]) => an.alleleId === alleleId && an.dateSuperseded === null)
    .map(([id]) => parseInt(id, 10))
    .at(0) ?? null

const selectLatestCustomAnnotationIdsForAlleleIds = (
  state: RootState,
  { alleleIds }: { alleleIds: number[] },
) =>
  alleleIds.reduce(
    (prev, curr) => ({
      ...prev,
      [curr]: selectLatestCustomAnnotationIdForAlleleId(state, { alleleId: curr }),
    }),
    {} as Record<number, number | null>,
  )

const selectReferencesByCustomAnnotationId = createSelector(
  (state: RootState) => state,
  selectCustomAnnotationsByAnnotationId,
  (state: RootState, annotation): Reference[] => {
    const referenceIds =
      annotation?.annotations.references?.reduce(
        (previous, current) => [...previous, current.id],
        [] as number[],
      ) ?? []

    return selectReferencesByIds(state, { referenceIds })
  },
)

export {
  fetchCustomAnnotationsForAlleleId,
  selectCustomAnnotationsByAlleleId,
  selectLatestCustomAnnotationIdForAlleleId,
  selectLatestCustomAnnotationIdsForAlleleIds,
  selectCustomAnnotationsByAnnotationId,
  selectReferencesByCustomAnnotationId,
}

export default customAnnotationsSlice.reducer
