import { Popover, Transition } from '@headlessui/react'
import { BellIcon } from '@heroicons/react/24/solid'
import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { fetchBroadcasts, selectBroadcasts } from 'store/broadcasts/broadcastsSlice'

const BROADCAST_FETCH_PERIOD = 60 * 1000 // 60sec

export default function BroadcastNotifier() {
  const dispatch = useDispatch()
  const broadcasts = useSelector(selectBroadcasts)
  const hasBroadcasts = broadcasts.length > 0
  // When the component is rendered, fetch the broadcasts
  // and re-fetch every 60sec.
  useEffect(() => {
    let nextFetch: NodeJS.Timeout | null = null
    const periodicFetch = async () => {
      try {
        await dispatch(fetchBroadcasts())
        nextFetch = setTimeout(periodicFetch, BROADCAST_FETCH_PERIOD)
      } catch (error) {
        console.error(error)
      }
    }
    periodicFetch()

    // Cleanup when the component unmounts.
    return () => {
      if (nextFetch) {
        clearInterval(nextFetch)
      }
    }
  }, [])

  return (
    <div className="w-4 px-4">
      <Popover className="relative">
        <>
          <Popover.Button>
            <BellIcon
              className={`h-6 w-6 fill-current pt-1.5 ${
                hasBroadcasts ? 'animate-bounce text-ellared-dark' : 'text-white'
              }`}
            />
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute -right-5 z-10 mt-3 w-screen max-w-sm px-0">
              <div className="overflow-hidden rounded shadow-lg ring-1 ring-black/5">
                <div className="relative space-y-4 bg-white p-5">
                  {broadcasts.length === 0 ? (
                    <div className="ml-4">
                      <p className="text-sm font-medium text-ellagray-500">
                        There are no messages.
                      </p>
                    </div>
                  ) : null}
                  {broadcasts.map((item) => (
                    <div key={item.id} className="ml-4 ">
                      <p className="text-sm font-medium text-ellagray-900 ">
                        {item.message}
                      </p>
                      <p className="text-sm text-ellagray-500">
                        {new Date(item.date).toLocaleString()}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      </Popover>
    </div>
  )
}
