import { AcmgCode, AcmgType } from 'types/store/Acmg'
import { Config } from 'types/store/Config'

import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'
import { AcmgStrengthHelper } from 'views/interpretation/helpers/acmg/AcmgStrengthHelper'

type ChangeDirection = 'upgrade' | 'downgrade'

export class AcmgStrengthChanger {
  private static changeCodeStrength(
    codeStr: string,
    config: Config,
    changeDirection: ChangeDirection,
  ): string {
    const base = AcmgHelper.getCodeBase(codeStr)
    const codeStrength = AcmgStrengthHelper.getCodeStrength(codeStr, config)
    const codeType = AcmgHelper.getCodeType(codeStr)

    if (codeType === 'other') {
      return codeStr
    }

    const configStrengths = config.acmg.codes[codeType]
    const strengthIndex = configStrengths.indexOf(codeStrength)
    const newStrength =
      configStrengths[
        strengthIndex + this.getStrengthChangeInterval(codeType, changeDirection)
      ]

    if (!newStrength) {
      return codeStr
    }

    // If new strength is same as base strength we just return base
    // in order to avoid BSxBS1
    const baseStrength = AcmgStrengthHelper.getCodeStrength(base, config)
    if (newStrength === baseStrength) {
      return base
    }

    return `${newStrength}x${base}`
  }

  private static getStrengthChangeInterval(
    codeType: Omit<AcmgType, 'other'>,
    changeDirection: ChangeDirection,
  ): number {
    switch (codeType) {
      case 'pathogenic':
        return changeDirection === 'upgrade' ? -1 : 1
      case 'benign':
        return changeDirection === 'upgrade' ? 1 : -1
      default:
        throw new Error(`No interval logic provided for "${codeType}"`)
    }
  }

  static upgradeCodeObj(code: AcmgCode, config: Config): AcmgCode {
    return { ...code, code: this.changeCodeStrength(code.code, config, 'upgrade') }
  }

  static canUpgradeCodeObj(code: AcmgCode, config: Config): boolean {
    return code.code !== this.changeCodeStrength(code.code, config, 'upgrade')
  }

  static downgradeCodeObj(code: AcmgCode, config: Config): AcmgCode {
    return { ...code, code: this.changeCodeStrength(code.code, config, 'downgrade') }
  }

  static canDowngradeCodeObj(code: AcmgCode, config: Config): boolean {
    return code.code !== this.changeCodeStrength(code.code, config, 'downgrade')
  }
}
