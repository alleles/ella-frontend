import React, { useCallback, useEffect, useState } from 'react'

import Button from 'components/elements/Button'

import { OverviewAnalysis } from 'types/store/AnalysisOverview'

/**
 * Analysis filter.
 *
 * AnalysisFilter will trigger the onChange function returning a filter method
 * to be used to filter analyses.
 */
interface Props {
  onChange
}

export default function AnalysisFilter({ onChange }: Props) {
  const [analysisText, setAnalysisText] = useState('')
  const [date, setDate] = useState('')
  const [includeHTS, setIncludeHTS] = useState(true)
  const [includeSanger, setIncludeSanger] = useState(true)
  const [includeNormalPriority, setIncludeNormalPriority] = useState(true)
  const [includeHighPriority, setIncludeHighPriority] = useState(true)
  const [includeUrgentPriority, setIncludeUrgentPriority] = useState(true)

  // Memoise the filter function depending on filter parameters
  const filter = useCallback(
    (analyses: OverviewAnalysis[]) => {
      let interval = 0

      if (date) {
        switch (date) {
          case 'l3d':
            interval = 3
            break
          case 'l1w':
            interval = 7
            break
          case 'l1m':
            interval = 30
            break
          default:
            interval = 3 * 30
            break
        }
      }

      return analyses.filter((analysis) => {
        if (
          analysisText &&
          analysis.name.toLowerCase().indexOf(analysisText.toLowerCase()) === -1 &&
          (!analysis.reviewComment ||
            (!!analysis.reviewComment &&
              analysis.reviewComment.toLowerCase().indexOf(analysisText.toLowerCase()) ===
                -1))
        )
          return false

        if (interval) {
          const comparisonDate = new Date()
          comparisonDate.setDate(comparisonDate.getDate() - interval)

          if (date === 'g3m') {
            if (new Date(analysis.date) > comparisonDate) return false
          } else if (new Date(analysis.date) < comparisonDate) return false
        }

        if (!includeHTS) {
          if (analysis.tags.includes('HTS')) {
            return false
          }
        }

        if (!includeSanger) {
          if (analysis.tags.includes('SANGER')) {
            return false
          }
        }

        if (
          (!includeNormalPriority && analysis.priority === 1) ||
          (!includeHighPriority && analysis.priority === 2) ||
          (!includeUrgentPriority && analysis.priority === 3)
        )
          return false
        return true
      })
    },
    [
      analysisText,
      date,
      includeHTS,
      includeSanger,
      includeNormalPriority,
      includeHighPriority,
      includeUrgentPriority,
    ],
  )

  // Update the filter function on depending components when filter parameters change.
  useEffect(() => {
    onChange(filter)
  }, [
    analysisText,
    date,
    includeHTS,
    includeSanger,
    includeNormalPriority,
    includeHighPriority,
    includeUrgentPriority,
  ])

  function onFilterReset() {
    setAnalysisText('')
    setDate('')
    setIncludeHTS(true)
    setIncludeSanger(true)
    setIncludeNormalPriority(true)
    setIncludeHighPriority(true)
    setIncludeUrgentPriority(true)
  }

  return (
    <form>
      <div className="grid grid-cols-5 items-start gap-4 pb-4 text-sm">
        <div className="col-span-2 flex shadow-sm">
          <span className="inline-flex items-center rounded-l-md border border-r-0 border-ellagray bg-ellagray-50 px-3 text-ellagray-500">
            Analysis / Comment
          </span>
          <input
            type="text"
            name="analysisText"
            id="analysisText"
            className="min-w-0 flex-1 rounded-r-md border-ellagray text-sm focus:border-ellablue focus:ring-ellablue"
            value={analysisText}
            onChange={(ev) => setAnalysisText(ev.target.value)}
          />
        </div>
        <div className="flex shadow-sm">
          <span className="inline-flex items-center rounded-l-md border border-r-0 border-ellagray bg-ellagray-50 px-3 text-ellagray-500">
            Date
          </span>
          <select
            id="dateRequested"
            name="dateRequested"
            className="block w-full rounded-r-md border-ellagray text-sm focus:border-ellablue focus:ring-ellablue"
            value={date}
            onChange={(ev) => setDate(ev.target.value)}
          >
            <option value="">---</option>
            <option value="l3d">&lt; 3 days ago</option>
            <option value="l1w">&lt; 1 week ago</option>
            <option value="l1m">&lt; 1 month ago</option>
            <option value="l3m">&lt; 3 months ago</option>
            <option value="g3m">≥ 3 months ago</option>
          </select>
        </div>
        <div className="grid grid-cols-2">
          <div>
            <div className="relative flex items-start">
              <div className="flex h-5 items-center">
                <input
                  id="hts"
                  name="hts"
                  type="checkbox"
                  className="h-4 w-4 rounded border-ellagray text-ellablue-darkest focus:ring-ellablue"
                  checked={includeHTS}
                  onChange={(ev) => setIncludeHTS(ev.target.checked)}
                />
              </div>
              <label htmlFor="hts" className="ml-2 font-medium text-ellagray-700">
                HTS
              </label>
            </div>
            <div className="relative flex items-start">
              <div className="flex h-5 items-center">
                <input
                  id="sanger"
                  name="sanger"
                  type="checkbox"
                  className="h-4 w-4 rounded border-ellagray text-ellablue-darkest focus:ring-ellablue"
                  checked={includeSanger}
                  onChange={(ev) => setIncludeSanger(ev.target.checked)}
                />
              </div>
              <label htmlFor="sanger" className="ml-2 font-medium text-ellagray-700">
                Sanger
              </label>
            </div>
          </div>
          <div className="col-start-2 space-y-0">
            <div className="relative flex">
              <div className="flex h-5 items-center">
                <input
                  id="normal"
                  name="normal"
                  type="checkbox"
                  className="h-4 w-4 rounded border-ellagray text-ellablue-darkest focus:ring-ellablue"
                  checked={includeNormalPriority}
                  onChange={(ev) => setIncludeNormalPriority(ev.target.checked)}
                />
              </div>
              <label htmlFor="normal" className="ml-2 font-medium text-ellagray-700">
                Normal
              </label>
            </div>
            <div className="relative flex">
              <div className="flex h-5 items-center">
                <input
                  id="high"
                  name="high"
                  type="checkbox"
                  className="h-4 w-4 rounded border-ellagray text-ellablue-darkest focus:ring-ellablue"
                  checked={includeHighPriority}
                  onChange={(ev) => setIncludeHighPriority(ev.target.checked)}
                />
              </div>
              <label htmlFor="high" className="ml-2 font-medium text-ellagray-700">
                High
              </label>
            </div>
            <div className="relative flex items-start">
              <div className="flex h-5 items-center">
                <input
                  id="urgent"
                  name="urgent"
                  type="checkbox"
                  className="h-4 w-4 rounded border-ellagray text-ellablue-darkest focus:ring-ellablue"
                  checked={includeUrgentPriority}
                  onChange={(ev) => setIncludeUrgentPriority(ev.target.checked)}
                />
              </div>
              <label htmlFor="urgent" className="ml-2 font-medium text-ellagray-700">
                Urgent
              </label>
            </div>
          </div>
        </div>
        <div className="w-40 justify-self-end">
          <Button type="button" onClick={onFilterReset}>
            Reset filter
          </Button>
        </div>
      </div>
    </form>
  )
}
