import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import isEqual from 'lodash/isEqual'

import API from 'app/API'

import referenceReducer, {
  fetchReferences,
  selectReferenceById,
  selectReferenceByPubMedId,
} from 'store/interpretation/referenceSlice'
import { store } from 'store/store'

import { Reference } from 'types/store/Reference'

const mock = new MockAdapter(axios)

function buildReference(override: Partial<Reference>): Reference {
  return {
    abstract: null,
    authors: null,
    journal: null,
    published: null,
    pubmedId: null,
    title: null,
    year: null,
    ...override,
  } as unknown as Reference
}

describe('reference reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(referenceReducer(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching references from the backend', async () => {
    mock.onGet(API.References.path).reply((config) => {
      const q = JSON.parse(config.params.q)
      if (isEqual(q, { pubmed_id: [4, 5, 6] })) {
        return [
          200,
          [
            {
              id: 4, // REMOVED
              title: 'pubmed dabla',
              pubmed_id: 44,
            },
            {
              id: 5, // REMOVED
              title: 'pubmed foo',
              pubmed_id: 55,
            },
            {
              id: 6, // REMOVED
              title: 'pubmed bar',
              pubmed_id: 66,
            },
          ],
        ]
      }
      if (isEqual(q, { id: [1, 2, 3] })) {
        return [
          200,
          [
            {
              id: 1, // REMOVED
              title: 'dabla',
            },
            {
              id: 2, // REMOVED
              title: 'foo',
            },
            {
              id: 3, // REMOVED
              title: 'bar',
            },
          ],
        ]
      }
      return [400]
    })

    const dispatch = jest.fn()
    await fetchReferences({ ids: [1, 2, 3], pubMedIds: [4, 5, 6] })(
      dispatch,
      () => undefined,
      {},
    )

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          1: expect.objectContaining({ title: 'dabla' }),
          2: expect.objectContaining({ title: 'foo' }),
          3: expect.objectContaining({ title: 'bar' }),
          4: expect.objectContaining({ title: 'pubmed dabla', pubmedId: 44 }),
          5: expect.objectContaining({ title: 'pubmed foo', pubmedId: 55 }),
          6: expect.objectContaining({ title: 'pubmed bar', pubmedId: 66 }),
        },
      }),
    )
  })

  it('should handle getting references by id and pubmed id', async () => {
    const state = {
      ...store.getState(),
      references: {
        1: buildReference({ title: 'foo', pubmedId: 11 }),
        2: buildReference({ title: 'bar' }),
        3: buildReference({ title: 'dabla', pubmedId: 33 }),
      },
    }

    expect(selectReferenceById(state, { referenceId: 1 })).toEqual(
      buildReference({ title: 'foo', pubmedId: 11 }),
    )
    expect(selectReferenceByPubMedId(state, { pubMedId: 2 })).toEqual(undefined)
    expect(selectReferenceByPubMedId(state, { pubMedId: 33 })).toEqual(
      buildReference({ title: 'dabla', pubmedId: 33 }),
    )
  })
})
