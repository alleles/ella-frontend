import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import { fetchAnnotationConfig } from 'store/interpretation/annotationConfigSlice'
import { selectFilterConfigById } from 'store/interpretation/filterConfigSlice'
import {
  fetchReferences,
  selectReferencesByPubMedIds,
} from 'store/interpretation/referenceSlice'
import type { RootState } from 'store/store'
import { throwSerializedError } from 'store/utils'

import * as ApiTypes from 'types/api/Pydantic'
import { Annotation, mapFromApi } from 'types/store/Annotation'
import { Reference } from 'types/store/Reference'

const initialState: Record<number, Annotation> = {}

const processAndStoreAnnotation = createAsyncThunk<
  Record<number, Annotation>,
  { annotationsFromApi: ApiTypes.Annotation; alleleId: number }
>(
  'annotation/processAndStoreAnnotation',
  async ({ annotationsFromApi, alleleId }, { dispatch, getState }) => {
    const state = getState() as RootState
    const { annotation_id } = annotationsFromApi
    const annotation = mapFromApi(annotationsFromApi, alleleId, state)

    await dispatch(fetchAnnotationConfig(annotation.annotationConfigId))

    // Typescript doesn't understand that pubmedIds/ids are always number
    // (due to the filter(Boolean)). See https://github.com/microsoft/TypeScript/issues/16655
    const referenceIds = {
      pubMedIds: annotation.references.map((r) => r.pubmedId).filter(Boolean),
      ids: annotation.references.map((r) => r.id).filter(Boolean),
    } as { pubMedIds: number[]; ids: number[] }

    await dispatch(fetchReferences(referenceIds))
    return { [annotation_id]: annotation }
  },
)

const annotationSlice = createSlice({
  name: 'annotations',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(processAndStoreAnnotation.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
    builder.addCase(processAndStoreAnnotation.rejected, (state, payload) => {
      throwSerializedError(payload.error)
    })
  },
})

const selectAnnotationByAnnotationId = (
  state: RootState,
  { annotationId }: { annotationId: number },
) => state.annotations[annotationId]

const SORTED_CONSEQUENCES = [
  'transcript_ablation',
  'splice_donor_variant',
  'splice_acceptor_variant',
  'stop_gained',
  'frameshift_variant',
  'start_lost',
  'initiator_codon_variant',
  'stop_lost',
  'inframe_insertion',
  'inframe_deletion',
  'missense_variant',
  'protein_altering_variant',
  'transcript_amplification',
  'splice_region_variant',
  'incomplete_terminal_codon_variant',
  'synonymous_variant',
  'start_retained_variant',
  'stop_retained_variant',
  'coding_sequence_variant',
  'mature_mirna_variant',
  'field_5_prime_utr_variant',
  'field_3_prime_utr_variant',
  'non_coding_transcript_exon_variant',
  'non_coding_transcript_variant',
  'intron_variant',
  'nmd_transcript_variant',
  'upstream_gene_variant',
  'downstream_gene_variant',
  'tfbs_ablation',
  'tfbs_amplification',
  'tf_binding_site_variant',
  'regulatory_region_variant',
  'regulatory_region_ablation',
  'regulatory_region_amplification',
  'feature_elongation',
  'feature_truncation',
  'intergenic_variant',
]
const selectWorstConsequenceById = createSelector(
  selectAnnotationByAnnotationId,
  (annotation) => {
    const { transcripts } = annotation

    const sortedAnnotatedConsequences = transcripts
      .flatMap((tx) => tx.consequences)
      .sort((a, b) => SORTED_CONSEQUENCES.indexOf(a) - SORTED_CONSEQUENCES.indexOf(b))

    if (sortedAnnotatedConsequences.length === 0) {
      return undefined
    }
    const worstConsequence = sortedAnnotatedConsequences[0]
    const transcriptsWithWorstConsequence = transcripts
      .filter((tx) => tx.consequences.includes(worstConsequence))
      .map((tx) => tx.transcript)

    return { consequence: worstConsequence, transcripts: transcriptsWithWorstConsequence }
  },
)

const selectReferencesByAnnotationId = createSelector(
  (state: RootState) => state,
  selectAnnotationByAnnotationId,
  (state: RootState, annotation): Reference[] => {
    const pubmedIds =
      annotation.references?.reduce((previous, current) => {
        if (current.pubmedId) {
          return [...previous, current.pubmedId]
        }
        return previous
      }, [] as number[]) ?? []

    return (
      selectReferencesByPubMedIds(state, { pubmedIds }).filter(
        (reference) => !!reference,
      ) ?? []
    )
  },
)

const selectLatestAnnotationIdByAlleleId = (
  state: RootState,
  { alleleId }: { alleleId: number },
) =>
  Object.entries(state.annotations)
    .filter(([, an]) => an.alleleId === alleleId && an.dateSuperseeded === null)
    .map(([id]) => parseInt(id, 10))[0]

const selectLatestAnnotationIdsByAlleleIds = (
  state: RootState,
  { alleleIds }: { alleleIds: number[] },
) =>
  alleleIds.reduce(
    (previous, current) => ({
      ...previous,
      [current]: selectLatestAnnotationIdByAlleleId(state, { alleleId: current }),
    }),
    {} as Record<number, number>,
  )

interface FreqTableRow {
  provider: string
  population: string
  numThreshold: number | null
}

const selectFrequencies = createSelector(
  selectAnnotationByAnnotationId,
  selectFilterConfigById,
  (state: RootState, { key }: { key: 'freq' | 'count' }) => key,
  (annotation, filterConfig, key): { maxValue: number | null } => {
    if (!annotation) {
      return { maxValue: null }
    }
    const computeMaxValue = (acc: number | null, r: FreqTableRow): number | null => {
      const populationValues = annotation.frequencies[r.provider][key]
      const num = annotation.frequencies[r.provider].num![r.population]

      if (populationValues === undefined) {
        return acc
      }
      const value: number = populationValues[r.population]
      const isLargerThanAcc = acc === null || value > acc
      const meetsThreshold = r.numThreshold === undefined || !num || num > r.numThreshold!
      if (isLargerThanAcc && meetsThreshold) {
        return value
      }
      return acc
    }
    const freqConfigByGroup: FreqTableRow[] = (() => {
      // Find the freq filter. WARNING: Select first frequency filter.
      // Not strictly correct when there are multiple frequency filters defined.
      const frequencyFilter = filterConfig.filterConfig.filters.find(
        (f) => f.name === 'frequency',
      )
      if (!frequencyFilter) {
        return []
      }
      if (!frequencyFilter.config.groups) {
        return []
      }
      const rows = Object.values(frequencyFilter.config.groups).flatMap<FreqTableRow>(
        (providers) =>
          Object.entries(providers).flatMap<FreqTableRow>(([provider, populations]) =>
            populations.map<FreqTableRow>((population) => {
              const frequencyNumThresholds = frequencyFilter.config.num_thresholds || {}
              let hasThreshold = true
              hasThreshold = hasThreshold && provider in frequencyNumThresholds
              hasThreshold =
                hasThreshold && population in frequencyNumThresholds[provider]
              const numThreshold = hasThreshold
                ? frequencyNumThresholds[provider][population]
                : null
              return { provider, population, numThreshold }
            }),
          ),
      )
      return rows
    })()
    const maxValue: number | null = freqConfigByGroup.reduce<number | null>(
      (acc, row) => {
        let hasValue = true
        hasValue = hasValue && row.provider in annotation.frequencies
        hasValue = hasValue && key in annotation.frequencies[row.provider]
        if (!hasValue) {
          return acc
        }
        return computeMaxValue(acc, row)
      },
      null,
    )
    return {
      maxValue,
    }
  },
)

export {
  processAndStoreAnnotation,
  selectAnnotationByAnnotationId,
  selectLatestAnnotationIdByAlleleId,
  selectLatestAnnotationIdsByAlleleIds,
  selectReferencesByAnnotationId,
  selectWorstConsequenceById,
  selectFrequencies,
}

export default annotationSlice.reducer
