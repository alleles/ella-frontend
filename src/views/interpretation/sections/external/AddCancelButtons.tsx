import { PlusIcon, XMarkIcon } from '@heroicons/react/24/solid'
import React from 'react'

import Button from 'components/elements/Button'

interface Props {
  onOk: () => void
  onCancel: () => void
}

export function AddCancelButtons({ onCancel, onOk }: Props) {
  return (
    <div className="ml-2 flex gap-x-2">
      <div className="w-12">
        <Button onClick={onOk}>
          <PlusIcon height={20} />
        </Button>
      </div>
      <div className="w-12">
        <Button onClick={onCancel}>
          <XMarkIcon height={20} />
        </Button>
      </div>
    </div>
  )
}
