import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { Broadcast, mapFromApi } from 'types/store/Broadcast'

interface SliceState {
  broadcasts: Broadcast[]
}

// Initial state
const initialState: SliceState = {
  broadcasts: [],
}

// Async actions
export const fetchBroadcasts = createAsyncThunk<Broadcast[]>(
  'broadcasts/fetchBroadcasts',
  async () => API.Broadcasts.get().then((response) => response.data.map(mapFromApi)),
)

const broadcastsSlice = createSlice({
  name: 'broadcasts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchBroadcasts.fulfilled, (state, action) => ({
      ...state,
      broadcasts: action.payload,
    }))
  },
})

// Selectors
export const selectBroadcasts = (state: RootState) => state.broadcasts.broadcasts

// Default export should always be the reducer
export default broadcastsSlice.reducer
