FROM node:lts-bullseye@sha256:ca745bea8b40933703bf8e792f141d4b219f0878aaa6af70176116d7594d4a05
ENV DEBIAN_FRONTEND=noninteractive

# Install git on top of alpine.
RUN apt-get update -qq && apt-get install -yqq python3-dev python3-pip git jq less python-is-python3
RUN mkdir /dist && chown node:node /dist
RUN mkdir /app
# install app dependencies
COPY package.json /app
COPY package-lock.json /app
RUN chown -R node:node /app

USER node:node

# set working directory
WORKDIR /app

RUN npm install

ENV PYTHONPATH=/dist/python
ENV PATH=${PYTHONPATH}/bin:${PATH}
COPY tests/requirements.txt /dist/requirements.txt
RUN pip3 install -r /dist/requirements.txt -t ${PYTHONPATH}

# ensure dir exists to allow storing extensions, etc. on host OS
RUN mkdir -p /home/node/.vscode-server/extensions

# add app
ADD . ./

# start the app
CMD ["scripts/entrypoint.sh"]
