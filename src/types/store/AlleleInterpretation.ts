// Maps an ApiTypes.AlleleInterpretation object to an object of type AlleleInterpretation
import * as ApiTypes from 'types/api/Pydantic'
import * as AcmgTypes from 'types/store/Acmg'
import * as ReferenceAssessment from 'types/store/ReferenceAssessment'
import * as User from 'types/store/User'

interface Comment {
  comment: string
}

interface SuggestedAcmg {
  included: AcmgTypes.AcmgCode[]
  suggested: AcmgTypes.AcmgCode[]
  suggestedClassification?: number
}

interface StateEvaluation {
  acmg: SuggestedAcmg
  external: Comment
  frequency: Comment
  reference: Comment
  prediction: Comment
  classification: Comment
  similar: Comment
}

export interface StateAlleleAssessment {
  evaluation: StateEvaluation
  attachmentIds: number[]
  classification: ApiTypes.AlleleAssessmentClassification | null
}

export interface AlleleState {
  alleleAssessmentId: number | null
  reuseAlleleAssessment: boolean
  alleleReportId: number | null
  alleleReport: Comment
  alleleAssessment: StateAlleleAssessment
  reuseAlleleReport: boolean
  referenceAssessmentIds: number[]
  referenceAssessments: ReferenceAssessment.ReferenceAssessment[]
}

export interface AlleleInterpretationState {
  allele: {
    [k: string]: AlleleState
  }
}

export interface AlleleInterpretation {
  id: number
  alleleId: number
  status: 'Not started' | 'Ongoing' | 'Done'
  finalized: boolean
  workflowStatus: 'Interpretation' | 'Review'
  userState: {
    [k: string]: any
  }
  state: AlleleInterpretationState
  genePanelName: string
  genePanelVersion: string
  dateLastUpdate: string
  dateCreated: string
  userId: number | null
}

export function mapFromApi(
  alleleInterpretationFromApi: ApiTypes.AlleleInterpretation,
  alleleId: number,
): AlleleInterpretation {
  return {
    id: alleleInterpretationFromApi.id,
    alleleId,
    dateCreated: alleleInterpretationFromApi.date_created,
    dateLastUpdate: alleleInterpretationFromApi.date_last_update,
    finalized: alleleInterpretationFromApi.finalized ?? false,
    genePanelName: alleleInterpretationFromApi.genepanel_name,
    genePanelVersion: alleleInterpretationFromApi.genepanel_version,
    state: mapFromApiAlleleInterpretationState(alleleInterpretationFromApi.state),
    status: alleleInterpretationFromApi.status,
    userId: alleleInterpretationFromApi?.user?.id || null,
    userState: {},
    workflowStatus: alleleInterpretationFromApi.workflow_status,
  }
}

export function mapFromApiStateAlleleAssessment(
  alleleAssessmentFromApi: ApiTypes.StateAlleleassessment,
): StateAlleleAssessment {
  const { attachment_ids, classification, evaluation } = alleleAssessmentFromApi
  return {
    attachmentIds: attachment_ids || [],
    classification: classification || null,
    evaluation: {
      external: {
        comment: evaluation?.external?.comment || '',
      },
      frequency: {
        comment: evaluation?.frequency?.comment || '',
      },
      reference: {
        comment: evaluation?.reference?.comment || '',
      },
      prediction: {
        comment: evaluation?.prediction?.comment || '',
      },
      classification: {
        comment: evaluation?.classification?.comment || '',
      },
      similar: {
        comment: evaluation?.similar?.comment || '',
      },
      acmg: {
        suggested: evaluation?.acmg.suggested?.map(AcmgTypes.mapFromApi) ?? [],
        included: evaluation?.acmg?.included?.map(AcmgTypes.mapFromApi) ?? [],
        suggestedClassification: evaluation?.acmg.suggested_classification,
      },
    },
  }
}

export function mapFromApiAlleleInterpretationState(
  alleleStateFromApi: ApiTypes.AlleleInterpretation['state'],
): AlleleInterpretationState {
  if (!alleleStateFromApi || !alleleStateFromApi.allele) {
    return { allele: {} }
  }

  const alleleIds = Object.keys(alleleStateFromApi.allele ?? {})

  return {
    allele: alleleIds.reduce(
      (previous: AlleleInterpretationState['allele'], currentAlleleId) => {
        if (!alleleStateFromApi || !alleleStateFromApi.allele) {
          throw new Error('Allele state is undefined')
        }

        const alleleInterpretationStateFromApi =
          alleleStateFromApi.allele[currentAlleleId]
        const alleleState: AlleleState = {
          alleleAssessmentId:
            alleleInterpretationStateFromApi.alleleassessment.reuseCheckedId || null,
          reuseAlleleAssessment:
            alleleInterpretationStateFromApi.alleleassessment.reuse || false,
          alleleReportId:
            alleleInterpretationStateFromApi.allelereport.copiedFromId || null,
          reuseAlleleReport: false, // TODO: Figure this out. Where is this specified on the alleleStateObject
          referenceAssessmentIds: alleleInterpretationStateFromApi.referenceassessments
            .filter(
              (
                referenceAssessment,
              ): referenceAssessment is ApiTypes.ReusedReferenceAssessment =>
                'id' in referenceAssessment,
            )
            .map((referenceAssessment) => referenceAssessment.id),
          referenceAssessments: alleleInterpretationStateFromApi.referenceassessments
            .filter(
              (
                referenceAssessment,
              ): referenceAssessment is ApiTypes.NewReferenceAssessment =>
                !('id' in referenceAssessment),
            )
            .map(ReferenceAssessment.mapFromApi),
          alleleReport: {
            comment:
              alleleInterpretationStateFromApi.allelereport.evaluation.comment || '',
          },
          alleleAssessment: mapFromApiStateAlleleAssessment(
            alleleInterpretationStateFromApi.alleleassessment,
          ),
        }

        return {
          ...previous,
          [currentAlleleId]: alleleState,
        }
      },
      {},
    ),
  }
}

export function mapToApi(
  alleleInterpretation: AlleleInterpretation,
  alleleInterpretationId,
  user: User.User,
): ApiTypes.AlleleInterpretation {
  return {
    date_created: alleleInterpretation.dateCreated,
    date_last_update: alleleInterpretation.dateLastUpdate,
    finalized: alleleInterpretation.finalized,
    genepanel_name: alleleInterpretation.genePanelName,
    genepanel_version: alleleInterpretation.genePanelVersion,
    id: alleleInterpretationId,
    state: mapToApiAlleleInterpretationState(
      alleleInterpretation.state,
      alleleInterpretation.genePanelName,
      alleleInterpretation.genePanelVersion,
      user,
    ),
    status: alleleInterpretation.status,
    user: User.mapToApi(user),
    user_id: user.id,
    user_state: {},
    workflow_status: alleleInterpretation.workflowStatus,
  }
}

export function mapToApiAlleleInterpretationState(
  alleleInterpretationState: AlleleInterpretationState,
  genePanelName: string,
  genePanelVersion: string,
  user: User.User,
): ApiTypes.AlleleInterpretation['state'] {
  if (!alleleInterpretationState || !alleleInterpretationState.allele) {
    throw new Error('Allele state is undefined')
  }

  return {
    allele: Object.keys(alleleInterpretationState.allele ?? {}).reduce(
      (previous: ApiTypes.AlleleInterpretationState['allele'], currentAlleleId) => {
        if (!alleleInterpretationState || !alleleInterpretationState.allele) {
          throw new Error('Allele state is undefined')
        }

        const currentAlleleIdAsInt = parseInt(currentAlleleId, 10)

        const allele = alleleInterpretationState.allele[currentAlleleId]

        const apiAlleleState: ApiTypes.AlleleState[string] = {
          referenceAssessmentIds: allele.referenceAssessmentIds,
          allele_id: currentAlleleIdAsInt,
          alleleassessment: {
            id: 0,
            date_created: '',
            allele_id: currentAlleleIdAsInt,
            genepanel_name: genePanelName,
            genepanel_version: genePanelVersion,
            user_id: user.id,
            user: User.mapToApi(user),
            seconds_since_update: 0,

            reuseCheckedId: allele.alleleAssessmentId || undefined,
            reuse: allele.reuseAlleleAssessment,
            attachment_ids: allele.alleleAssessment.attachmentIds,
            classification: allele.alleleAssessment.classification,
            evaluation: {
              external: allele.alleleAssessment.evaluation.external,
              frequency: allele.alleleAssessment.evaluation.frequency,
              reference: allele.alleleAssessment.evaluation.reference,
              prediction: allele.alleleAssessment.evaluation.prediction,
              classification: allele.alleleAssessment.evaluation.classification,
              similar: allele.alleleAssessment.evaluation.similar,
              acmg: {
                included: allele.alleleAssessment.evaluation.acmg.included.map(
                  AcmgTypes.mapToApiIncludedItem,
                ),
                suggested: allele.alleleAssessment.evaluation.acmg.suggested.map(
                  AcmgTypes.mapToApiSuggestedItem,
                ),
                suggested_classification:
                  allele.alleleAssessment.evaluation.acmg.suggestedClassification,
              },
            },
          },
          allelereport: {
            user_id: user.id,
            usergroup_id: user.group?.id ?? 0,
            usergroup: user.group ?? { id: 0, name: '' },
            user: User.mapToApi(user),
            seconds_since_update: 0,

            id: 0,
            date_created: '',
            allele_id: currentAlleleIdAsInt,
            copiedFromId: allele.alleleReportId ?? undefined,
            evaluation: allele.alleleReport,
          },
          referenceassessments: allele.referenceAssessmentIds.map(
            (referenceAssessment) => ({
              id: referenceAssessment,
              allele_id: currentAlleleIdAsInt,
              reference_id: referenceAssessment,
              evaluation: {},
            }),
          ),
        }

        return {
          ...previous,
          [currentAlleleId]: apiAlleleState,
        }
      },
      {} as ApiTypes.AlleleInterpretationState['allele'],
    ),
  }
}
