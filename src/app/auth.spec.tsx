import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import React from 'react'
import { unmountComponentAtNode } from 'react-dom'
import ReactDOM from 'react-dom/client'
import { act } from 'react-dom/test-utils'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import API from 'app/API'
import useAuth, { AuthProvider } from 'app/auth'

import { store } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import { UserFull } from 'types/api/Pydantic'

import { deepCopy } from 'utils/general'

const mock = new MockAdapter(axios)

describe('Auth utils', () => {
  function TestComponent() {
    const { login, logout, authenticated } = useAuth()

    return (
      <div>
        <p>{authenticated.toString()}</p>
        <button
          id="login"
          onClick={() => {
            login('foo', 'bar')
          }}
          type="button"
        >
          login
        </button>
        <button
          id="logout"
          onClick={() => {
            logout()
          }}
          type="button"
        >
          logout
        </button>
      </div>
    )
  }

  function ContextProvider() {
    return (
      <Provider store={store}>
        <Router>
          <AuthProvider>
            <TestComponent />
          </AuthProvider>
        </Router>
      </Provider>
    )
  }

  let container: HTMLDivElement | null = null
  let root: any | null = null

  beforeEach(() => {
    container = document.createElement('div')
    document.body.appendChild(container)
    root = ReactDOM.createRoot(container)
  })
  afterEach(() => {
    if (container) {
      root.unmount()
      unmountComponentAtNode(container)
      container.remove()
      container = null
    }
    mock.reset()
  })

  it('should handle logging in, logging out and maintaining authentication state', async () => {
    mock.onPost(API.Login.path).reply(200, {})
    mock.onPost(API.Logout.path).reply(200, {})

    act(() => {
      root.render(<ContextProvider />)
    })

    if (!container) {
      throw new Error('Container is null')
    }

    const loginButton = container.querySelector('#login')
    const logoutButton = container.querySelector('#logout')
    const label = container.querySelector('p')

    if (!label || !logoutButton || !loginButton) {
      throw new Error('An element was not found in DOM')
    }

    expect(label.textContent).toBe('false')

    await act(async () => {
      loginButton.dispatchEvent(new MouseEvent('click', { bubbles: true }))
    })

    expect(mock.history.post.length).toBe(1)
    expect(mock.history.post[0].url).toBe(API.Login.path)
    expect(JSON.parse(mock.history.post[0].data)).toMatchObject({
      username: 'foo',
      password: 'bar',
    })

    expect(label.textContent).toBe('true')

    await act(async () => {
      logoutButton.dispatchEvent(new MouseEvent('click', { bubbles: true }))
    })

    expect(mock.history.post.length).toBe(2)
    expect(mock.history.post[1].url).toBe(API.Logout.path)

    expect(label.textContent).toBe('false')
  })

  it('should handle populating the store on login', async () => {
    mock.onPost(API.Login.path).reply(200, {})
    mock.onPost(API.Logout.path).reply(200, {})

    // We mock the store post-login endpoints to make sure they are called and set on the store
    mock.onGet(API.Config.path).reply(200, { foo: 'bar' })
    mock
      .onGet(API.CurrentUser.path)
      .reply(200, { id: 1, username: 'foobar', first_name: 'foo', last_name: 'bar' })
    mock.onGet(API.Users.path).reply<Partial<UserFull>[]>(200, [
      { id: 1, username: 'foobar', first_name: 'foo', last_name: 'bar' },
      { id: 2, username: 'foo' },
      { id: 3, username: 'bar' },
    ])

    act(() => {
      root.render(<ContextProvider />)
    })

    if (!container) {
      throw new Error('Container is null')
    }

    const loginButton = container.querySelector('#login')
    const logoutButton = container.querySelector('#logout')
    const initialState = deepCopy(store.getState())

    if (!loginButton || !logoutButton) {
      throw new Error('Login or logout button not found in DOM')
    }

    await act(async () => {
      loginButton.dispatchEvent(new MouseEvent('click', { bubbles: true }))
    })

    const state = store.getState()

    expect(state.config).toMatchObject({ foo: 'bar' })
    expect(selectCurrentUser(state)).toMatchObject({
      firstName: 'foo',
      lastName: 'bar',
      username: 'foobar',
    })

    expect(state.users.users).toMatchObject({
      1: { username: 'foobar' },
      2: { username: 'foo' },
      3: { username: 'bar' },
    })

    await act(async () => {
      logoutButton.dispatchEvent(new MouseEvent('click', { bubbles: true }))
    })

    // Once the user logs out the store should be reset
    expect(store.getState()).toMatchObject(initialState)
  })
})
