import API from 'app/API'

import {
  CustomAnnotation,
  UpdateCustomAnnotationPayload,
  mapToApiAnnotations,
} from 'types/store/CustomAnnotation'
import { User } from 'types/store/User'

type Arguments<T> = {
  alleleId: number
  customAnnotationsType: keyof CustomAnnotation['annotations']
  user: User
  newAnnotationValues: T
  currentCustomAnnotations: CustomAnnotation | undefined
}

const createUpdateCustomAnnotationsPayload = <T>({
  alleleId,
  user,
  newAnnotationValues,
  currentCustomAnnotations,
  customAnnotationsType,
}: Arguments<T>): UpdateCustomAnnotationPayload => {
  if (currentCustomAnnotations) {
    return {
      alleleId,
      userId: user.id,
      annotations: {
        ...currentCustomAnnotations.annotations,
        [customAnnotationsType]: newAnnotationValues,
      },
    }
  }

  return {
    alleleId,
    userId: user.id,
    annotations: {
      [customAnnotationsType]: newAnnotationValues,
    },
  }
}

const updateCustomAnnotations = <T>({
  alleleId,
  user,
  newAnnotationValues,
  currentCustomAnnotations,
  customAnnotationsType,
}: Arguments<T>): Promise<any> => {
  const payload = createUpdateCustomAnnotationsPayload({
    alleleId,
    user,
    newAnnotationValues,
    currentCustomAnnotations,
    customAnnotationsType,
  })
  return API.CustomAnnotations.post(
    payload.alleleId,
    payload.userId,
    mapToApiAnnotations(payload.annotations),
  )
}

export default updateCustomAnnotations
