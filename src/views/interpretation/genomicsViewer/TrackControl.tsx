import React, { useEffect, useMemo, useState } from 'react'

import API from 'app/API'

import SectionBox from 'components/elements/SectionBox'
import ToggleButton from 'components/elements/ToggleButton'

import { ApiTrackConfigs } from 'types/api/TrackConfigs'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import { TrackConfig } from 'views/interpretation/genomicsViewer/TrackConfig'
import { TrackSelectionContext } from 'views/interpretation/genomicsViewer/TrackSelectionContext'

interface TrackConfigs {
  [trackId: string]: TrackConfig
}

interface TrackIdsByPreset {
  [presetId: string]: Set<string>
}
// preset button states
interface PresetStates {
  [presetId: string]: boolean
}

type SelectedTracks = Set<string>

const createTrackConfigs = (apiTrackConfigs: ApiTrackConfigs): TrackConfigs => {
  const addGenericPresets = (
    presets: string[],
    selected: boolean,
  ): [string, ...string[]] => {
    const presetIdDefault = 'Default'
    const presetIdOther = 'Other'
    if (selected) {
      return [presetIdDefault, ...presets]
    }
    if (!presets || presets.length === 0) {
      return [presetIdOther]
    }
    return [presets[0], ...presets.slice(1)]
  }
  const trackConfigs: TrackConfigs = {}
  Object.keys(apiTrackConfigs).forEach((k: string) => {
    const apiTrackConfig = apiTrackConfigs[k]
    const selected = 'show' in apiTrackConfig ? Boolean(apiTrackConfig.show) : false
    const finalizedTrackConfig: TrackConfig = {
      selected,
      igv: apiTrackConfig.igv,
      type: apiTrackConfig.type ? apiTrackConfig.type : null,
      presets: addGenericPresets(apiTrackConfig.presets || [], selected),
      description: apiTrackConfig.description,
    }
    trackConfigs[k] = finalizedTrackConfig
  })
  return trackConfigs
}

const createTrackIdsByPreset = (tc: TrackConfigs): TrackIdsByPreset => {
  const r: TrackIdsByPreset = {}
  if (!tc) {
    return r
  }
  Object.entries(tc).forEach(([trackId, trackCfg]) => {
    trackCfg.presets.forEach((presetId) => {
      if (!(presetId in r)) {
        r[presetId] = new Set()
      }
      r[presetId].add(trackId)
    })
  })
  return r
}

// set of track IDs: Set[track_ID_1, track_ID_2, ... ]
const computeSelectedTracks = (tcs: TrackConfigs): SelectedTracks => {
  const r = new Set<string>()
  Object.entries(tcs).forEach(([trackId, trackCfg]) => {
    if (trackCfg.selected) {
      r.add(trackId)
    }
  })
  return r
}

const createSortedPresetIds = (trackIdsByPreset: TrackIdsByPreset): string[] =>
  Object.keys(trackIdsByPreset).reduce(
    (acc: string[], e) => (e === 'Default' ? [e, ...acc] : [...acc, e]),
    [],
  )

const isSubset = (a: Set<String>, b: Set<String>) =>
  a.size >= b.size && [...b].every((value) => a.has(value))

const createSortedPresetStates = (
  trackConfigs: TrackConfigs,
  trackIdsByPreset: TrackIdsByPreset,
  selectedTracks: SelectedTracks,
): PresetStates => {
  // init model
  const presetModel = {}
  // set status
  createSortedPresetIds(trackIdsByPreset).forEach((presetId) => {
    presetModel[presetId] = isSubset(
      selectedTracks,
      new Set([...trackIdsByPreset[presetId]]),
    )
  })
  return presetModel
}

export default function TrackControl() {
  const { id: analysisId, presentationAlleles, type } = useInterpretation()

  if (type === 'allele') {
    throw Error('Backend does not support fetching tracks for allele type')
  }

  const [trackConfigs, setTrackConfigs] = useState<TrackConfigs>({})

  // get and fetch track configs from api
  useEffect(() => {
    const alleleIds = Object.keys(presentationAlleles)
    API.TrackConfigs.get(analysisId, alleleIds).then((result) => {
      // derive internal state from RootState
      const apiTrackConfigs: ApiTrackConfigs = result.data
      setTrackConfigs(createTrackConfigs(apiTrackConfigs))
    })
  }, [analysisId, presentationAlleles])

  // trackConfigs info prepared to render and use buttons
  const trackIdsByPreset: TrackIdsByPreset = createTrackIdsByPreset(trackConfigs)
  const selectedTracks: SelectedTracks = useMemo(
    () => computeSelectedTracks(trackConfigs),
    [trackConfigs],
  )
  const sortedPresetStates: PresetStates = createSortedPresetStates(
    trackConfigs,
    trackIdsByPreset,
    selectedTracks,
  )

  // provide selected track configs to other components
  const context = React.useContext(TrackSelectionContext)
  useEffect(() => {
    const selectedTrackConfigs: TrackConfig[] = Array.from(selectedTracks).map(
      (trackId) => trackConfigs[trackId],
    )
    context.setTrackConfigs(selectedTrackConfigs)
  }, [selectedTracks])

  // button callbacks
  const onTrackButtonClick = (trackId: string) => {
    const tc = { ...trackConfigs }
    tc[trackId].selected = !tc[trackId].selected
    setTrackConfigs(tc)
  }
  const onPresetButtonClick = (presetId: string, show: boolean) => {
    const tracksToUpdate = {}
    Object.entries(trackConfigs).forEach(([trackId, trackCfg]) => {
      if (!trackCfg.presets.includes(presetId)) {
        return
      }
      if (show) {
        // activate track
        tracksToUpdate[trackId] = true
      } else {
        // only deactivate if not referenced by any other active preset
        const associatedActivePresets = trackCfg.presets.filter(
          (e) => sortedPresetStates[e] && e !== presetId,
        )
        if (associatedActivePresets.length === 0) {
          tracksToUpdate[trackId] = false
        }
      }
    })
    // anything to do?
    if (Object.keys(tracksToUpdate).length === 0) {
      return
    }
    // update state
    const tc = { ...trackConfigs }
    Object.keys(tracksToUpdate).forEach((trackId) => {
      tc[trackId].selected = tracksToUpdate[trackId]
    })
    setTrackConfigs(tc)
  }
  return (
    <div className="grow pl-2">
      <SectionBox title="Preset selection" color="ellablue-lighter">
        <span className="mt-4 text-sm uppercase">Presets</span>
        <div className="m-5 grid grid-flow-row-dense gap-1 md:grid-cols-3 lg:grid-cols-4">
          {Object.keys(sortedPresetStates).map((presetId) => (
            <ToggleButton
              onClick={(b) => onPresetButtonClick(presetId, b)}
              selected={sortedPresetStates[presetId]}
              key={presetId}
            >
              {presetId}
            </ToggleButton>
          ))}
        </div>
      </SectionBox>
      <SectionBox title="Track selection" color="ellablue-lighter">
        {Object.keys(trackIdsByPreset).map((presetId: string) => {
          const trackIdsForPreset: string[] = Array.from(trackIdsByPreset[presetId])
          return (
            <React.Fragment key={presetId}>
              <span className="mt-4 text-sm uppercase">{presetId}</span>
              <div className="m-5 grid grid-flow-row-dense gap-1 md:grid-cols-3 lg:grid-cols-4">
                {trackIdsForPreset.map((trackId: string) => {
                  const trackName = trackConfigs[trackId].igv.name
                  return (
                    <ToggleButton
                      key={`${presetId}/${trackName}`}
                      onClick={() => onTrackButtonClick(trackId)}
                      selected={trackConfigs[trackId].selected}
                      title={trackConfigs[trackId].description}
                    >
                      {trackName}
                    </ToggleButton>
                  )
                })}
              </div>
            </React.Fragment>
          )
        })}
      </SectionBox>
    </div>
  )
}
