import { createSelector } from 'reselect'

import {
  selectAlleleAssessmentById,
  selectAlleleAssessmentIsOutdated,
  selectLatestAlleleAssessmentByAlleleId,
} from 'store/interpretation/alleleAssessmentSlice'
import { selectAlleleById } from 'store/interpretation/alleleSlice'
import {
  selectAnnotationByAnnotationId,
  selectFrequencies,
} from 'store/interpretation/annotationSlice'
import { selectCustomAnnotationsByAnnotationId } from 'store/interpretation/customAnnotationSlice'
import { AlleleDisplay, selectAlleleDisplay } from 'store/selectors/genePanelAnnotation'
import { RootState } from 'store/store'

import { Allele } from 'types/store/Allele'
import { AlleleAssessment } from 'types/store/AlleleAssessment'
import {
  AlleleInterpretationState,
  StateAlleleAssessment,
} from 'types/store/AlleleInterpretation'
import { AnalysisInterpretationState } from 'types/store/AnalysisInterpretation'
import { Annotation } from 'types/store/Annotation'
import { CustomAnnotation } from 'types/store/CustomAnnotation'
import { AnalysisPresentationAllele, PresentationAllele } from 'types/view/Interpretation'

export type ColumnNames =
  | 'position'
  | 'inheritance'
  | 'hgvsc'
  | 'geneSymbols'
  | 'hgvsp'
  | 'consequence'
  | 'genotype'
  | 'quality'
  | 'depth'
  | 'ratio'
  | 'hiFreq'
  | 'hiCount'
  | 'externalDbs'
  | 'classification'
// | 'tags'

export type AlleleTableDataRow = {
  key: string
  allele: Allele
  annotation: Annotation
  displayAllele: AlleleDisplay[]
  displayFrequencies: {
    freq: { maxValue: number | null }
    count: { maxValue: number | null }
  }
  customAnnotation: CustomAnnotation | null
  presentationAllele: PresentationAllele | AnalysisPresentationAllele
  assessment: {
    existing: AlleleAssessment | null
    state: StateAlleleAssessment | null
    existingIsOutdated: boolean | null
  }
  [k: string]: unknown
}

export const selectTableData = (
  state: RootState,
  {
    presentationAlleles,
    genePanelName,
    genePanelVersion,
    filterConfigId,
    interpretationState,
  }: {
    presentationAlleles: (PresentationAllele | AnalysisPresentationAllele)[]
    genePanelName: string
    genePanelVersion: string
    filterConfigId?: number
    interpretationState?: AlleleInterpretationState
  },
): AlleleTableDataRow[] =>
  presentationAlleles.map((presentationAllele) => {
    const { alleleId, annotationId, customAnnotationId } = presentationAllele
    const displayFrequencies =
      filterConfigId !== undefined
        ? {
            freq: selectFrequencies(state, { annotationId, filterConfigId, key: 'freq' }),
            count: selectFrequencies(state, {
              annotationId,
              filterConfigId,
              key: 'count',
            }),
          }
        : {
            freq: { maxValue: null },
            count: { maxValue: null },
          }
    const assessment = interpretationState
      ? {
          existing: interpretationState.allele?.[alleleId]?.alleleAssessmentId
            ? selectAlleleAssessmentById(state, {
                assessmentId: interpretationState.allele?.[alleleId]
                  .alleleAssessmentId as number,
              }) ?? null
            : selectLatestAlleleAssessmentByAlleleId(state, { alleleId }) ?? null,
          state: interpretationState.allele?.[alleleId].reuseAlleleAssessment
            ? null
            : interpretationState.allele?.[alleleId]?.alleleAssessment,
          existingIsOutdated: interpretationState.allele?.[alleleId]?.alleleAssessmentId
            ? selectAlleleAssessmentIsOutdated(state, {
                assessmentId: interpretationState.allele?.[alleleId]
                  ?.alleleAssessmentId as number,
              })
            : null,
        }
      : { existing: null, state: null, existingIsOutdated: null }

    return {
      key: `${alleleId}`,
      allele: selectAlleleById(state, { alleleId }),
      annotation: selectAnnotationByAnnotationId(state, {
        annotationId,
      }),
      customAnnotation: customAnnotationId
        ? selectCustomAnnotationsByAnnotationId(state, {
            customAnnotationId,
          })
        : null,
      displayAllele: selectAlleleDisplay(state, {
        genePanelName,
        genePanelVersion,
        alleleId,
        annotationId,
      }),
      presentationAllele,
      displayFrequencies,
      assessment,
    }
  })

export const selectTableDataByCallerAndCategory = createSelector(
  selectTableData,
  (
    state: RootState,
    { interpretationState }: { interpretationState: AnalysisInterpretationState },
  ) => interpretationState,
  (tableData, interpretationState) => {
    const cnvs = tableData.filter((row) => row.presentationAllele.callerType === 'cnv')
    const snvs = tableData.filter((row) => row.presentationAllele.callerType === 'snv')

    const isClassified = (row: AlleleTableDataRow) =>
      row.assessment.state === null && row.assessment.existing !== null
    const isUnClassified = (row: AlleleTableDataRow) => !isClassified(row)

    const isRelevant = (row: AlleleTableDataRow) =>
      !interpretationState.allele?.[row.key].analysis.notrelevant
    const isNotRelevant = (row: AlleleTableDataRow) => !isRelevant(row)

    const isTechnical = (row: AlleleTableDataRow) =>
      interpretationState.allele?.[row.key].analysis.verification === 'technical'
    const isNotTechnical = (row: AlleleTableDataRow) => !isTechnical(row)

    return {
      cnv: {
        classified: cnvs.filter(isRelevant).filter(isNotTechnical).filter(isClassified),
        unclassified: cnvs
          .filter(isRelevant)
          .filter(isNotTechnical)
          .filter(isUnClassified),
        technical: cnvs.filter(isRelevant).filter(isTechnical),
        notRelevant: cnvs.filter(isNotRelevant),
      },
      snv: {
        classified: snvs.filter(isRelevant).filter(isNotTechnical).filter(isClassified),
        unclassified: snvs
          .filter(isRelevant)
          .filter(isNotTechnical)
          .filter(isUnClassified),
        technical: snvs.filter(isTechnical),
        notRelevant: snvs.filter(isNotTechnical).filter(isNotRelevant),
      },
    }
  },
)
