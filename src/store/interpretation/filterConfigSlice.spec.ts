import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import filterConfigSlice, {
  fetchFilterConfigById,
  selectFilterConfigById,
} from 'store/interpretation/filterConfigSlice'
import { store } from 'store/store'

import { FilterConfig } from 'types/store/FilterConfig'

const mock = new MockAdapter(axios)

function buildFilterConfig(override: Partial<FilterConfig>): FilterConfig {
  return {
    name: '',
    filterConfig: {
      filters: [],
    },
    active: false,
    ...override,
  }
}

describe('reference reducer', () => {
  afterEach(() => mock.reset())

  it('should handle initial state', () => {
    expect(filterConfigSlice(undefined, { type: 'unknown' })).toEqual({})
  })

  it('should handle fetching references from the backend', async () => {
    mock.onGet(API.FilterConfigs.path(1)).reply(200, {
      id: 1,
      active: true,
      name: 'dabla',
      filterconfig: {
        filters: [
          {
            name: 'foo',
            config: {},
            exceptions: [
              {
                name: 'bar',
                config: {},
              },
            ],
          },
        ],
      },
    })

    const dispatch = jest.fn()
    await fetchFilterConfigById(1)(dispatch, () => undefined, {})

    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        payload: {
          1: {
            active: true,
            name: 'dabla',
            filterConfig: {
              filters: [
                {
                  name: 'foo',
                  config: {},
                  exceptions: [
                    {
                      name: 'bar',
                      config: {},
                    },
                  ],
                },
              ],
            },
          },
        },
      }),
    )
  })

  it('should handle getting filter configs by id', async () => {
    const state = {
      ...store.getState(),
      filterConfigs: {
        1: buildFilterConfig({ name: 'foo' }),
        2: buildFilterConfig({ name: 'bar' }),
      },
    }

    expect(
      selectFilterConfigById(state, {
        filterConfigId: 1,
      }),
    ).toEqual(buildFilterConfig({ name: 'foo' }))
    expect(
      selectFilterConfigById(state, {
        filterConfigId: 2,
      }),
    ).toEqual(buildFilterConfig({ name: 'bar' }))
    expect(
      selectFilterConfigById(state, {
        filterConfigId: 3,
      }),
    ).toEqual(undefined)
  })
})
