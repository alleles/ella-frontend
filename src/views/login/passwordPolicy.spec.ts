import buildValidatorFromPasswordPolicy from 'views/login/passwordPolicy'

const policy = {
  password_minimum_length: 4,
  password_match_groups_descr: [
    'Uppercase letters [A-Z]',
    'Lowercase letters [a-z]',
    'Digits [0-9]',
    'Special characters',
  ],
  password_match_groups: ['.*[A-Z].*', '.*[a-z].*', '.*[0-9].*', '.*[^A-Za-z0-9].*'],
  password_num_match_groups: 3,
}

describe('password policy validator', () => {
  it('it can construct a yup schema from a password policy', () => {
    const schema = buildValidatorFromPasswordPolicy(policy)
    expect(schema).toBeDefined()
  })

  it('it will accept passwords of at least password_minimum_length length', () => {
    const schema = buildValidatorFromPasswordPolicy(policy)
    expect(schema).toBeDefined()
    expect(schema.isValidSync('Aa12')).toBe(true)
    expect(schema.isValidSync('Aa1')).toBe(false)
  })

  it('it will throw an exception upon failing minimum length', () => {
    const schema = buildValidatorFromPasswordPolicy(policy)
    try {
      schema.validateSync('Aa1')
    } catch (err: any) {
      expect(err.message).toBe('Password must be at least 4 characters long')
    }
  })

  it('it will enforce the password policy based on regexps', () => {
    const schema = buildValidatorFromPasswordPolicy(policy)
    expect(schema).toBeDefined()
    expect(schema.isValidSync('A123')).toBe(false) // Only uppercase & numeric
    expect(schema.isValidSync('a123')).toBe(false) // Only lowercase & numeric
    expect(schema.isValidSync('1234')).toBe(false) // Only numeric
    expect(schema.isValidSync('ABC!')).toBe(false) // Only uppercase & special

    expect(schema.isValidSync('Aa12')).toBe(true) // Uppercase & lowercase & numeric
    expect(schema.isValidSync('aA1!')).toBe(true) // Lowercase & uppercase & numeric & special
    expect(schema.isValidSync('1A1!')).toBe(true) // Numeric & uppercase & special
    expect(schema.isValidSync('A1!2')).toBe(true) // Uppercase & numeric & special
  })

  it('it will throw an exception upon failing minimum requirements', () => {
    const schema = buildValidatorFromPasswordPolicy(policy)
    try {
      schema.validateSync('ABC!')
    } catch (err: any) {
      expect(err.message).toBe('Password must meet at least 3 password policies')
    }
  })
})
