import { format } from 'date-fns'
import tinymce from 'tinymce'

const getDate = () => format(new Date(), 'yyyy-MM-dd')

const svg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
  <path d="M2.695 14.763l-1.262 3.154a.5.5 0 00.65.65l3.155-1.262a4 4 0 001.343-.885L17.5 5.5a2.121 2.121 0 00-3-3L3.58 13.42a4 4 0 00-.885 1.343z" />
</svg>`

tinymce.PluginManager.add('signature', (editor) => {
  const username = editor.getParam('current_user')
  const date = getDate()

  const insertSignature = () => {
    const el = editor.dom.createHTML(
      'span',
      null,
      `[<span data-editor-text-color="blue">${username} ${date}</span>]`,
    )
    editor.insertContent(el)
  }

  editor.ui.registry.addIcon('pencil', svg)

  editor.addShortcut('alt+s', 'Insert signature', () => {
    insertSignature()
  })

  editor.ui.registry.addButton('signature', {
    // text: 'Sign',
    label: 'Signature',
    icon: 'pencil',
    onAction: () => insertSignature(),
  })
  return {
    getMetadata() {
      return {
        name: 'Signature',
        url: 'https://example.com/docs/customplugin',
      }
    },
  }
})
