import get from 'lodash/get'
import React from 'react'
import { useSelector } from 'react-redux'

import { Card } from 'components/elements/Card'

import { selectAlleleById } from 'store/interpretation/alleleSlice'
import { selectAnnotationConfigById } from 'store/interpretation/annotationConfigSlice'
import { selectAnnotationByAnnotationId } from 'store/interpretation/annotationSlice'
import { RootState } from 'store/store'

import { mapToPartialApi } from 'types/store/Allele'
import { Annotation } from 'types/store/Annotation'
import {
  AnnotationSchema,
  FrequencySchema,
  ItemListSchema,
  KeyValueSchema,
  SectionType,
  TemplateConfig,
  getAnnotationSchemasBySection,
} from 'types/store/AnnotationConfig'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import { ClinvarDetailsTemplate } from 'views/interpretation/components/annotation/configurable/templates/ClinvarDetailsTemplate'
import { FrequencyTemplate } from 'views/interpretation/components/annotation/configurable/templates/FrequencyTemplate'
import { ItemListTemplate } from 'views/interpretation/components/annotation/configurable/templates/ItemListTemplate'
import { KeyValueTemplate } from 'views/interpretation/components/annotation/configurable/templates/KeyValueTemplate'

function prettyPrintLabel(schema: AnnotationSchema<TemplateConfig>): string {
  if (schema.title) return schema.title
  return schema.source.split('.').pop()?.replace('_', ' ') as string
}

const templateEngineFn = (urlTemplate: string | null, map: Record<any, any>) => {
  if (!urlTemplate) {
    return undefined
  }
  // Find all "templates" in the string, and replace them with values from map-object
  // We don't use an actual template string, but do string replacement
  const interpolated = urlTemplate.replace(/\${(.*?)}/g, (_, path) => get(map, path))
  return interpolated
}
interface ConfigurableAnnotationProp {
  section: SectionType
}

interface AnnotationProp {
  schema: AnnotationSchema<TemplateConfig>
  annotation: Annotation
}

function ConfigurableAnnotationDetails({ schema, annotation }: AnnotationProp) {
  const annotationData = get(annotation, schema.source)
  if (!annotationData) return null

  switch (schema.template) {
    case 'frequencyDetails':
      return (
        <FrequencyTemplate
          frequencyData={annotationData}
          schema={schema as FrequencySchema}
        />
      )
    case 'itemList':
      return (
        <ItemListTemplate
          itemListData={annotationData}
          schema={schema as ItemListSchema}
        />
      )

    case 'keyValue':
      return (
        <KeyValueTemplate
          keyValueData={annotationData}
          schema={schema as KeyValueSchema}
        />
      )

    case 'clinvarDetails':
      return <ClinvarDetailsTemplate clinvarDetailsData={annotationData} />

    default:
      throw Error(`Unsupported template:${schema.template}`)
  }
}

export default function ConfigurableAnnotation({ section }: ConfigurableAnnotationProp) {
  const { selectedAlleleId } = useInterpretationStateContext()

  const { presentationAlleles } = useInterpretation()
  const { annotationId } = presentationAlleles[selectedAlleleId]

  const allele = useSelector((state: RootState) =>
    selectAlleleById(state, { alleleId: selectedAlleleId }),
  )

  const annotation = useSelector((state: RootState) =>
    selectAnnotationByAnnotationId(state, { annotationId }),
  )

  const annotationConfig = useSelector((state: RootState) =>
    annotation
      ? selectAnnotationConfigById(state, {
          annotationConfigId: annotation.annotationConfigId,
        })
      : null,
  )

  // In old UI, annotation was nested under the allele object. To make the existing string work we need to nest it in under the
  // allele object. In addition, the template string uses api-style alleles (e.g. allele.vcf_ref), so we need to map Allele to
  // apiAllele to interpolate correctly
  const stringInterpolationMap = {
    allele: { ...mapToPartialApi(allele, selectedAlleleId), annotation },
  }

  if (annotationConfig) {
    const annotationSchemas = getAnnotationSchemasBySection(section, annotationConfig)

    return (
      <div className="flex flex-wrap">
        {annotationSchemas.map((schema) => (
          <div key={schema.source.replace('.', '_')} className="flex">
            <Card
              label={prettyPrintLabel(schema)}
              link={templateEngineFn(schema.url, stringInterpolationMap)}
            >
              {get(annotation, schema.source) &&
                ConfigurableAnnotationDetails({ schema, annotation })}
            </Card>
          </div>
        ))}
      </div>
    )
  }

  return <div>Annotation Schema is missing</div>
}
