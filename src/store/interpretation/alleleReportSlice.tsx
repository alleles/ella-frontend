import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleReport, mapFromApi } from 'types/store/AlleleReport'

const initialState: Record<number, AlleleReport> = {}

const fetchAlleleReportsForAlleleId = createAsyncThunk<typeof initialState, number>(
  'alleleReport/fetchAlleleReportForAlleleId',
  async (alleleId: number) => {
    const apiAlleleReports = await API.AlleleReports.get(alleleId).then(
      (result) => result.data,
    )

    const alleleReports = apiAlleleReports.reduce(
      (prev, current) => ({
        ...prev,
        [current.id]: mapFromApi(current),
      }),
      {} as Record<number, AlleleReport>,
    )
    return alleleReports
  },
)

const processAndStoreAlleleReport = createAsyncThunk<
  Record<number, AlleleReport>,
  ApiTypes.AlleleReport
>('alleleReport/processAndStoreAlleleReport', (alleleReport) => ({
  [alleleReport.id]: mapFromApi(alleleReport),
}))

const alleleReportSlice = createSlice({
  name: 'alleleReports',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAlleleReportsForAlleleId.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
    builder.addCase(processAndStoreAlleleReport.fulfilled, (state, action) => {
      Object.assign(state, action.payload)
    })
  },
})

const selectAlleleReportById = (
  state: RootState,
  { alleleReportId }: { alleleReportId: number },
) => state.alleleReports[alleleReportId]

const selectAlleleReportsByIds = (
  state: RootState,
  { alleleReportIds }: { alleleReportIds: number[] },
) =>
  alleleReportIds.reduce((acc, alleleReportId) => {
    acc[alleleReportId] = selectAlleleReportById(state, { alleleReportId })
    return acc
  }, {} as Record<number, AlleleReport | undefined>)

export {
  fetchAlleleReportsForAlleleId,
  processAndStoreAlleleReport,
  selectAlleleReportById,
  selectAlleleReportsByIds,
}

export default alleleReportSlice.reducer
