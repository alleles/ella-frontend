import React from 'react'

import { YesNoModal } from 'components/elements/modals/YesNoModal'

interface Props {
  onYes: () => void
  onNo: () => void
}

export function ReassignInterpretationConfirmationModal({ onYes, onNo }: Props) {
  return (
    <YesNoModal onNo={onNo} onYes={onYes}>
      This interpretation is currently assigned to another user. Are you sure you want to
      assign it to yourself?
    </YesNoModal>
  )
}
