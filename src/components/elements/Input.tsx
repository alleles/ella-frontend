import React, { InputHTMLAttributes } from 'react'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  label?: string
  error?: string
}

export default function Input({ label, error, ...props }: Props) {
  return (
    <div className="flex w-full flex-col">
      {error && <div className="p-1 text-sm text-ellared-dark">{error}</div>}
      <div className="flex w-full">
        {label && (
          <span className="inline-flex items-center rounded-l-md border border-r-0 border-ellagray bg-ellagray-50 px-3 text-sm text-ellagray-500">
            {label}
          </span>
        )}
        <input
          className="block w-full min-w-0 flex-1 rounded-r-md border border-ellagray p-2 text-sm focus:border-ellablue focus:ring-ellablue disabled:text-ellared"
          {...props}
        />
      </div>
    </div>
  )
}
