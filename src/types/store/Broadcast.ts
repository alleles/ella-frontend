import * as ApiTypes from 'types/api/Pydantic'

export interface Broadcast {
  id: number
  message: string
  date: string
}

export function mapFromApi(apiBroadcast: ApiTypes.Broadcast): Broadcast {
  return {
    id: apiBroadcast.id,
    message: apiBroadcast.message,
    date: apiBroadcast.date,
  }
}
