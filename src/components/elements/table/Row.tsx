import React, { HTMLProps } from 'react'

interface RowProps extends HTMLProps<any> {
  children
}

export default function Row({ children, ...props }: RowProps) {
  return (
    <tr
      className={`space-y-1 align-baseline text-sm font-medium text-ellagray-500 hover:bg-ellablue-lightest ${
        props.className || ''
      }`}
      {...props}
    >
      {children}
    </tr>
  )
}
