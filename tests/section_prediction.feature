Feature: View and edit Prediction section

  Background:
    Given state "test-bdd"

  Scenario: Check Prediction section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "PREDICTION"
    And I should see "CONSEQUENCE"
    And I should see "CONSERVATION"
    And I should see "DOMAIN"
    And I should see "SPLICE SITE"

    # Can edit comment field
    When I start the interpretation
    And I type "Test Prediction comment" into the editor with xpath "//div[@id='prediction-comment-editor-content']"
    Then I should see "Test Prediction comment"

    # Check annotation for 2 variants
    # BRCA2 c.51_52del
    When I select the allele row 1 from the allele table
    Then I should see "frameshift"
    # BRCA2 c.72A>T
    When I select the allele row 3 from the allele table
    Then I should see "missense"

# TODO: Test choices/buttons, including suggested ACMG criteria - blocked by #187
