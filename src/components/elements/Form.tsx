import React, { FormHTMLAttributes } from 'react'

import Error from 'components/elements/Error'

interface Props extends FormHTMLAttributes<HTMLFormElement> {
  error?: string
}

export default function Form({ error, children, ...props }: Props) {
  return (
    <div className="flex w-full flex-col">
      {error && <Error error={error} />}
      <form {...props}>{children}</form>
    </div>
  )
}
