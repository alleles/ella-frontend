import React from 'react'
import { NavLink, NavLinkProps } from 'react-router-dom'

export default function Link({ children, ...props }: NavLinkProps) {
  return (
    <div className="truncate underline hover:text-ellablue-darkest">
      <NavLink {...props}>{children}</NavLink>
    </div>
  )
}
